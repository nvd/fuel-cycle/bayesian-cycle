\mychapter{Physics of Nuclear Fuel Cycles}{nfcphysics}

A \gls{nfc}, or fuel cycle in short, is a sequence of industrial processes handling nuclear material. One goal of
fuel cycles is producing electricity in nuclear reactors, but a fuel cycle can also result in \gls{fissile}
material for use in nuclear weapons. A fuel cycle can either be ``once-through'', as in this work: in this case,
uranium enters the fuel cycle, is processed through multiple steps, and electricity as well as fissile material are
produced; final products are stored and not reused in the fuel cycle. More complex fuel cycles reprocess material that
has already passed through parts of the fuel cycle again in earlier stages. \cite{FuelCycleOverview}

\mysection{Investigated Fuel Cycle}{nfcphysics:cycle}

The fuel cycle used later in this work for analyzing the implementation in this work is relatively simple. It consists
of the following components, which have a correspondence in the simulation software used in this work. Most components
are part of most other fuel cycles too; the re-enrichment stage is typical for a fuel cycle starved of uranium supply,
forcing recovery of uranium from spent fuel.

\begin{description}
\item[Uranium source,] an infinite source of uranium with an \gls{enrichmentgrade} of our choice. The fuel production
process is simplified in this model, and the uranium mine (unrealistically) produces fuel for direct consumption by the
reactor.
\item[Reactor,] modeled after the american \emph{Savannah River Site} K-reactor \cite{NucWeapHandbook87}, which is
mainly used for production of \gls{weaponsgradePu}.
\item[Reprocessing plant,] a reprocessing facility separating spent fuel chemically into uranium and plutonium as
well as leftover waste \gls{isotope}s (\gls{fission} and decay products).
\item[Plutonium storage,] receives the plutonium from the \emph{Separations} facility.
\item[Reprocessing waste dump,] receives the separated waste from the \emph{Separations} plant.
\item[Re-enrichment facility,] enriches the separated uranium from the spent fuel, which is produced by the
reprocessing plant.
\item[Depleted uranium dump,] receives \gls{depletedu} from the re-enrichment facility.
\item[Highly enriched uranium storage,] receives \gls{heu} from the re-enrichment facility.
\end{description}

\begin{figure}[h]
 \centering
 \includegraphics[width=0.6\textwidth]{figures/nfc/fuelcycle.pdf}
 % fuelcycle.pdf: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption[Flow diagram of simulated fuel cycle]{A simple flow diagram of the fuel cycle considered here.}
 \label{fig:nfc:fuelcycle}
\end{figure}

Uranium from the source runs through this chain, becoming partially transmuted into plutonium and other minor isotopes,
before the different products and residues are delivered into dumps or storage facilities, where they remain. How these
components work, both in isolation and in cooperation with each other, is governed by \emph{simulation parameters},
which determine individual operational factors; such as the enrichment grade produced by the enrichment facility. The
following sections describes these components in greater detail; both the real-world antetypes and their equivalent in
the simulation are discussed.

The \emph{dumps}, containing depleted uranium and reprocessing waste, are important as they will be the
source of information for later reconstruction attempts. In a real-world scenario, the compositions of waste in
these facilities may be available to inspectors, as opposed to stockpiles of highly-\gls{enrichedu} or plutonium.
Therefore, this work is mainly concerned with using the available materials, their composition, mass, etc. in order to
make statements about the history of these materials and therefore the structure and characteristics of the fuel cycle
that they have passed through.

Reconstructing parameters such as the cycle time of the reactor, the product enrichment
grade of highly enriched uranium, and the fresh fuel enrichment grade of the initially used enriched uranium is the
main goal of this work.

There are two main purposes of fuel cycles, and often the same fuel cycle is used for both: for civilian
applications, power is generated; this is a common use case around the world today. For military
applications, the goal is to produce \gls{fissile} material; fissile material comprises those nuclides which can
sustain a chain reaction, and can be used to build nuclear weapons. The most important fissile nuclides are \uranium{5}
and \plutonium{39}; for this reason, this work is focused on reconstructing fuel cycle parameters related to the
production of these two isotopes. Minor nuclides are also used for some applications, but are not considered relevant
for this work.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mysection{Components}{nfcphysics:components}

\subsection{Source of Fuel}
\label{sec:nfcphysics:source}

Uranium enters a fuel cycle after being mined from a natural deposit. The uranium ore is
extracted by conventional mining methods, such as surface pit mining or leaching using aggressive liquids to dissolve
the uranium contained in the rock. The ore extracted from the deposit is then \emph{milled}, i.e. crushed, and mixed
with -- for example -- an acidic or alkaline solution such as sulphuric acid \ce{H2SO4} in order to extract the
uranium. So-called ore concentrate contains \ce{U3O8} after extraction from the solution at levels of up to
\SI{80}{\percent}, and can be dried to create the well-known \emph{yellow cake} (named for its color). This is the
commercially most important form of natural uranium.

Natural uranium typically contains the isotopes shown in \autoref{tab:nfcphysics:natu}. The specific concentrations,
especially of \uranium{4}, vary by deposit \cite[p.\,50]{Fedchenko2015}. It is also important to note here that
\uranium{4} from natural uranium is not part of the fuel cycle simulation in this work, as the used reactor models do
not support simulating this isotope. In an actual fuel cycle however, \uranium{4} is of high relevance to nuclear
archaeology applications due to the characteristic concentrations in different deposits, and the behavior in the
reactor.

\begin{table}[h]
\centering
\begin{tabular}{r|c}
 Isotope & Concentration (\si{\percent}) \\
 \midrule
 \uranium{3} & \gls{nno} \\
 \uranium{4} & \num{0.0054} \\
 \uranium{5} & \num{0.7204} \\
 \uranium{6} & \gls{nno} \\
 \uranium{8} & \num{99.2724} \\
\end{tabular}
\caption[Natural distribution of uranium isotopes]{Natural occurrence of uranium isotopes. \cite{RSCUranium}}
\label{tab:nfcphysics:natu}
\end{table}

Some reactor types, like light water reactors (LWR), are not able to use natural uranium due to its low \uranium{5}
content; \uranium{8} is not fissile and cannot sustain a chain reaction in such reactors. \uranium{5} is more
readily fissioned by \gls{thermalneutron}s (neutrons with an energy of around \SI{25}{\milli\electronvolt}).
Thus, the concentration of \uranium{5} needs to be increased before uranium can be used as fuel; this is achieved in
\emph{Enrichment Plants}. Today, enrichment works by using centrifuges working with uranium hexafluoride \ce{UF6} in its
gaseous form. Those molecules containing \uranium{8} are about \SI{0.86}{\percent} heavier than molecules with a
\uranium{5} atom. Gas centrifuges separate a stream of \emph{feed} gas into \emph{product} and \emph{tails}, with the
product containing slightly more of the desired \uranium{5} than both feed and tails. Each centrifuge is only able to
increase the relative \uranium{5} contents by a small factor, called the \emph{(single-stage) separation factor} $q$. It
is defined by the ratio of abundances $R$:

\begin{equation}
 R_A = \frac{c_A}{1-c_A}~\text{(concentration~}c\text{ of isotope $A$)}
\end{equation}

of \uranium{5} between the product (enriched) stream $p$ and tails (depleted) stream $t$:

\begin{equation}
 q = \frac{R_{235, p}}{R_{235, t}} = \frac{c_{235, p} (1-c_{235,t})}{(1-c_{235,p}) c_{235,t}}
\end{equation}

The separation factor of a single centrifuge is by far not enough to directly produce highly enriched uranium from
natural uranium. In order to achieve higher enrichment grades, many stages of centrifuges (usually 10 to 20 for low
enrichment grades) need to be connected to form a \emph{cascade}, in which the product of each stage is fed to a
subsequent stage while the tails of each stage is fed back to a previous stage in order to extract more of the
remaining \uranium{5}. Along with \uranium{5}, other isotopes lighter than \uranium{8} (\uranium{4} and \uranium{6})
are co-enriched. Further details are not important at this point, as the simulation used in this work glosses over the
specifics of natural uranium enrichment anyway. More details about enrichment theory is found in
\cite[Ch.\,5]{SIPRI83}.

Uranium can be enriched to levels of \SIrange{3}{5}{\percent}, making it suitable for use in light water
reactors\footnote{The enrichment grade required for reactor use varies with the employed reactor type.}, either for
electricity production or in order to transmute elements and e.g. produce nuclides such as plutonium for nuclear
weapons. Uranium of an enrichment grade $<\SI{10}{\percent}$ cannot be assembled to achieve the critical mass, i.e. the
specific mass which, if assembled at once, will generate a runaway chain reaction, resulting in a nuclear explosion.
Enrichment can also achieve higher concentrations, to make weapons-grade uranium at concentrations of more than
\SI{20}{\percent} \uranium{5}. \cite[p.\,5, 95]{SIPRI83}, \cite{FuelCycleEnrichment}

In the simulation used for this work, the uranium source produces slightly enriched uranium
(\SIrange{1.0}{2.0}{\percent}). The enrichment of natural uranium to slightly enriched uranium is skipped; instead, the
source directly delivers uranium of the desired enrichment grade. Another import deviation of the simulation from
reality is the lack of \uranium{4} in natural and slightly enriched uranium. The used reactor models are not (yet)
equipped to take \uranium{4} concentrations into account, and therefore the entire \uranium{4} occurring in the fuel
cycle is generated through nuclear reactions. This means that no realistic absolute concentration can be derived from
the simulation, but the prediction as described in the sections below is not affected in principle, as it doesn't
require the concentrations to be perfectly reproducing real circumstances. Especially if the \uranium{4} concentration
of fresh fuel is known to some accuracy, it can be modeled in the reconstruction process. Depending on the specific
fuel cycle, an unknown concentration of \uranium{4} in natural uranium can lead to significant uncertainties, however.
\cite{FuelCycleOverview}

\subsection{Reactor}

The fuel material used in a reactor is often a ceramic form of uranium, for example uranium dioxide \ce{UO2},
containing a mixture of slightly enriched uranium. It is produced by conversion from the enriched \ce{UF6}. Other forms
are also used, depending on the reactor type.

Electricity is generated in civilian power plants by steam turbines. The steam is generated from heat as a result of
neutrons generated by decaying nuclei being moderated (slowed) in water, graphite, or another moderator. In military
reactors, the primary goal is for the neutrons to be (for example) captured by \uranium{8} nuclei to form \uranium{9}
which then transmutes via two $\beta^-$ decays into \plutonium{39}. Like \uranium{5}, \plutonium{39} is a fissile
material, and already partially burned during the time in the reactor.

The life cycle of fuel within a reactor can be characterized by (among other parameters) the \emph{specific
\gls{burnup}}, often measured in \si{\mega\watt\day\per\kilogram} or \si{\mega\watt\day\per\tonne}. It specifies how
much thermal energy was generated per unit mass of uranium. The \gls{sbu} is calculated from other parameters,
notably the \gls{irradiationtime} (a synonym for \gls{cycletime}):

\begin{equation}
\label{def:burnup}
 \text{specific burnup} = \frac{\text{power}\times \text{irradiation time}}{\text{core mass}}
\end{equation}

Burnup is a very important parameter, as it indicates the regime employed by the reactor operator. If the
burnup is small, usually quite below \SI{10}{\mega\watt\day\per\kilogram}, it is a sign that the reactor has been used
to generate \gls{weaponsgradePu}. Otherwise, high burnups of \SIrange{30}{70}{\mega\watt\day\per\kilogram} (for
light water reactors) show that fuel was used purely for electricity generation. The reason for this distinction is
that with increasing burnup, the continuous process of neutron capture converts \plutonium{39} into \plutonium{40},
which has a high spontaneous fission rate and makes building reliable nuclear weapons difficult (see isotope sections
below; \cite[p.\,111]{Fedchenko2015}). Burnup is also an economic factor; the higher it is, the more energy is
extracted from the fuel. The limiting factor is often the accumulation of fission products which inhibit the reactor's
operation. \cite{FuelCycleOverview,PhysicsOfPlutonium}

The reactor in this work is modeled after the K reactor in the Savannah River Site plant complex. This pressurized heavy
water reactor uses \ce{D2O} as moderator, and can run with either natural or slightly enriched uranium
(\SIrange{1}{2}{\percent}). Its main purpose is generating plutonium and tritium for nuclear and thermonuclear weapons,
respectively, as well as other radioisotopes for medical or scientific purposes. The K reactor commenced operation in
the 1950s; there are in total five reactors named C, K, L, P, and R on the same site. The reactor fundamentally
contains two kinds of assemblies: First, \emph{fuel assemblies} ``drive'' the reactor, and contain either enriched
uranium or plutonium in order to sustain a chain reaction. The other kind is the \emph{target assembly}, containing
depleted uranium which is irradiated by neutrons generated by the fuel assemblies. The irradiation causes \uranium{8}
to gradually turn into \plutonium{39}, as described earlier. Further, target assemblies can also contain lithium
enriched in \isotope[6]{Li} which, when irradiated with neutrons, turns into \isotope[4]{He} and \isotope[3]{H}.
Tritium (\isotope[3]{H}) is necessary for use in the \emph{H bomb}, which is also named after it.
\cite[p.\,97ff.]{NucWeapHandbook87}

\subsection{Separation of Spent Fuel}

Once nuclear fuel has been cycled out of the reactor it served in, it is called \emph{spent fuel}. It contains a
lower concentration of \uranium{5} than the fresh fuel, a higher concentration of plutonium, and a host of
other isotopes which are direct or indirect (via decay) products of the irradiation of fuel with neutrons. For the
simulation used here, the subset of isotopes considered in spent fuel are shown in \autoref{tab:nfcphysics:spentiso},
and determined by the configuration of \cd{GprReactor}. The set of emitted isotopes within the simulation is not
identical to those generated by an actual reactor.

The fragments produced by fissioning a nucleus are often unstable due to an excess of neutrons, and will undergo
$\beta^-$ decay until a stable isotope is reached. Caesium and strontium are also common products of a fission of
\uranium{5}\footnote{\uranium{5} fissions statistically into fragments of different masses. Daughter nuclide masses
around \SI{95}{\amu} and \SI{135}{\amu} are preferred.}. Neptunium is a result of neutron capture by \uranium{8} and
consecutive $\beta^-$ decay, and continues to decay itself into \plutonium{39} within days. \cite{IAEAFissionProducts,
PhysicsOfUranium,FuelCycleOverview}

\begin{table}[h]
\centering
\begin{subtable}[t]{0.3\textwidth}
 \begin{tabular}[t]{r|c}
  Element & Isotope \\
  \midrule
  \midrule
  \multirow{3}*{Sr} & 88 \\
  & 89 \\
  & 90 \\
  \midrule
  \multirow{4}*{Cs} & 133 \\
  & 134 \\
  & 135 \\
  & 137 \\
  \midrule
  \multirow{3}*{Np} & 239 \\
  & 240 \\
  & 241 \\
 \end{tabular}
\end{subtable}
\begin{subtable}[t]{0.3\textwidth}
 \begin{tabular}[t]{r|c}
  Element & Isotope \\
  \midrule
  \midrule
  \multirow{8}*{U} & 232 \\
  & 233 \\
  & 234 \\
  & 235 \\
  & 236 \\
  & 238 \\
  & 239 \\
  & 240 \\
 \end{tabular}
\end{subtable}
\begin{subtable}[t]{0.3\textwidth}
 \begin{tabular}[t]{r|c}
  Element & Isotope \\
  \midrule
  \midrule
  \multirow{7}*{Pu} & 238 \\
  & 239 \\
  & 240 \\
  & 241 \\
  & 242 \\
  & 243 \\
  & 244 \\
 \end{tabular}
\end{subtable}

 \caption[Isotopes in simulated spent fuel]{Isotopes in spent fuel considered in this simulation.}
 \label{tab:nfcphysics:spentiso}
\end{table}

As the goal of running a fuel cycle like this is producing \gls{fissile} material for use in nuclear weapons, the
reactor products need to be refined. This happens in a reprocessing plant, where a chemical process such as
\textsc{PUREX}, separates plutonium and uranium from the usually discarded trace isotopes of other elements. The
products of a reprocessing plant are \ce{PuO2} and \ce{UO2} in the case of a \textsc{PUREX} process.
\cite{FuelCycleReprocessing}

The simulation used in this work sends the products of the reprocessing facility to three different agents; two
dumps for the leftover waste products and the separated plutonium, and the re-enrichment plant, which receives uranium
separated from the spent fuel. The dumps respective storage facilities don't apply further processing to the materials
stored in them, although natural radioactive decay continues.

\subsection{Enrichment of Spent Fuel}

The reason for separated uranium being sent from the reprocessing plant to another enrichment facility is that spent
fuel still contains a high concentration of uranium. Even though \uranium{5} has been ``burned off'' by the reactor
(see \autoref{fig:nfcphysics:u235_sepwaste}), there is usually some left, and discarding \uranium{5} may
not be economical in scenarios where there is a limited supply of natural uranium. (Even though this fuel cycle has an
unlimited supply of uranium, the idea is to simulate real-world fuel cycles where this may not be the case.)

In this fuel cycle, the spent fuel re-enrichment facility is responsible for enriching the spent fuel's uranium.
The following tests assume an enrichment grade of \SI{90}{\percent} to simulate production of weapon-grade
uranium.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \mysection{Relevance of the Simulated Fuel Cycle}{nfcphysics:relevance}
%
% \TODO{slated for removal if space is needed!}
%
% The fuel cycle simulated in this work does not come from thin air; while it is not as such used anywhere,
% general inspiration comes from the situation in Pakistan. There, plutonium production has been happening since the late
% 1970s, and is characterized by a scarcity of natural uranium. Pakistan itself does have uranium deposits, and runs two
% mines -- \emph{Bagalchore}, discovered in 1959, and \emph{Qabul Khel}, opened in 1992 -- for fulfilling its own uranium
% needs. Both are now considered depleted or dormant (respectively); new mines have been discovered, and are exploited as
% of now: for example, the \emph{Taunsa} mine in 2000. \cite{IAEA409, IAEA410, NTIQabul, NTITaunsa}
%
% Additionally, Pakistan has not signed the \gls{npt}, which means that it is difficult for Pakistan to find suppliers
% for nuclear-use components or uranium, except where \gls{iaea} safeguards are in place (as of 2009, Pakistan seems to
% have imported not more than \SI{110}{\tonne} of uranium from Niger, under \gls{iaea} safeguards). Therefore its mines
% are the only reliable source of uranium for uncontrolled use, e.g. military applications.
% %
% % \begin{table}[h]
% %  \centering
% %  \begin{tabular}{r|cc}
% %   Characteristic & Bagalchore & Qabul Khel \\
% %   \midrule
% %   Operations started & 1977-80 & 1992 \\
% %   Reserves (in U t) & 181 & \numrange{500}{1000} \\
% %   Status & exhausted 1998 & operational 2011 \\
% %   U Grade & \SIrange{0.03}{0.1}{\percent} & \SIrange{0.02}{0.03}{\percent} \\
% %  \end{tabular}
% %  \caption{Uranium mines in Pakistan \cite{Mian2009, NTIQabul, NTITaunsa}}
% %  \label{tab:nfcphysics:pakistanmines}
% % \end{table}
%
% These constraints lead to Pakistan being starved for uranium, and making it not only economically viable, but
% imperative, to reprocess and re-enrich spent fuel. Another factor justifying this is the low burn-up achieved in its
% reactors; after all, plutonium production is a main goal of the non-safeguarded nuclear facilities, and plutonium
% production requires low burn-up rates, leaving a relatively high proportion of \uranium{5} in the spent fuel.
%
% In this fuel cycle, both plutonium and uranium for use in nuclear weapons are produced from limited domestic uranium
% supplies. By estimates from Mian et al. \cite{Mian2009}, Pakistan may have accumulated around \SI{450}{\kilo\gram} of
% fissile plutonium and \SIrange{2500}{6000}{\kilo\gram} of \gls{highlyenrichedu}, enough for 90 weapons made of
% plutonium and up to 240 weapons utilizing \gls{heu}. \cite{Mian2009}
%
% Another reason for simulating a fuel cycle like the presented one is its comparative simplicity. Large fuel cycles,
% like the one present in the United States, are almost impossible to reason about: not only are many of its factors
% unknown or classified, they also comprise such a large number of reactors, enrichment plants, reprocessing facilities,
% waste dumps, and other components, often exchanging nuclear materials in complex manners, that essentially no
% statements about the fuel cycle can be made from material compositions alone (e.g., waste composition). In contrast, by
% considering only a small number of components and limited well-known exchange patterns as in the presented fuel cycle,
% more useful and accurate predictions can be made.
%
% Further below, a specific scenario is introduced using an imaginary nation, in order to not confuse choices made for
% this specific simulation with facts from the real world.

\mysection{Relevant Isotopes}{nfcphysics:isotopes}

% \subsection*{Uranium-232}
%
% \uranium{2} doesn't occur in nature, but trace amounts are present in spent fuel. It has a half-life of
% \SI{68.9}{\year}, decaying further into \isotope[228]{Th}. Different chains lead to its production, all of them
% containing several neutron capture and decay ($\alpha, \beta$) steps; the last link is \plutonium{36} - $\alpha$ -
% \uranium{2}. Within a reactor, it also can turn into \uranium{3} by means of neutron capture. Through some of its
% daughter nuclides, it presents a challenge due to their radioactivity; some decay products emit high-energy $\beta$ and
% $\gamma$ radiation. \cite{FuelCycleReprocessing,ReprocessedUranium2007}
%
% \subsection*{Uranium-233}
%
% \uranium{3} is a \gls{fissile}, \gls{nno} (except for trace amounts) isotope of uranium with a half-life of
% \SI{1.59e5}{\year}. It is produced synthetically in nuclear reactors by $\beta$ decay of \isotope[233]{Pa}, electron
% capture of \isotope[233]{Np} or $\alpha$ decay of \plutonium{37}. While fissile, it only occurs in trace amounts within
% spent fuel, and is of limited importance to commercial and military applications.
% \cite{FuelCycleReprocessing,RSCUranium,ReprocessedUranium2007}

\subsection*{Uranium-234}
\label{sec:nfc:u234}

\uranium{4} occurs naturally in trace amounts (\SI{0.0054}{\percent}, i.e. 54 ppm \cite{RSCUranium}), and decays into
\isotope[230]{Th} with a half-life of \SI{245e3}{\year}, emitting high-energy $\alpha$ rays. \uranium{4} is both
produced and consumed in a reactor: when \uranium{5} collides with a neutron, it either fissions -- or, with a low
probability, captures the neutron to make \uranium{6}, which can decay through emission of two neutrons, resulting in
\uranium{4} (an $n,2n$ reaction). Conversely, \uranium{4} will capture a neutron to convert to \uranium{5}, which acts
as fuel (but the neutron capture is detrimental to a predictable operation of the fuel elements). The latter reaction is
more probable than production of \uranium{4} from \uranium{5} but still comparatively rare, as the concentration of
\uranium{4} is very low. \cite[pp.\,7-9]{ReprocessedUranium2007}

At this point, it bears repeating that the \uranium{4} content of natural uranium is not taken into account in this
simulation (\autoref{sec:nfcphysics:source}).

\subsection*{Uranium-235}

\begin{figure}[h]
 \centering
 \includegraphics[width=0.6\textwidth]{figures/nfc/u235_separated_waste.pdf}
 \caption[U-235 concentration in reprocessing waste for different cycle times]{\uranium{5} concentration in the
reprocessing plant's waste relative to total uranium content as a function of cycle time, at an initial enrichment level
of \SI{1.1}{\percent}, as used by default in the simulated fuel cycle. This fraction is identical to the fraction of
\uranium{5} in the spent fuel.}
\label{fig:nfcphysics:u235_sepwaste}
\end{figure}

\uranium{5} is the most important isotope for a fuel cycle, as it is the most common naturally occurring \gls{fissile}
isotope. An enrichment facility is responsible for increasing its concentration for use in reactors, where it is
``burned'' (i.e., fissioned and converted into fission products). Along with \plutonium{40}, it is
therefore an indicator for the cycle time. It has a long half-life of \SI{703.8e6}{\year}, and decays via $\alpha$ into
\isotope[231]{Th}. \cite{ReprocessedUranium2007}

\subsection*{Uranium-236}

\uranium{6} is produced by neutron capture from \uranium{5}, and has a half-life of \SI{23.42e6}{\year}. Due to this
generation process, its content in spent fuel is proportional to the specific burnup that the fuel has experienced,
making it a good indicator of cycle time (assuming that the reactor power is constant). It
is not fissile. \cite{ReprocessedUranium2007}

% \subsection*{Uranium-238}
%
% \uranium{8} is the most common isotope of uranium in nature, and not fissile. It has a half-life of \SI{4.47e9}{\year},
% decaying into \isotope[234]{Th} initially. In a reactor, \uranium{8} is converted into \plutonium{39} by means of
% neutron capture (\uranium{9}) and subsequent $\beta^-$ decays via neptunium. \cite{PhysicsOfUranium}

\subsection*{Plutonium-238}

\plutonium{38} ($t_{1/2} = \SI{88}{\year}$) is generated by irradiating \isotope[237]{Np}, which turns into
\isotope[238]{Np} and then decays via $\beta^-$ into \plutonium{38}. It doesn't have a military use, but is useful for
applications where its high decay heat (\SI{0.6}{\watt\per\gram}) can be used as energy source in a radio nuclide
battery -- for example, deep space missions, remote research stations, or even pacemakers. In that case, \plutonium{38}
is enriched to levels of about \SI{90}{\percent}. It is counterproductive in nuclear weapons at significant
concentrations due to its decay heat. It is a very rare isotope today, as production has dropped in recent decades;
however, new efforts have started to pick up production of \plutonium{38} for use in scientific applications.
\cite[p.\,113]{Fedchenko2015}, \cite{PhysicsOfPlutonium}

\subsection*{Plutonium-239}

\begin{figure}
 \centering
 \includegraphics[width=0.7\textwidth]{figures/nfc/separated_waste_sink_pu_concs_by_cycletime.pdf}
 \caption[Pu-239 and Pu-240 concentrations as function of cycle time]{Concentration of \plutonium{39} and
\plutonium{40} in produced plutonium, as function of cycle time. This
result was generated by the simulated fuel cycle.}
 \label{fig:nfcphysics:pu240}
\end{figure}

\plutonium{39} is, like \uranium{5}, a \gls{fissile} isotope of plutonium with a half-life of \SI{2.4e4}{\year}.
It is generated by neutron capture and double $\beta^-$ decay when \uranium{8} is irradiated with neutrons, which takes
place in any reactor type running with uranium fuel. The concentration of \plutonium{39} is therefore an indicator for
the irradiation time of fuel in a reactor. Over time, \plutonium{39} can capture another neutron and turn into
\plutonium{40}, which is described below. In spent fuel of civilian reactors, \SIrange{55}{70}{\percent} of the
plutonium (which itself is only about \SI{1}{\percent} of the spent fuel) is \plutonium{39}.

It is of special interest for both civilian and military applications: For civilian applications, a fuel type called
MOX (\emph{mixed oxide}) is made from uranium and plutonium. This way, plutonium can contribute to power generation in
commercial reactors. In military applications, \plutonium{39} is used because of its low critical mass and great
fissility, which make it suitable for nuclear weapons. As a rule of thumb, around \SI{10}{\kilo\gram} of \plutonium{39}
are required for a fission bomb. \cite{PhysicsOfPlutonium}

\subsection*{Plutonium-240}

\plutonium{40} is created by neutron capture from \plutonium{39}, has a half life of \SI{6.5e3}{\year}, and is not
fissile. However, it exhibits a high rate of spontaneous fission; this makes it undesirable for
use in military applications demanding high reliability and predictability of nuclear weapons, which is thwarted by the
spontaneously emitted neutrons and high decay heat exhibited by \plutonium{40}. \cite{PhysicsOfPlutonium}

Due to their extremely close masses, it is essentially infeasible to separate \plutonium{39} from \plutonium{40}. Thus,
a high concentration of \plutonium{40} in irradiated fuel indicates a high specific burnup, and simultaneously proves
that there is no goal of producing nuclear weapons from this material. In general, \gls{weaponsgradePu} contains less
than \SI{7}{\percent} of \plutonium{40}, whereas fuel- and reactor-grade plutonium contains more than \SI{7}{\percent}
respectively \SI{19}{\percent} of \plutonium{40}. \cite[Table 5.3]{Fedchenko2015}

\subsection*{Higher plutonium isotopes}

There are heavier plutonium isotopes, namely \plutonium{41} (half-life \SI{14.4}{\year}, fissile; decays into
\isotope[241]{Am}, an intense $\alpha, X$, and $\gamma$ emitter), \plutonium{42} (half-life \SI{374e3}{\year}, and
like \plutonium{40} fissioned by fast neutrons, as well as fissioning spontaneously), and \plutonium{44} (the most
stable isotope at $t_{1/2} = \SI{88e6}{\year}$). They come into existence by neutron capture of lighter plutonium
isotopes, and are of no particular interest for fuel cycles, except that they also indicate irradiation time of
nuclear fuel by build-up through neutron capture. \cite{PhysicsOfPlutonium}, \cite[p.\,19f.]{SIPRIUPu}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mysection{Measurement of Isotopic Composition}{nfcphysics:measurement}

% \TODO{Limits of accuracy, SIPRIUPu, p.8}

The information used for inferring unknown parameters in the following chapters is taken from the waste material, i.e.
depleted uranium and separated waste. Uranium and plutonium stockpiles could be included too, although it cannot be
assumed that access to those more sensitive materials is as likely as to waste material.

In any case, the waste must be characterized; the most common way to do this is using mass spectrometry. This
measurement method can identify and quantify even minute traces of individual isotopes, and is therefore well-suited to
nuclear investigations. The virtually only disadvantage is that mass spectrometry is destructive, although a small
sample is enough to produce useful results. There are different kinds of mass spectrometry; they have in common that
they are best suited for measuring isotope ratios, not absolute concentrations.\footnote{This fact directed the
implementation of inference experiments in this work: all of the results presented in later chapters were obtained by
simulating concentration ratios.}

A common kind of mass spectrometry is the \emph{Thermal Ionization Mass Spectrometry} (TIMS) method, which is commonly
used to determine isotope ratios with high precision. TIMS works by vaporizing, then ionizing a small sample on a hot
surface (thermal ionization). The precondition for this to work is a low first ionization energy of the relevant
substances in the sample, a property shared by ``interesting'' elements such as plutonium and uranium. After
ionization, an electric field accelerates the ions into a mass analyzer utilizing a magnetic or electric field to
separate the particles by their charge/mass ($e/m$) ratio. Finally, ions are detected and individual ``peaks''
(accumulations of particles with identical $e/m$) can be discerned. The strength (height) of each peak is determined by
the relative concentration of the respective particle type, and explains why abundance ratios are precisely measured by
a mass spectrometer -- as opposed to absolute concentrations. For this reason, TIMS has been a ``key measurement
technique in nuclear forensic investigations'' (\cite[p.\,50]{Fedchenko2015}) for some time. Especially the abundance
ratios of uranium and plutonium can be measured well.

Another mass spectrometry method called \emph{Inductively Coupled Plasma Mass Spectrometry} (ICP-MS) can also measure
concentrations, and find traces down to a mass concentration of \num{1e-15}. Here a sample is ionized in an argon
plasma before sending the ions to an $e/m$ analyzer, which can be a sector field, time-of-flight, or combined
analyzer. Typically, these instruments are used for finding minute quantities of isotopes. There are some
disadvantages, however; for example, interference by plasma-generated molecule ions.

The details of measurement are not very important to this work. As long as a reliable measurement of either
isotopic concentrations or abundance ratios along with a robust uncertainty estimate is available, the measurement
method is of no relevance. \cite[Ch.\,3]{Fedchenko2015}

While the measurement methods available today offer extremely high precision, there are some factors introducing a much
higher uncertainty which can impact the accuracy of results. For example, nuclear waste is unlikely to be purely from
one facility; as mostly worthless (yet dangerous) waste, it is likely to be mixed with other products making it very
difficult to obtain measurements that can help reconstruct a fuel cycle's history. Furthermore, when measuring waste
composition the radioactive decay must be taken into account. This requires that the age of the waste be known; this is
difficult if there are no records, but also challenging when dealing with a mixture of different waste batches
discarded at different times.
