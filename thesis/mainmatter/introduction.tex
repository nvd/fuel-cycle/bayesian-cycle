\chapter{Introduction}

In recent years, burning forests and climate change, a global pandemic, and civil wars have often taken precedence over
nuclear threats in every day reporting. But nuclear weapons have not gone anywhere, and they will not in the
foreseeable future. Conversely, various states are still attempting to acquire \gls{nuclearweapon}s or to enhance their
existing inventory. Despite the lower profile of such activities compared to the acute crises of today, the threat
emanating from them should not be underestimated: the probability of a nuclear war may appear small in the present
moment, but its consequences would be devastating and make other crises appear quite manageable.

Occasionally however, states agree to partial or full disarmament in exchange for eased economic sanctions or
similar relief. In this case, the international community needs to be able to verify records and reports about the
nuclear fuel cycle (the processing chain handling nuclear material from mining in the ground to
power generation or weapons production) under consideration, so that no potentially harmful materials or facilities are
left unaccounted for. But being able to reconstruct a fuel cycle can also be interesting for a nuclear fuel cycle
operator aiming to resolve inconsistencies in its own reports; gaps between the accounting information of nuclear
material and the actual inventory are not unheard of, for example in the United States \cite[Ch.\,2]{GFMR2010}.
The capability of establishing a nuclear fuel cycle's operational history and production output
independently of potentially falsified information is therefore vital for disarmament and verification efforts.

More insight into a fuel cycle can be achieved using methods from \emph{nuclear archaeology}, which rely on
measurements of e.g. nuclear waste or facility machinery to reconstruct the operational history of a facility or a fuel
cycle. In this work, the \cyclus~fuel cycle simulation software is combined with statistical inference algorithms in
the \pymc~package to test if the reconstruction of parameters of an entire fuel cycle is feasible; the measurements are
therefore artificial, but remain inspired by what is physically possible to measure in an actual fuel cycle.
Specifically, this means that nuclear waste and depleted uranium are considered, and its isotopic composition is the
main source for information about the fuel cycle which produced it.

The structure of the work follows its title:

\paragraph{Reconstructing Nuclear Fuel Cycles}

The goal of this work is to be able to reconstruct fuel cycle parameters of interest from measurements of the
composition of waste or products originating in a fuel cycle. Nuclear archaeology has been achieving just this for
quite some time, and at a high level (for example, in \cite{Sharp2013}). In contrast, this work attempts a more
``hands-off'' approach by providing a model of the fuel cycle, a number of measurements, and then using established
algorithms to infer quantitative results.

An overview of the physics underlying nuclear fuel cycles and relevant measurements is given in the beginning.

\paragraph{Using Bayesian Inference}

Bayesian inference is the specific method used for reconstruction. It is a well-established method in the
area of inverse problems, i.e. problems of finding parameters for a process which explain a given observation. The
Bayesian method lends itself to such problems, as it can treat prior information (such information which is known about
a process before observing its effects) in a natural way, then use observations (measurements, for example) to improve
on the prior information, and finally calculate probability density distributions for the parameters to be
reconstructed. \cite{Mallick2011}

In this work, a \emph{Markov Chain Monte Carlo} (MCMC) approach is used to solve the problem at hand computationally.
A brief introduction to both the foundations of Bayesian inference and MCMC methods explains the concepts necessary to
understand the remaining chapters.

\paragraph{A First Implementation}

Using Bayesian inference for nuclear archaeology is not a new approach. What is new is the integration of nuclear fuel
cycle simulation software with a probabilistic programming framework. After describing the implementation, this work
will demonstrate the technical viability using various tests. It is shown that -- assuming a sufficiently good fuel
cycle model as well as measurements of limited uncertainty -- the implemented method can reconstruct several fuel cycle
parameters at once.

As this is only a first implementation, no real data has been used yet; instead, the work follows rather theoretical
considerations while keeping the applicability of the developed methods on real-world scenarios in mind. The influence
of uncertainties from the fuel cycle and measurements is discussed, and approaches to taking uncertainties into account
for the reconstruction process are presented.

In principle, the implementation can be used directly for actual applications. However, the uncertainties of
measurements, the fuel cycle structure, and ancillary parameters would likely be extremely high. Those uncertainties
would make it infeasible to use the presented method for reconstructing any but the most simple scenarios. Therefore,
this work should be viewed as a mostly theoretical contribution to the general problem of fuel cycle reconstruction.
