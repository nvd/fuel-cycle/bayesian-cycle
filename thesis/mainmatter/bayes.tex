\mychapter{Bayesian Inference \& Sampling}{bayesianinference}

\mysection{Fundamentals}{sec:bayes:fundamentals}

Giving a full introduction to Bayesian statistics and \gls{inference} methods based on it would be beyond this work's
scope. However, a concise summary of the mathematical foundations required for the following chapters and an
instructional example are given in this chapter -- Bayesian statistics is not as common as the conventional
frequentist approach, after all. The notation is taken from \cite{Gelman2014}.

The underlying theorem is \emph{Bayes' rule}, which in its simplest form expresses the relationship of
\emph{conditional} probabilities of two events $A$ and $B$:

\begin{equation}
\label{eqn:bayes:bayes}
 P(A \mid B) = \frac{P(B \mid A) P(A)}{P(B)} \propto P(B\mid A) P(A)
\end{equation}

The \emph{conditional probability} $P(A\mid B)$ is the probability of event $A$ occurring if there is knowledge about
event $B$ having occurred (or vice versa). However, this thesis is not concerned with coin tossing or urn drawing; the
notion of \emph{events} is not very useful. Instead, the important and more abstract terms in this context are described
in the following. The definitions are adapted from \cite[pp.\,6-8]{Gelman2014}.

\begin{description}
 \item[Parameters] $\vec \theta$, which describe a physical process quantitatively. $\vec \theta$ is a vector,
and its components are the different parameters of a process.

 \item[Observations] $\vec y$, taken from that physical process; usually one or more measurements of one or another
quantity. More than one $\vec y$ can exist, for example if multiple measurements exist.

 \item[Joint Distribution] $p(\vec y, \vec \theta)$, the probability distribution of both observations and
parameters. It gives the absolute probability of a given observation and a given parameter occurring simultaneously. It is
the product of the prior distribution $p(\vec \theta)$ and the likelihood $p(\vec y \mid \vec \theta)$, but usually not very important
in the application of Bayesian inference: \begin{equation}\label{eqn:bayes:joint} p(\vec y, \vec \theta) = p(\vec
\theta) p(\vec y \mid \vec \theta)\end{equation}

 \item[Prior Distribution] $p(\vec \theta)$, describing an estimate or belief of a parameter's probability
distribution (or \emph{density}). \emph{Prior} means that this estimate is
established before any observations have come into play.

 \item[Prior Predictive Distribution,] the joint distribution $p(\vec y, \vec \theta)$ marginalized over $\vec \theta$
(i.e., the probability of $\vec y$ without considering $\vec \theta$); it is the expected distribution of
measurements when assuming the prior. Defined as \begin{equation}p(\vec y) = \int p(\vec y, \vec \theta) \dd
\vec \theta
\end{equation}

\item[Likelihood] $p(\vec y \mid \vec \theta)$, proportional to the probability an observation $\vec y$ if $\vec \theta$
is the true parameter vector of the process. The likelihood is ``proportional'' to the probability, because it is not normalized to
1 and therefore not a true probability.

 \item[Posterior Density/Distribution] $p(\vec \theta \mid \vec y) \propto p(\vec \theta) p(\vec y \mid \vec \theta)$,
the result of any Bayesian \gls{inference} calculation. Informally phrased as \emph{``posterior is prior times
likelihood''}, it incorporates prior assumptions and measurements to give a function proportional
to the probability distribution of parameters $\vec \theta$. It is generally not (yet) normalized to a total
probability of 1, although this can be achieved easily. Note the similarity to Bayes' rule \autoref{eqn:bayes:bayes}:
the lack of the denominator causes the lack of normalization. $p(\vec y)$ is usually not known and difficult to
calculate, but constant -- this means it isn't relevant for the desired goal. The difference to the joint distribution
in \autoref{eqn:bayes:joint} is that the observations $\vec y$ are known and fixed.

 \item[Posterior Predictive Distribution] $p(\vec y' \mid \vec y) = \int p(\vec y'\mid \vec \theta) p(\vec \theta
\mid \vec y) \dd \vec \theta$, incorporating both the prior belief and observations, and makes a prediction about
future observations. It is the expected distribution of future measurements taking into account the observations found
in so far.
\end{description}

In this work, these are the specific realizations of the abstract definitions above:

\begin{description}
 \item[Observations] are vectors of usually more than one of
 \begin{itemize}
  \item Isotopic concentration (mass concentration)
  \item Isotopic abundance (ratio of two isotopes' concentrations)
  \item Mass of a kind of material
 \end{itemize}

 \item[Parameters] are characteristics of the fuel cycle, such as the cycle time or enrichment grade.

 \item[Prior Distributions] are usually uniform distributions, as there is no prior knowledge assumed except for a
valid range for each parameter. For example, the cycle time may be assumed not to be longer than \SI{120}{\day} and not
shorter than \SI{20}{\day}. In this case, the posterior distribution is exclusively given by the likelihood;
only its valid range is determined by the limits of the uniform prior. Depending on the scenario, ``informed priors''
can be used as well: a normal distribution, for example.

 \item[Posterior Distributions] are calculated for parameters, in order to make statements such as: \emph{``Given a
measurement $\vec y$, the cycle time was between 58 and 62 days with a \SI{90}{\percent} chance.''}.
\end{description}

In the special case of flat or uniform priors, as are used in this work, the definition of the \emph{posterior
distribution} implies that the probability distribution of parameters is proportional to the likelihood of their
respective resulting measurements. Therefore the likelihood plays a central role in the implementation of a Bayesian
inference process.

\mysection{Software}{sec:bayes:software}
\subsection{Probabilistic Programming}

This work relies on the computational implementation of the Bayesian principles explained before. The main
goal, as discussed in the previous section, is to reconstruct the probability distribution for the parameter vector
$\vec \theta$, conditioned by the observations and priors. In principle, such a distribution could be calculated by a
simple computer program sampling points in the parameter space deterministically, e.g. with linear spacing; then for
each sampled parameter vector the relative probability is simply the product of prior probability and likelihood.
However, this is computationally expensive for parameter spaces with many dimensions, and is therefore not always a
good option. Instead, clever algorithms can reduce computational time spent on regions of low likelihood in the
parameter space, and instead concentrate on the regions of interest. These algorithms combine random sampling methods in
a combination with \emph{Markov chains}, and thus the general principle is called ``MCMC'', for ``Markov Chain
Monte Carlo''.

These algorithms are also freely available in many different software packages for different software ecosystems; in
this work, \pymc~for the Python programming language is used. The latter is used in this work for \gls{inference}
purposes, and will therefore be the focus of the remaining parts of this chapter, including the example in the following
section.

These frameworks for probabilistic programming often use similar algorithms and also do not differ much in how they are
used. The principle of probabilistic programming is that instead of conventional variables, \emph{random variables} are
introduced. A random variable is described by a probability distribution and doesn't assume a single value. Based on
random variables, a model can be built and evaluated in a sampling step, resulting in the desired posterior probability
distribution.

\subsection{Sampling Algorithm}

Markov chains are series of random values with a special property: for a series of random variables $x$, each
variable $x_i$ only depends on $x_{i-1}$, the value of the random variable directly preceding it. For example, a random
walk where each step is determined using a normal distribution with the mean equal to the previous step constitutes a
Markov chain. When used in specific ways, as for sampling a Bayesian problem, Markov chains can be made to converge to
a probability distribution which is to be determined; remember that this is the problem formulated above.

The earliest algorithm making use of Markov chains for approximating probability distributions was the \emph{Metropolis
algorithm}, originally devised for use in thermodynamics where the goal was simulating systems behaving according to a
Boltzmann distribution (for example, the Ising model of paramagnetism). This algorithm was later generalized to become
the \emph{Metropolis-Hastings algorithm}, with the ability of drawing samples from arbitrary probability distributions.
The goal in the context of this work and Bayesian \gls{inference} is therefore to construct a Markov chain whose
samples, in the stationary case (i.e. when the influence of any start conditions have become negligible), approach the
posterior distribution $p(\vec \theta \mid \vec y)$.

The Metropolis-Hastings algorithm (MH) is an intuitively simple algorithm, which shall be quickly
outlined here. This serves as an example for understanding the sampling process of a fuel cycle model, i.e. the core
part of this thesis; while it is \emph{possible} to use MH for solving the problems that this work is
concerned with, it is neither efficient nor precise to do so. Other, more advanced algorithms are used instead, for
example the \emph{Slice} algorithm implemented by \pymc. The principle remains the same, though, and understanding
this simple algorithm is helpful for understanding the methodology introduced later.
In order to draw samples from a posterior distribution, MH proceeds like this:

\begin{enumerate}
 \item Draw a random start parameter combination $\vec \theta_0$ from the prior distribution $p(\vec
\theta)$. This is simple, as well-known (and in the specific case of this thesis, uniform) distributions are used as
prior distributions.
 \item Repeat for step $i$ of $n$ steps in this \emph{chain} (as in Markov chain):

 \begin{enumerate}
\item Propose a new parameter set $\vec \theta_i'$, sampled from the \emph{jump distribution}, a distribution
 parameterized by $\vec \theta_{i-1}$. The proposal is given by the algorithm, and is usually not informed by
 the problem at hand. The jump distribution takes into account distance within the parameter space, in order to
 facilitate progress of the chain and cover the parameter space efficiently.

  \item Decide whether to accept the new parameter set by calculating the ratio of conditional probabilities: \[q =
p(\vec \theta_i' \mid \vec y) / p(\vec \theta_{i-1} \mid \vec y)\] Recall that $p(\vec \theta \mid \vec y) = p(\vec
\theta) p(\vec y \mid \vec \theta)$; this implies that the ratio of probabilities is equal to the ratio of likelihoods
if $p(\vec \theta)$ is a uniform distribution.\footnote{To be precise, the MH algorithm also factors in the
jump distribution for the acceptance probability; this matters only if the jump distribution is asymmetric, however.}

    \begin{enumerate}
    \item If $q > 1$, i.e. the new parameter set is more likely than the previous set $\vec
    \theta_{i-1}$: accept the new parameter set $\vec \theta_i := \vec \theta_i'$, and continue with 2.a.
    \item If $0 < q < 1$, draw a random number $s$ from a uniform distribution between 0 and 1.
    \begin{enumerate}
        \item If $s < q$: accept the new sample; set $\vec \theta_i :=\vec \theta_i'$ and continue with step 2.a for
sample $i+1$.
        \item Otherwise: Do not accept the new sample, and go to step 2.a again, setting $\vec \theta_i = \vec
\theta_{i-1}$.
    \end{enumerate}
    \end{enumerate}
    \end{enumerate}
\end{enumerate}

The essential part is step 2.b, where  the algorithm prefers samples from areas in the parameter space which have a
higher probability of generating a given observation (i.e., higher likelihood). However, because not only the most
likely events are possible, less likely samples will be drawn as well. This is achieved by probabilistically
accepting such samples with a probability proportional to their relative likelihood in step 2.b.ii. By repeating this
algorithm in multiple, long chains, the quality of the inference result is improved; a single chain starting from one
start value may not be able to cover the entire parameter space, despite precautions taken against this. The sampling
implementation in this work sets a different start value for each Markov chain, which, in addition to the different
random seed, helps covering the entire parameter space even if there are multiple areas of high likelihood.

Frequently, a number of samples drawn right after the start of the chain are discarded; this is called ``tuning'' and
is justified with the influence of the start value on the overall chain. After discarding initial samples, the chain is
supposed to be returning samples from a stationary distribution which is independent of the start value. Finally, there
are even more refinements not shown here, such as dynamically changing the acceptance probability depending on the
chain's behavior.

The result of running this algorithm is a set of samples $\vec \theta_i$ ($i \in 1..n$) distributed
according to the underlying probability distribution $p(\vec \theta \mid \vec y)$. This fact can be proven
mathematically, and is useful because it allows obtaining such a set of samples without knowing the true
underlying probability distribution. From the samples, a multivariate probability distribution for all parameters can
be estimated, for example using the methods shown in \autoref{fig:bayes:numint}. \cite[Ch.\,11]{Gelman2014}

 \begin{figure}
 \centering
 \includegraphics[width=.5\textwidth]{figures/bayes/mh_randomwalk_sinemvar.pdf}
 \caption[Metropolis-Hastings random walk]{Metropolis-Hastings random walk over a bivariate distribution ($p(x, y)
\propto \sin(x) \sin(y)$) showing
1000 samples: high-likelihood areas (yellow) receive more samples, but transitions between different such areas
    are unlikely and can lead to disproportionately few samples drawn from other areas; the start value was located in
the bottom left hotspot. This causes that area to receive the highest number of samples.}
 \label{fig:bayes:mhwalk}
 \end{figure}

 \begin{figure}
 \centering
 % maybe move towards the algorithm section?
  \includegraphics[width=.6\textwidth]{figures/bayes/npp_mc_integration.pdf}
  \caption[Numerical integration of a standard normal distribution]{Numerical reconstruction by integration of a
standard normal distribution's \gls{pdf}, using a
trivial Monte Carlo method with 1000 samples. \emph{Top}: the distribution's probability density function in green, and
blue random samples within its area. \emph{Bottom}: reconstructed density function calculated by differentiating the
number of samples with respect to $x$ (\emph{num. diff.}), respectively \emph{kernel density estimation} (\emph{KDE}), a
technique for reconstructing a probability distribution from a set of samples \cite{SciPyKDE}.}
  \label{fig:bayes:numint}
 \end{figure}

There are some inherent difficulties of this technique; for example: multi-modal distributions (i.e., distributions with
more than one peak, separated by areas of low likelihood) are unlikely to be sampled well; depending on the algorithm,
a high number of samples is necessary. Furthermore, the calculation of the likelihood function required in step 2.b can
be expensive -- in this work, it involves running an entire fuel cycle simulation, and thus a high amount of computing
resources is required. Lastly, different chains can converge to different distributions depending on the initial value
of the chain, although this can be used to our advantage by randomizing start values and therefore covering the
parameter space at least stochastically so as to not miss a significant peak (high-likelihood area) when sampling. See
\autoref{fig:bayes:mhwalk} for a visualization of a random walk in an especially difficult probability distribution.

Finally, it must be reiterated that the main part of this work does not employ Metropolis-Hastings as sampling
algorithm. Even if it works with a sufficient number of samples, it has resulted in subpar results compared to other,
more advanced algorithms. \cite[Ch. 11]{Gelman2014}, \cite{salvatier2016probabilistic}

\mysection{Instructional Example}{bayes:exampleref}

A simple example of a Bayesian inference problem solved using \pymc~is presented in \autoref{ch:app:bayes}.
