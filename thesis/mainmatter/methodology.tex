\mychapter{Methodology}{methodology}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mysection{Overview}{methodology:overview}

The process of Bayesian inference was described in \autoref{ch:bayesianinference}; the following components and
methods have been developed in this framework. First, it is necessary to describe the difference between a ``pure''
Bayesian model as described in the chapter on that topic, and the so-called \gls{blackbox} model used for the inference
of fuel cycle parameters.

In the ``pure'' model, a set of static observations is used to assign likelihoods to randomly drawn samples. More
samples are drawn from areas of higher likelihood, allowing to model the underlying probability distributions. The
calculation of the likelihood $p(\vec y \mid \vec \theta')$ of the observation for a sample $\vec \theta'$ (power,
efficiency) is straight-forward for simple models, for example linear or polynomial relationships between sampled
parameters and the observations.

In contrast, a fuel cycle can not be simulated within a few lines of Python code. To assign
a likelihood $p(\vec y_0 \mid \vec \theta')$ to a sample $\vec \theta'$ (input parameters to a fuel cycle
model), the fuel cycle simulation must be run first. Specifically, the fuel
cycle model is used to predict an observation for the sample. The likelihood is calculated by comparing the
sample's observation with the real world observation. In this work, no observation from the real world is available;
therefore a synthetic ``true measurement'' is generated from the \gls{groundtruth} for each experiment.

The fuel cycle simulation needs to be integrated with the \pymc~model in a way that enables
\pymc~to infer the unknown parameters. Additionally, a sensible way of assigning likelihoods to samples needed to be
developed. The general problem is not new, and has been solved for other applications. Called a \emph{black box
likelihood}, the missing part is a \gls{blackbox} likelihood function. This function, run by
\pymc~for each sample, needs to first convert a sampled parameter vector into a configuration understood by \cyclus,
then run a fuel cycle simulation using these parameters, read and interpret the simulation result, and finally
calculate a likelihood for the considered sample. The last step decides how well a specific sample
explains the real world observation of isotopes. Most challenges in this part of the work have been of technical nature,
and will be described in more detail in the following sections. \cite{Pitkin2018}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mysection{Components}{methodology:components}

There are three logical components making up the software developed in this work:
\begin{enumerate}
 \item \cyclus~is the simulation framework. It evaluates a fuel cycle for many different parameters (samples) during
the inference process, and is assisted by the \cd{miso\_enrichment} library for accurate physical calculations.
\item A small \cd{blackbox} library encapsulates the \cyclus~functionality and makes its results easily consumable by
the remaining code. A tiny \cd{cyclus\_db} module is responsible for extracting simulation outputs from
the simulation results provided by \cyclus.
\item A \emph{driver program}, uninspiringly called \cd{run.py}, combines the functionality of part (2) with the
inference algorithms provided by \pymc. It is capable of massive parallelism when run on a \gls{hpc} cluster, but can
also be run as a single instance on a single machine for testing and debugging purposes.
\end{enumerate}

In addition to these three major components, a number of auxiliary tools and scripts are used to process and analyze
inference results. An \emph{experiment} in the context of this work means running the inference process for a set of
\emph{target values}, and \emph{\gls{hyperparameter}s}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mysection{\cyclus}{methodology:cyclus}

\cyclus~is an agent-based nuclear fuel cycle simulator. It receives a fuel cycle specification, and simulates the
described fuel cycle in discrete time steps, with a focus on the flow of nuclear material. For example,
\cyclus~simulates the enrichment of uranium before its introduction into a reactor; the irradiation of fuel and the
resulting power production in a reactor; the subsequent reprocessing or selling of spent fuel; and the production of
fissile or weapons-grade materials. \cyclus~is very flexible, and can be extended in many ways. This flexibility is a
result of the agent-based nature of \cyclus: each facility, whether nuclear power plant or enrichment facility, is
represented by an \emph{agent}. The agent can trade with \emph{commodities} in order to fulfill its task; for example,
an enrichment facility needs to purchase natural uranium, and will offer enriched uranium on a virtual marketplace. The
agents themselves are \gls{blackbox}es and don't interact with each other except through exchange of \emph{commodities}
(usual nuclear material), which reduces the complexity of simulating a fuel cycle. Another way that \cyclus~manages to
keep the simulation process comparatively simple is by using an \emph{tick-tock} schedule; for each time step of
\SI{1}{\day}, each agent has two opportunities to react to the current simulation state and complete work: once before
the step (\emph{tick}), and once after (\emph{tock}). Between the tick and the tock, a \emph{resource exchange} occurs,
in which agents can exchange resources. For example, a nuclear power plant may unload its reactor in the \emph{tick}
phase, offer spent fuel and obtain fresh fuel in the resource exchange phase, and add new fuel in the \emph{tock} phase.
Agents can also enter or leave a fuel cycle before respectively after a time step. This work doesn't make use of this
feature, and all agents exist for the entirety of the fuel cycle. \cite{HUFF201646,CyclusCEP20}

Agents are either built-in, defined as prototypes by an accompanying library called \textsc{Cycamore}, or can be
custom-built. In addition to these two kinds of agents, the fuel cycle simulated here also makes use of a library
called \cd{miso\_enrichment} written by Max Schalz. \cd{miso\_enrichment} provides an agent prototype called
\cd{MIsoEnrich}, modelling a multi isotope enrichment facility in a physically correct way. It also provides another
prototype called \cd{GprReactor}, which provides a reactor simulation. The specific implementation is based on prior
work expressing the reactor's characteristics using \gls{gpr} models, by first simulating the reactor in \serpent~and
interpolating from the results of that simulation \cite{Antonio2019}. Because the simulation itself is time-consuming,
results are stored as arrays (``kernels'') which are then used by the \cd{miso\_enrichment} library. Using these two
agents is essential to this work, as the predefined agents are not able to provide these physically accurate
calculations; the agent prototypes coming with \cyclus~are more focused on the interactions between agents, so
that many scenarios requiring a more physically accurate simulation require specialized implementations.
\cite{Schalz2020, MisoEnrichment}
This section doesn't offer enough space to cover all remaining details of \cyclus~(which can be found in
\cite{HUFF201646}).

The fuel cycle in this work makes use of the \cd{Sink, Source, Storage, Separations, Reactor} prototypes exported by
\textsc{Cycamore}, and the two agents from \cd{miso\_enrichment} described in the previous paragraph.

The simulation configuration is a \textsc{json} document containing a structured specification of all agents within the
fuel cycle, and agent-specific parameters. A new specification is generated for each
\cyclus~invocation by a Python module. This makes it easy to update parameters either for different experiments or in
the course of sampling a specific fuel cycle (each sample requires different parameters; more on this below).
\cyclus~simulates the fuel cycle specified in its input, and generates an \textsc{\gls{SQLite}} database file
containing the entire simulation state as of the last time step. In this work, the database file rarely exceeded
\SI{400}{\kilo\byte}, and the run time of the simulation remained below \SI{2}{\second} on the \gls{hpc} nodes used for
the inference process. This relatively low use of resources allows running inference attempt within relatively small
time frames (less than one day) using a limited amount of computing resources. The simulation time is impacted by
increasing fuel cycle complexity and prolonged simulation time. The inference time also requires more simulation runs
for more unknown parameters being inferred.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mysection{\cd{blackbox}: From Simulation Inputs to Likelihoods}{methodology:blackbox}

\begin{figure}[h]
 \centering
 \includegraphics[width=0.8\textwidth]{figures/methodology/software.pdf}
 % software.pdf: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption[Structure of developed software]{A rough overview of the sampling software's internal structure.}
 \label{fig:methodology:software}
\end{figure}

The \cd{blackbox} module is responsible for interfacing with \cyclus~and calculating the observations for a
parameter sample; the sample's observations are then used for calculating the likelihood.

For interfacing with \cyclus, the \cd{CyclusCliModel} class was implemented. There are essentially two ways of
interfacing: either using the Python library coming with \cyclus, or executing the \cd{cyclus} \gls{binary} from the
inference process. The former option was originally preferred, but doesn't work in some scenarios enforced by \pymc:
for example, a parallel execution from the same process was difficult to achieve. In addition to that, simulation
result files were often left on the file system, becoming a problem when tens of thousands of
simulations are executed consecutively. Instead the latter approach was chosen eventually, and running the binary from
within the script turned out to be more robust than using the original Python library.

The biggest challenge when running child processes is handling errors reliably, cleaning up properly,
and encapsulating the behavior well for use by other code. Python's built-in \cd{subprocess} library is responsible
for running the binary. Simulation input files are staged on a temporary \gls{filesystem} (similar to \cd{/tmp} on
\textsc{Unix} systems), and output files are written to that same file system. Care is
taken to avoid leaving a large number of outdated files by removing input files after \cyclus~has completed, so that
even very long inference processes run reliably.

As shown in \autoref{fig:methodology:software}, the abstract \cd{CyclusCliModel} class is intended to be subclassed. In
the driver program, which is responsible for the overall inference process execution, a subclass inherits the simulation
execution capabilities. The subclass additionally implements the extraction of relevant information from the
simulation output, specifically isotope concentrations and waste masses.

The major part in the \cd{blackbox} module is, however, the \cd{CyclusLogLikelihood} class; it is implemented as
\textsc{Theano} operator (\textsc{Theano} is a framework for compiling mathematical Python expressions to efficient
machine code; it is an implementation detail of \pymc) which is used by \pymc~during the inference process. It
encapsulates the entire likelihood calculation functionality: first, it converts a sampled set of parameters into a
simulation input file, then invokes \cyclus, extracts the relevant measurements using a \cd{CyclusCliModel}, and
calculates the likelihood of the sample by comparing the new measurements with the reference measurements obtained from
the ground truth; a \cd{LikelihoodFunction} instance is responsible for the latter part. This class is
subclassed by a class which has a precise understanding of the results returned by a matching \cd{CyclusCliModel}. The
result type is implemented as simple data structure (\cd{namedtuple}).

In order to avoid duplicate calculations, \emph{memoization} is implemented by the \cd{CyclusLogLikelihood} class. The
likelihood for a parameter sample is stored in a lookup table, and returned without invocation of \cyclus~if the
sampling algorithm is trying to obtain the likelihood for the same parameter set later (the reason for this is unclear).
For some runs, \SI{17}{\percent} of \cyclus~simulations could be skipped (out of a total of more than $10^3$ simulations
for each chain), each of which would have taken up to three seconds.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mysection{Driver: Where Inference Happens}{methodology:driver}

The driver program is in some aspects the central result of this work: it produces inference
results, which are in the form of a collection of samples. The set of samples is converted into an estimated
probability distribution (posterior probability distribution).
To achieve this, the \cd{blackbox} module and \pymc~as the statistical core are combined. The general program flow is
quite simple:
\begin{enumerate}
 \item Parse command line flags, initialize logging and derive random seeds.
 \item Build a \pymc~model by reading an experiment specification from special files.
 \item Start sampling the model, intermittently saving traces (i.e., sample collections) to a durable storage space.
\end{enumerate}

Directly after the program has started execution, a log file is opened and all of the process output is sent to it.
Using both the job ID assigned by the cluster management system and each instance's serial ID creates unique log files
for every single execution. The log files can be archived for later inspection in case of bugs or inference
problems. They also contain detailed information about the inference progress and calculated results. Extracts from log
files will be used below to show that the program works as intended.

\Gls{randomseed}s are derived from a universal base seed (which is set to \num{12345}), to which the instance ID is
added to produce the base seed. This guarantees that all jobs with the same instance ID will share the same random
seed. From the base seed, individual seeds for each Markov chain are derived, as well as auxiliary seeds (such as
randomized start values for the individual chains, or for the \emph{jitter} method described in
\autoref{sec:application:jitter}). Randomized start values are important, as the start value influences the inference
result significantly in the case of multi-modal distributions: the start value can then determine which local maximum
is sampled by a chain. Seeding the inference process like this has proven reliable and enabled reproducible results.

In the next step, the Bayesian model is built. The information required is read from two \textsc{json} documents; one
contains the ground truth (i.e., the parameter values used to generate the reference composition, and which are
expected to be inferred correctly by the inference process), and another one contains the sampling specification,
meaning the type of prior distribution and its parameters. Using uniform, normal, and truncated normal distributions
for priors is supported.

From the ground truth, a \emph{true measurement} is generated by performing a \cyclus~simulation using the specified
parameters; an instance of \cd{ElboniaCyclusModel} is responsible for all \cyclus~invocations. (If this method were
used to infer properties of a real-world fuel cycle, the true measurement would come from physical measurements
instead. Lacking these, a synthetic true measurement is generated here.) The simulation results in an object containing
isotope concentrations for all sinks configured in the command line arguments. Additionally, an \cd{IsotopeLikelihood}
instance is created; it is also described in \autoref{fig:methodology:software} and is solely responsible for
calculating a likelihood from a parameter sample's observation and the true measurement generated in the first step. By
sharing common code in base classes, different inference methods can be implemented with little
code duplication.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Likelihood Calculation}
\label{sec:methodology:likelihood}

After running the simulated fuel cycle for a drawn sample, measurements are extracted from the simulation output: a few
\textsc{SQL} queries result in the isotopic compositions and total masses of a specified material kind, e.g. depleted
uranium (as different sample origins can support reconstructing different parameters, the sampled materials are
specified for each run). From the measurements, a likelihood needs to be calculated. The calculation of a sample $i$'s
likelihood is quite simple in principle, occurs in the \cd{IsotopeLikelihood} class, and roughly follows the presented
flow. Here, the true measurements are compared to the measurements simulated for the sample $i$.

\begin{enumerate}
 \item Initialize variables for collecting the log likelihoods: \[L_\text{mass} := 0, L_\text{concentration} := 0,
L_\text{abundanceRatio} := 0\]
 \item If masses $m$ are to be measured (this can be configured), continue with 2.a. for each sink $j$ under
consideration.
 \begin{enumerate}
  \item Calculate the $\sigma$ used for the likelihood calculation: $\sigma := \text{rel. sigma} \times m_{0,j}$. The
\emph{relative sigma} is an important hyperparameter used in inference, and set to \SI{1}{\percent} for most
experiments shown in \autoref{ch:application}. However, larger values may be needed if measurements carry a high
uncertainty. A comparison of inference results for different values of the relative sigma is discussed later in this
work.
  \item Normalize the mass measured for this sample to be on a standard normal distribution with the true measurement
$m_{0,j}$ at the center: \[x = \frac{m_{i,j} - m_{0,j}}{\sigma},\]
  \item Using the standard normal distribution's \gls{pdf} $\phi_0$, calculate \begin{equation}\label{eqn:lnnormal}
\ln{\phi_0(x)} :=
\ln( \frac{1}{\sqrt{2\pi\sigma^2}} \exp(-\frac{x^2}{2\sigma^2})). \end{equation} Add the result to
$L_\text{mass}$.
 \end{enumerate}

 \item If abundance ratios are to be measured\footnote{This will be the case in all scenarios presented later, as
abundance ratios are the natural measurement produced by mass spectrometry.}, continue with 3.a. for each sink $j$. The
specific abundance ratios to use are specified as a command line parameter.
 \begin{enumerate}
  \item For each configured abundance ratio $k$, check if both isotopes $k_1, k_2$ are present in the measurement.

  \begin{itemize}
    \item If at least one isotope is not present in the measured composition, assume a simulation failure and add
$-\infty$ to $L_\text{abundanceRatio}$. This is equivalent to assigning a likelihood $p(\vec y \mid i) = 0$ to the
sample $i$. Sometimes, simulation failures occur due to parameter combinations that exceed the limits of \gls{gpr}
models, or produce otherwise nonsensical output. It is important to give appropriate feedback to \pymc~about such
``bad'' parameters.
  \end{itemize}
  \item Calculate the abundance ratio $r_{i,j,k} = c_{{k_1}}/c_{{k_2}}$ of the two isotopes for the sample's
measurement, and similarly the abundance ratio for sink $j$ of the true measurement, $r_{0,j,k}$.
  \item Again, treat both numbers naively like the mass above: calculate the $\sigma$ to use by multiplying the true
measurement's abundance ratio with the relative sigma value:
\begin{equation}\label{eqn:methodology:abundancerelsigma}\sigma := \text{rel. sigma} \times r_{0,j,k}\end{equation}
  \item Then, normalize the sample's measured abundance ratio as \[x = \frac{\text{abundance ratio} - \text{true
abundance ratio}}{\sigma}.\]
  \item Calculate the log likelihood $\ln{\phi_0(x)}$ as in \autoref{eqn:lnnormal} and add it to
$L_\text{abundanceRatio}$.
 \end{enumerate}

 \item If absolute concentrations are to be measured (which is mutually exclusive with abundance
ratio measurements), follow the same steps as the likelihood calculation involving abundance ratios, but use
concentrations instead. The concentrations returned by the simulation add up to 1; the relative sigma
is again used to calculate an uncertainty proportional to the concentration.

 \item Finally, return the total log likelihood $L_\text{mass} + L_\text{concentration} + L_\text{abundanceRatio}$.
\end{enumerate}

Summarized in one sentence, this algorithm compares all of a sample's measurements with the true measurement obtained
from the ground truth, and returns the log likelihood, which is the higher the closer a measurement is to the true
measurement. A log likelihood of $-\infty$ corresponds to a likelihood of 0, and higher log likelihoods therefore to
non-zero likelihoods. Because a likelihood is not a probability, the log likelihood can assume values $> 0$
(corresponding to likelihoods $>1$).

The relative sigma hyperparameter introduced in step 2.a is a very simple way of adapting the assumed uncertainty of
measurements and the underlying model. In this work, it has been constant in each experiment, but in advanced scenarios
it makes sense to adapt this value on a per-measurement basis in order to give more importance to those measurements
considered accurate while still being able to utilize measurements considered uncertain. Depending on the measurement
method, it would also make sense to implement a specific uncertainty calculation; for example, in a mass spectrometer,
an abundance ratio $r_{a,b} = n_a/n_b$ has an uncertainty of
\begin{equation}
 \qty(\frac{\sigma_r}{r_{a,b}})^2 = \qty(\frac{\sigma_{n_a}}{n_a})^2 + \qty(\frac{\sigma_{n_b}}{n_b})^2
\end{equation}
which can be modeled using a Poisson distribution, i.e. $\sigma_{n_i} = \sqrt{n_i}$ with the respective particle count
$n_i$ for the $i$-th kind of particle. This is only one approach to treating measurement uncertainties however; the
following chapter will discuss additional possibilities.

The result of this algorithm is then used to inform \pymc~of the likelihood of any chosen sample. By repeating this
process for thousands or tens of thousands of samples, \pymc~will be able to approximate the posterior distribution of
the unknown parameters.

\subsection{On Suitable Measurements}
\label{ssec:methodology:suitable}

For the likelihood calculation to work well, it is helpful if observed measurements are bijective for each parameter;
for any two values of one parameter, two different measurements should be observed. Examples for measurements
fulfilling this are shown in \autoref{fig:methodology:uisoconc}. For the calculation of the cycle time, for example,
plutonium concentrations are of interest; the reason can be seen in \autoref{fig:methodology:pu240sepwaste}, which
shows a very clear dependency of plutonium isotope concentrations from the cycle time. Other isotopes can be used as
well, however; this depends on which isotopes are simulated and are available in the considered sample material.

The data for these figures was generated using the same \cyclus~model used for sampling, by covering the parameter
space in a linear way.

\begin{figure}[h]
 \centering

\includegraphics[width=\textwidth]{figures/application/prodenrich/prodenrich_depleteduraniumsink_concentrations.pdf}
 \caption[Uranium isotopes' concentrations by product enrichment grade]{Concentrations by isotope for uranium in the
depleted uranium stock for different product enrichment grades. Sampled at a cycle time of \SI{61}{\day} and
\SI{1.08}{\percent} fresh fuel enrichment, and \emph{default parameters} otherwise.}
 \label{fig:methodology:uisoconc}
\end{figure}

\begin{figure}[h]
% this can be kept (but possibly moved): it shows how well Pu serves to infer cycle time.
 \centering
 \includegraphics[width=0.66\textwidth]{figures/application/cycletime/separated_waste_sink_pu_concs_by_cycletime.pdf}
 \includegraphics[width=0.33\textwidth]{figures/application/cycletime/separated_waste_sink_pu_240_ratio.pdf}

 \caption[Pu isotope concentrations by cycle time]{\emph{Left and center}: Concentrations of plutonium isotopes as
functions of cycle time relative to total plutonium content in separated waste. \emph{Right}: Abundance ratio of
\plutonium{40} and \plutonium{39} as function of cycle time (mass concentration ratio; obviously very similar to the
center plot because the concentration of \plutonium{39} changes very little). Sampled at \SI{90.8}{\percent} product
enrichment and \SI{1.08}{\percent} fresh fuel enrichment.}
 \label{fig:methodology:pu240sepwaste}
\end{figure}

\subsection{Sampling}

The likelihood calculation is used by \pymc~to reconstruct the probability distribution of the unknown
parameters. For ``pure'' examples, as the one shown in \autoref{ch:bayesianinference}, \pymc~makes use of
\gls{ad} in order to reason about the probability distributions analytically; for example, by taking
the gradient of the likelihood with respect to the sampled parameters. This is not feasible using an
external model like \cyclus.\footnote{Implementing a fuel cycle simulation, possibly simpler than \cyclus,
from scratch in a language more suited to AD -- either Python using TensorFlow, or the Julia programming language -- it
would be possible to differentiate the fuel cycle and more efficiently reason about it in a Bayesian inference
framework.}
Therefore, \pymc~cannot use its most advanced sampling algorithm \textsc{NUTS} (\emph{No U-Turn Sampler}), a variant of
\textsc{HMC} (\emph{Hamiltonian Monte Carlo}). Instead, there is a choice left between classic
\emph{Metropolis-Hastings} (MH; as described in \autoref{ch:bayesianinference}) and a more clever adaptive slice
sampling algorithm (\textsc{Slice}, in the following). Comparing results from both algorithms has shown very clearly
that MH does not give useful results within a reasonable number of samples, and therefore is not used for any analysis
presented in this work, not even for a comparison.
\cite{salvatier2016probabilistic}

An attempt was made at differentiating the fuel cycle model by using finite differences. This is implemented as
additional \textsc{Theano} operator, providing a gradient method in the parameter space. Having such an operator
theoretically enables using the \textsc{NUTS} sampling algorithm. However, trials of this algorithm resulted in
an extremely low sample rate and non-satisfying inference results compared to the \textsc{Slice} algorithm. Therefore,
the gradient implementation remained inactive for the analyses discussed further below.\footnote{The algorithm used for
it is published as \href{https://gist.github.com/dermesser/ce70f177ff5e17fa06d548d1325f6b5d}{GitHub Gist}.}

\subsection{Simulation at Scale}

The main challenge in the sampling process is the high amount of compute power required by the fuel cycle simulation.
Whereas a standard model can draw tens of thousands of samples within seconds, a single run of \cyclus~using the already
simplified fuel cycle can take several seconds already. Therefore, it is important to
\begin{itemize}
 \item run many Markov chains in parallel, on one or more machines;
 \item take only as many samples as are needed to obtain a reasonably accurate reconstruction of the unknown parameters;
 \item save intermittent state as frequently as possible and necessary to protect against crashes or the driver process
being interrupted by the \gls{clustermanager}.
\end{itemize}

The calculations are split into many \emph{instances}, each of which samples one chain.\footnote{To be strictly
correct, there are two modes implemented: either sample one chain in steps of one sample, or sample multiple chains in
parallel in each instance. The former method has proven to be more reliable, and was therefore used for all relevant
experiments.}

Generating one sample takes more than one simulation execution, depending on the sampling algorithm. The \textsc{Slice}
algorithm employed here usually needs to run less than ten simulation executions when inferring two parameters, but
this number increases with additional parameter space dimensions.

Each instance saves the incomplete state every 100 to 200 samples to a \gls{netcdf} file on a shared cluster file
system, where it can be inspected later. It was found that for 10 to 15 chains, even as few samples as 200 per chain
can be enough to reach a chain equilibrium, resulting in a reliable reconstruction of parameters.

\subsection{Verification of Desired Behavior}

With all components in place, it is imperative to verify that the ensemble behaves as intended. Of course, obtaining an
inference result predicting the ground truth to a good accuracy confirms that the overall set-up does work; but it is
important to consider the low-level details of the inference process as well. This is even more relevant if the
inference process doesn't behave as expected; for example, if it doesn't converge or reconstructs values different from
the ground truth.

In order to gain such introspection, logs generated by the driver program can be inspected. An example with removed
timestamps is shown below. The raw text has some structure, reflecting the flow
explained in the previous section:
\begin{enumerate}
 \item \emph{Mutate} parameters proposed by the sampling algorithm, and generate the simulation input file.
 \item \emph{Run} the \cd{cyclus} binary using the generated input file.
 \item Extract and calculate the \emph{likelihood} of the sample.
\end{enumerate}

In addition to the details of the sampling process, logs also contain the exact configuration used for running an
experiment; including random seeds, command line arguments, models, and priors. Each experiment run is also linked by
its job ID to a specific commit in the version control repository, allowing inspection of every single change made
between different experiments.

A partial log is shown here:
\begin{verbatim}
:: Mutating: using parameters {'cycle_time': 60.54414698146056, 'high_enrichment_grade':
89.31954765375376}
:: Running cyclus: ['cyclus', '-i', '/w0/tmp/slurm_vf962887.22555573/tmplvo5pifz.json',
'-o', '/w0/tmp/slurm_vf962887.22555573/tmps589pcpf.sqlite']
:: Cyclus run finished!
:: Likelihood for isotopic concentration {92234: 2.7397363493108365e-09, 92235:
0.0017213651169342577, 92236: 0.00011779593200079968} is -2.879820151813105
:: Mutating: using parameters {'cycle_time': 60.54414698146056, 'high_enrichment_grade':
94.90559133035943}
:: Running cyclus: ['cyclus', '-i', '/w0/tmp/slurm_vf962887.22555573/tmp5obgpd2s.json',
'-o', '/w0/tmp/slurm_vf962887.22555573/tmpsne43hdi.sqlite']
:: Cyclus run finished!
:: Likelihood for isotopic concentration {92234: 2.709958603982624e-09, 92235:
0.0017157006428273295, 92236: 0.00011897514221215605} is -5.998078727760489
:: Mutating: using parameters {'cycle_time': 60.54414698146056, 'high_enrichment_grade':
82.21161966207573}
:: Running cyclus: ['cyclus', '-i', '/w0/tmp/slurm_vf962887.22555573/tmp7wem4krw.json',
'-o', '/w0/tmp/slurm_vf962887.22555573/tmpb4yq8snm.sqlite']
:: Cyclus run finished!
:: Likelihood for isotopic concentration {92234: 2.778780819288951e-09, 92235:
0.0017287355419483986, 92236: 0.00011673795998123891} is -10.513332283628527
:: Mutating: using parameters {'cycle_time': 60.54414698146056, 'high_enrichment_grade':
91.47066911520066}
:: Running cyclus: ['cyclus', '-i', '/w0/tmp/slurm_vf962887.22555573/tmpd3o7o510.json',
'-o', '/w0/tmp/slurm_vf962887.22555573/tmpl4l8jtmz.sqlite']
:: Cyclus run finished!
:: Likelihood for isotopic concentration {92234: 2.728166639387506e-09, 92235:
0.0017191835659982819, 92236: 0.00011824200548826644} is -2.994907145147081
\end{verbatim}

Disseminating the log and extracting a few samples in \autoref{tab:methodology:logsamples}, it becomes clear that the
likelihood calculation works; the target parameters of this experiment were a cycle time of \SI{60}{\day}, and a product
enrichment grade of \SI{90}{\percent}. Therefore, the closer a sample is to the target value, the higher its likelihood
respectively log-likelihood should be. And this is indeed what can be observed. The cycle time being
constant within these samples is caused by the \textsc{Slice} algorithm used here.

\begin{table}[h]
 \centering
 \begin{tabular}{cc|r}
  \multicolumn{2}{c}{sample} & \\
  \cmidrule(rr){1-2}
  cycle time / d & product enrichment / \% & log-likelihood \\
  \midrule
  \num{60.54} & \num{89.31} & \num{-2.88} \\
  \num{60.54} & \num{94.91} & \num{-5.99} \\
  \num{60.54} & \num{82.21} & \num{-10.51} \\
  \num{60.54} & \num{91.47} & \num{-2.99} \\
 \end{tabular}
 \caption[Some generated samples]{Some samples and their likelihoods from the log example.}
 \label{tab:methodology:logsamples}
\end{table}

This observation alone already gives confidence that, as long as \pymc~works correctly, the inference will produce
useful results. In more difficult situations, the details contained in logs have also repeatedly proven useful to
finding errors in the code or an individual model. Furthermore, the timestamps omitted here also allow an estimation of
the time taken for each sample, and a prediction of the runtime of an experiment. In difficult cases, logs can even be
processed by additional code in order to discover patterns difficult to notice in the logs themselves, or to better
understand the sampling algorithm used by \pymc.

\mysection{Merging and Visualization}{methodology:visualization}

Every process produces its own files containing samples, called \emph{trace files}. Therefore, data from the various
files must be merged before the results can be analyzed. A simple merging script (``\cd{merge.py}'') using the
\cd{arviz} and \cd{seaborn} libraries reads the files, merges samples and generates plots of the raw sampling data.
Additionally, it emits new trace files containing the merged set of samples generated during an experiment.
\cite{arviz2019,Waskom2021}

% \begin{figure}[h]
%  \centering
%  \includegraphics[width=1\textwidth]{figures/methodology/plot_merge_trace_22511847.png}
%  % plot_merge_trace_22511847.png: 0x0 px, 0dpi, nanxnan cm, bb=
%  \caption{An example for a partial trace plot produced by the merging script, for an experiment using measurements not
% suitable for inference with an additional bug in the code. The left column shows probability distributions inferred by
% each chain; the right column shows the random walk of each chain. (In the version of the code producing this trace,
% missing isotopes were incorrectly considered in the likelihood calculation, leading to bad samples being considered
% very likely. Additionally, only \uranium{4} and \plutonium{39}, \plutonium{40} were measured: it's impossible to measure
% only these concentrations and produce a correct inference result.)}
%  \label{fig:methodology:trace}
% \end{figure}

The generated plots are more suitable for analyzing the quality of an inference experiment than for publishing in a
document like this; therefore, figures in this document were generated separately from the merged samples using
the \cd{matplotlib} library.

Figures showing measurements as a function of one or more parameters (such as \autoref{fig:methodology:uisoconc}) were
generated using the \cd{CairoMakie.jl}\footnote{\url{https://makie.juliaplots.org/stable/}} module from data generated
by an auxiliary program. This program uses the same \cyclus~integration as the driver program, but invokes \cyclus~for
a number of parameter sets spread evenly across a chosen parameter space. Processing the resulting measurements, it is
possible to draw the figures for better understanding the influence of one or another parameter on the observed
measurements.

\mysection{Code and Artifacts}{methodology:code}

The code written for this thesis will be published at \url{https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle}.
Results, such as the CDF files containing the samples used for generating graphs as well as the notebooks generating
the graphs are also available there.
