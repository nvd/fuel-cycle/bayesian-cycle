#!/usr/local_rwth/bin/zsh

### Job name
#SBATCH --job-name=CyclusLinearOutput
#SBATCH --account=rwth0774
#
### File / path where STDOUT will be written, %J is the job id
#SBATCH --output=singularity-job-out.%J

### Request the time you need for execution. The full format is D-HH:MM:SS
### You must at least specify minutes or days and hours and may add or
### leave out any other parameters
#SBATCH --time=24:00:00

### Request memory you need for your job in MB
#SBATCH --mem-per-cpu=2048

### Request number of hosts
#SBATCH --nodes=1

### Request number of CPUs
#SBATCH --cpus-per-task=1

### Commands:

### echo "[${HOSTNAME}] My IDs are ${SLURM_JOB_ID} - ${SLURM_ARRAY_JOB_ID} - ${SLURM_ARRAY_TASK_ID} out of ${SLURM_ARRAY_TASK_COUNT}"

/usr/bin/singularity \
	exec /rwthfs/rz/SW/UTIL.common/singularity/cyclus \
	/bin/bash -c 'pushd /home/vf962887/cyclus/src/bayesian-cycle/sampling && \
        PATH=~/.local/bin:${PATH} PYTHONPATH=. \
        python3 ~/cyclus/src/bayesian-cycle/sampling/models/candu/test_cyclus_outputs.py'
