#!/usr/local_rwth/bin/zsh

### Job name
#SBATCH --job-name=CyclusCycleSeparationIter

### File / path where STDOUT will be written, %J is the job id
#SBATCH --output=/work/vf962887/job_output/singularity-job-out.%J

### Request the time you need for execution. The full format is D-HH:MM:SS
### You must at least specify minutes or days and hours and may add or
### leave out any other parameters
#SBATCH --time=24:00:00

### Request memory you need for your job in MB
#SBATCH --mem-per-cpu=768

### Request number of hosts
#SBATCH --nodes=1

### Request number of CPUs (4 chains sampling)
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=2
### Commands:

### Submit like this:
### sbatch --array=1-<n> -N1 < job_array_candu.sh

### echo "[${HOSTNAME}] My IDs are ${SLURM_JOB_ID} - ${SLURM_ARRAY_JOB_ID} - ${SLURM_ARRAY_TASK_ID} out of ${SLURM_ARRAY_TASK_COUNT}"

/usr/bin/singularity \
	exec /rwthfs/rz/SW/UTIL.common/singularity/cyclus \
	/bin/bash -c 'pushd /home/vf962887/cyclus/src/bayesian-cycle/sampling && \
        THEANO_FLAGS=exception_verbosity=high,base_compiledir=${TMP}/theano_${SLURM_JOB_ID}_${SLURM_ARRAY_TASK_ID} \
        PATH=~/.local/bin:${PATH} \
        PYTHONPATH=. \
        python3 ~/cyclus/src/bayesian-cycle/sampling/models/candu/run.py \
        --index=${SLURM_ARRAY_TASK_ID} --run 3000_sigma0.1 \
        --true-parameters-file models/candu/parameters/cycletime_separation_parameters.json \
        --sample-parameters-file models/candu/parameters/cycletime_separation_sampling.json \
        --only-isos 92234,92235,92336,92238 \
        --samples 3000 --iter-sample 100'

# we can rescue /tmp files during this time
sleep 10m
