# cluster

Notes and code for running our simulations on RWTH's HPC cluster.

Generally, the commands will look like this

```bash
singularity exec /rwthfs/rz/SW/UTIL.common/singularity/cyclus /bin/bash -c 'pushd /home/vf962887/cyclus/src/bayesian-cycle/sampling && PYTHONPATH=. python3 ~/cyclus/src/bayesian-cycle/sampling/attempt_sampling/run.py'
```

* singularity to run the container
* use provided image
* run command line in bash, this is more comfortable
* set `PYTHONPATH` for peculiarity of this or other run scripts

## Submitting jobs

The `job_array_...` configurations are made to run as job arrays:

```
sbatch --array=1-<n> -N1 < job_array_...sh
```

## Pitfalls

* Multiple instances on the same machine can interfere.
    * misoenrichment solved this through independent filenames.
    * theano is decoupled through appropriate theano\_flags (see `job_array_candu.sh`)
