#!/usr/local_rwth/bin/zsh

#SBATCH --ntasks=5
#SBATCH --tasks-per-node=1
#SBATCH --mem-per-cpu 100M

echo "root script on ${HOSTNAME}"
for I in {1..5}; do
srun --ntasks 1 bash -c 'hostname && export | grep SLURM' & ;
done
wait
