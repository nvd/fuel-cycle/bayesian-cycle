#!/usr/local_rwth/bin/zsh

### Job name
#SBATCH --job-name=CyclusAttemptedSamplingRun

### File / path where STDOUT will be written, %J is the job id
#SBATCH --output=singularity-job-out.%J

### Request the time you need for execution. The full format is D-HH:MM:SS
### You must at least specify minutes or days and hours and may add or
### leave out any other parameters
#SBATCH --time=12:00:00

### Request memory you need for your job in MB
#SBATCH --mem-per-cpu=4096

### Request number of hosts
#SBATCH --nodes=1

### Request number of CPUs
#SBATCH --cpus-per-task=1

### Commands:

/usr/bin/singularity \
	exec /rwthfs/rz/SW/UTIL.common/singularity/cyclus \
	/bin/bash -c 'pushd /home/vf962887/cyclus/src/bayesian-cycle/sampling && THEANO_FLAGS=exception_verbosity=high PYTHONPATH=. python3 ~/cyclus/src/bayesian-cycle/sampling/attempt_sampling/run.py --samples 50 --run crashrun'
