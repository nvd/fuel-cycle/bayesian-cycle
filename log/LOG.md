# Lewin's BSc Log

- **2021-08-14**
    - Thesis
        - revised chapter on scenario, added mass plots, better explanations, etc.
        - improved some plots
    - sampling
        - opened #28 for wrong priors
        - ran fixed job 22753252 for non-interesting-prior (power) experiment (#27)
- **2021-08-13**
    - Thesis
        - chapter on scenario and specific analysis runs grows finished
    - sampling
        - use non-constant priors for assumed parameters like reactor power. Issue #27, job 22750437

...busy hacking on the thesis. not slacking off, I promise!...

- **2021-08-10**
    - Thesis
        - working on scenario sections for individual inferences
        - extended Julia notebook for generating parameter-isotope plots (concentration/abundance
                depending on parameter)
        - 165 MB of raw concentration data for 15.6k data points in three parameters (fresh/product
                enrichment and cycle time)
    - Sampling
        - jobs ran well overnight; see issues for updates
- **2021-08-09**
    - Thesis
        - talked to Malte and Max
        - improved scenario. Agreed to not look at separation factor of reprocessing plant. Only
        look at
            - cycle time
            - fresh fuel enrichment
            - product enrichment
        - put focus on uncertainties: do more experiments with
            - higher sigmas (#13)
            - groundtruth jitter - simulating measurement uncertainties (#23)
            - on a side note, cross-isotope abundances (#25)
            - far reach: cool-off time inference (#24)
        - find out more *why* individual parameters can be inferred
            - influence of e.g. cycle time on sink contents
- **2021-08-09**
    - Thesis
        - refined bayesian theory
        - revised bayesian example
        - wrote scenario (Elbonia)
    - Code
        - implemented memoization to avoid superfluous Cyclus runs
    - Sampling
        - #20 (non-uniform priors)
        - #21 (directly compare cyclerichment+natenrich between deplU with or without mass, and sep.
                waste)
- **2021-08-08**
    - Thesis
        - wrote section about Bayesian theory
        - started section about Bayesian inference software
    - Sampling
        - checked separation efficiency: it works. #19
- **2021-08-07**
    - Thesis
        - wrote about isotopes in the fuel cycle. Mostly finished
    - Sampling
        - turned on decay, which makes the sinks decay their contents. Matters in the ten or so days
        between fuel switch and simulation termination.
        - this surprisingly enables better inference of natural+spent fuel enrichment + cycle time?
          --> #18
        - with decay enabled and separated waste abundance calculation, look at more parameters.
        Today: separation efficiency in the reprocessing facility. See #19
- **2021-08-06**
    - Thesis
        - Wrote more about fuel cycles
    - Sampling
        - Started some jobs... just look into git log ;-)
- **2021-08-05**
    - Sampling
        - Suddenly worse results in abundance jobs (#15, #5, #10) :(
            - extremely stupid bug: #16. Now fixed, and many jobs re-running. Things look more
            promising again.
        - running very simple experiment for just cycle time, as proof of concept for the thesis
        - running a job just off separated waste, which should contain a lot of information (#17)
        by itself
    - Cyclus
        - compared output of reactor and separated waste sink: Uranium concentrations are not
        identical, but proportional with a factor close to 1 (#17)
    - Thesis
        - Wrote about natural Uranium and fuel cycle physics
        - Wrote about reactors (partially)
        - IDEA: for verification, cite job logs. Easiest way to prove that code is working as
        intended
- **2021-08-04**
    - Sampling
        - #5: run cyclerichment and cyclerichment+natural enrichment with waste isotopes
    - Evaluation
        - #13: likelihood sigmas
            - cycle time is still inferable with 2 or 4% rel. sigma, spent fuel enrichment is not.
    - Writing
        - Bayesian inference example with NPP
- **2021-08-03**
    - Meeting with Malte & Max
        - Measurement uncertainty and likelihood sigma
            - Likelihood sigma is different from physical measurement sigma
            - However, physical uncertainty will be high: on the order of (up to) 5%
            - #13
        - Write about more Physics
            - What happens in the fuel cycle
            - U234 origin & concentration in nat U
            - Production of Isotopes: https://nuclearweaponarchive.org/Nwfaq/Nfaq6.html
            - Management of Reprocessed Uranium: https://contattafiles.s3.us-west-1.amazonaws.com/tnt32660/qKbPLMX8oxpJONV/iaea_management_repU.pdf
            - physical meaning of individual parameters; why do they change isotopic composition
        - real world application: be careful about not becoming too theoretical
            - keep an eye on application
            - ensure that real-world examples could benefit from the results
            - but: scope limited, as reactor and fuel cycle simulations have limits in their
            closeness to the real world.
            - what does the second peak have to say in cyclerichment for Pu-240 (#14)
        - add to structure v0.2:
            - origin and physics of isotopes U-234, U-236 (which are mainly observed)
            - possibly also Pu-240, etc.
            - meaning and influence of parameters: cycle time for weapons Pu, etc.
        - Look at previous BSc theses
        - reactor temperature is mostly irrelevant
    - start new jobs for #13, #7
        - fix missing-isotopes-have-high-likelihood-bug in #7 (#14)
    - Make Max' new Cs/Sr kernels run
- **2021-08-02**
    - Evaluation
        - of SMC sampling
            - job was configured incorrectly :( now running again as #22590656
            - according to job log, never leaves stage 0. I.e., way too slow. Unsure how to improve?
            Play a bit more with toy models...
    - To Do
        - Pakistan's fuel cycle:
            - check U235 concentration of spent fuel in my model
    - Reactor temperature: talked to Benjamin
        - moderator temperature changes neutron spectrum: neutron spectrum is bimodal (peaks for
            thermal and very high energies), and thermal peak shifts to higher energies
        - generally, temperature changes crosssections in fuel and moderator, and thus changes
        characteristics of isotope production
        - reactor model probably assumes a thermal equilibrium between moderator and fuel, which in
        reality is unlikely
        - fuel temperature is more important than moderator temperature, due to direct change of
        crosssection.
        - hotter fuel increases crosssections and increases production (generally), of e.g.
        Plutonium. Energy difference between nuclei and neutrons decreases, leading to a higher
        chance of neutron capture.
        - this could explain our increasing Pu mass. However, isotope composition either doesn't
        change very much or is counteracted by cycle time/enrichments.
        - next step: check temperature influence using cyclus runner script
        - behavior of iosotpes for temperatures:
            - ![](img/isobytemp.png)
- **2021-08-01**
    - RWTH Gitlab is down... again.
    - Evaluation
        - 22573117 #9: cyclerichment and temperature with tails mass but only mass of DepletedUraniumSink
            - ![22573117](img/plot_merge_trace_22573117.png)
            - compared to #22555606 (cyclerichment+temperature with tails mass of U and Pu), this
            result is worse. This means that Pu tails mass is playing a big role for inferring
            temperature.
            - #22555606: ![22555606](img/plot_merge_trace_22555606.png)
        - 22573390 #11: cycle time, natural+spentfuel enrichment, temperature
            - ![22573390](img/plot_merge_trace_22573390.png)
            - four parameters at once. Result: not good. Very multimodal, likely ambiguities.
            - several chains crashed due to Cyclus timeout after 120s. Either bad parameters or bad
            machines.
        - 22573425 #11: cycle time, natural+spentfuel enrichment, temperature but with Pu
            - ![22573425](img/plot_merge_trace_22573425.png)
            - Bad quality because only few chains completed this run. For some reason, the sampling
            was slower than for 22573390.
        - 22573866 #10: abundances for cyclerichment
            - ![22573866](img/plot_merge_histogram_22573866.png)
            - almost identical result to 22555572 (*for #9: tails mass (cyclerichment), run2*)
            - #22555572: ![22555572](img/plot_merge_histogram_22555572.png)
            - possibly very slight improvement in high-enrichment grade
            - but theoretically good result: this shows that abundances have same information as
            concentrations. Not more, not less. And they abundances are suited for inference, too.
    - Sampling
        - Implementing SMC sampling
        ([sample_smc](https://github.com/pymc-devs/pymc3/blob/main/pymc3/smc/sample_smc.py))
        - Started job #22581088 to check how SMC sampling compares to Slice sampling (22555589) for
        the multimodal cycletime/natural enrichment/spent fuel enrichment scenario. The
        documentation claims that it does better for multimodal scenarios.
        - Iterative sampling (with frequent checkpoints) is not possible; instead there are 4 chains
        in parallel in each of 5 jobs
- **2021-07-31**
    - Sampling
        - I found more about
        [SMC](https://docs.pymc.io/pymc-examples/examples/samplers/SMC2_gaussians.html), maybe an
        opportunity to improve sampling for multimodal distributions (both enrichments)
        - Evaluating experiments for
        [#9](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/9) (tails mass
        observation)
            - no improvements for cycletime+spent enrichment/cycletime+spent enrichment+natural
            enrichment.
            - But: **moderator temperature can now be inferred quite accurately**
        - Evaluating experiments for
        [#6](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/6) (gradient, NUTS)
            - I used as model just two sampled parameters: natural Uranium enrichment, spent fuel
            Uranium enrichment. This was done for two reasons
                - two parameters means that gradients only have two components, i.e. 4 Cyclus runs
                per gradient.
                - these parameters are continuous. A parameter like cycle time (in days) is
                discrete, because it is converted into an int in the Cyclus model (the time step is
                        1d there)
            - NUTS was very slow; only one chain of 15 was completed after 12h (the runtime of the
                    job). Other chains were either almost done (230/250 samples) or not at all
            (30/250 samples).
            - The quality of that one chain was bad, and there were many divergences. This may be
            caused by bad gradients, which are in turn caused by my primitive algorithm with only
            one iteration (to speed things up)
            - The Slice comparison job, which used the conventional sampling method I've been using
            for all other runs, delivered quite a good result.
            - There doesn't seem to be much reason to use NUTS, then.
        - Ran jobs for #11
            - 22573390
            - 22573425 (plus Pu239/240)
    - Models
        - Implemented abundance calculation for #10
        - Running job 22573866 for this.
    - Thesis
        - copied template from Max' master thesis
- **2021-07-30**
    - Yesterday's experiments were unsuccessful due to a bug I fixed in d8d4d9e (damn Python!!)
    - Running same jobs again today:
        - 22555572	for #9: tails mass (cyclerichment), run2
        - 22555589	for #9: tails mass (cyclerichment + nat enrich), run 2
        - 22555606	for #9: tails mass (cycle time, spent enrichment, temperature), run 2
        - 22555628	#6 NUTS: natural U and spent fuel enrichment, max iter 1, run 2
        - 22555935	#6 Slice: natural U and spent fuel enrichment
    - talk with Max
        - will train GPR separation kernels for isotopes `"Cs133", "Cs134", "Cs134m", "Cs135",
        "Cs135m", "Cs137", "Sr86", "Sr88", "Sr90"`
- **2021-07-29**
    - Run experiments for [#8](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/8)
        - #22532567 (60d 80%)
        - #22532779 (40d 92%)
        - #22533115 (80d 85%)
    - by the way: why is U235 enriched from the separated fuel? doesn't the reactor burn up the
    U235? -> yes it does
        - we look at tails from this enrichment, but is it realistic to enrich the spent fuel in the
        first place?
        - note: "Reprocessed uranium will contain 236U, which is not found in nature; this is one
        isotope that can be used as a fingerprint for spent reactor fuel." -> it is done sometimes
    - gradient again ([#6](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/6))
        - #22551400 (Slice)
        - #22551382 (NUTS, gradient: max. iterations 1)
        - this way we can have a simple comparison between straight sampling and gradient sampling
        - start values are randomized of course
        - more details in that issue
    - created some more chain-likelihood-histograms like
    [here](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/4#note_1484053)
- **2021-07-28**
    - Evaluated isotope likelihood comparison in
    [#7](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/7). tl;dr: for
    cyclerichment, Pu doesn't give an advantage. Otherwise: more isotopes = better.
    - Run [#3](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/3) (natural + spent
    fuel enrichment + cycle time) with different isotope combinations for comparison. Jobs:
        - 22511760
        - 22511782
        - 22511828
        - 22511847
    - filed [#8](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/8) to learn more
    about cyclerichment, because it may be the "flagship process" because it works so well
    - Read more in
        - Nuclear Forensics: chapter 5 and chapter 3 are especially interesting for the thesis'
        topic
        - Max' Master thesis: NFC in general, political motivation: this is a good basis to build on
        - TODO: read Max' sources
    - Writing: slowly starting to think about it
        - definition list?
        - template?
        - get summary ready of how enrichment and burnup influences U234 and U236, as well as Pu
        isotopes (as basis for the likelihood calculation)
        - explain fuel cycle model used for inference
        - collect and describe tricks
            - technical
                - parallel sampling (theano setup)
                - cluster setup, logging, etc.
                - random seeding
                - simulation result extraction from SQLite
                - iterative sampling
                - merging
            - statistical and model
                - likelihood calculation
                - random start values
                - gradient calculation
                - calculation of cycle time, acceleration of simulation
            - search git log and this log for other tricks
    - Fuel cycle
        - double check burnup calculation
        - burnup (0.5 - 2.5 MWd/kg) is focused on military applications, not civilian fuel cycles
        - note that natural uranium isotopic composition is not taken into account
        - Thorium? see Nuclear Forensics p. 120ff, for age determination
- **2021-07-27**
    - Evaluated traces of yesterday's job.
        - By topic (some overlap: all sampling used randomized start values)
            - [Enrichment of natural uranium and spent fuel (and cycle
                    time)](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/3)
            - [Randomized start
            values](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/4) 
            - [Use Pu isotopes for likelihood
            calculation](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/5)
            - [NUTS (gradient-based)
    sampling](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/6)
    - Sampling
        - look at relationship between number of isotopes sampled and inference quality
        (https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/7).
- **2021-07-26**
    - Sampling
        - with randomized start values and 0.5% sigma: #22443251
        - the hope is that the entire parameter space will be covered more evenly.
```
2021-07-26 08:32:42.992550 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 64.09131316959923, 'high_enrichment_grade': 91.39059374946784, 'slight_enrichment_grade': 1.7061414618360415}
2021-07-26 08:32:43.005457 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 23.16318826607723, 'high_enrichment_grade': 75.41751861128469, 'slight_enrichment_grade': 1.674853692745811}
2021-07-26 08:32:43.006546 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 108.05503835511284, 'high_enrichment_grade': 77.68258670521624, 'slight_enrichment_grade': 1.4014667134987262}
2021-07-26 08:32:40.410741 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 40.219272056328585, 'high_enrichment_grade': 46.67551790427716, 'slight_enrichment_grade': 1.4392413202776524}
2021-07-26 08:32:40.395622 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 42.83942763726226, 'high_enrichment_grade': 82.6382615238863, 'slight_enrichment_grade': 1.4018746777267492}
2021-07-26 08:32:40.395098 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 48.48961277180368, 'high_enrichment_grade': 27.405750466469865, 'slight_enrichment_grade': 1.0277195589798995}
2021-07-26 08:32:44.127571 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 97.07972281213405, 'high_enrichment_grade': 60.80401649532018, 'slight_enrichment_grade': 1.140252011056785}
2021-07-26 08:32:44.130423 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 73.44717700248896, 'high_enrichment_grade': 61.8597186811684, 'slight_enrichment_grade': 1.536242551679189}
2021-07-26 08:32:44.130345 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 68.67443285593, 'high_enrichment_grade': 42.80741079300411, 'slight_enrichment_grade': 1.514487914711772}
2021-07-26 08:32:44.124848 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 47.57851042412305, 'high_enrichment_grade': 51.85577461424464, 'slight_enrichment_grade': 1.8263074373186416}
2021-07-26 08:32:44.124877 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 54.390291178797185, 'high_enrichment_grade': 23.116597895113035, 'slight_enrichment_grade': 1.710156634170246}
2021-07-26 08:32:44.125253 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 53.152446346827716, 'high_enrichment_grade': 74.99764620881182, 'slight_enrichment_grade': 1.4531716859383468}
2021-07-26 08:32:44.127614 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 37.585293505994514, 'high_enrichment_grade': 55.487670486466065, 'slight_enrichment_grade': 1.453029267754993}
2021-07-26 08:32:44.124460 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 118.4760491915011, 'high_enrichment_grade': 15.628776804647618, 'slight_enrichment_grade': 1.7100590237656503}
2021-07-26 08:32:42.992553 :: Starting to sample iteratively (iter_sample), initial parameters {'cycle_time': 77.70428659619276, 'high_enrichment_grade': 82.01935798240586, 'slight_enrichment_grade': 1.3188352101683625}
```
-
    -
        - parameters needed adjustment: enrichment of natural U below 1% caused misoenrichment to
        crash (parameters out of range)
    - it looks like with randomized start values, we see a multimodal distribution? (evaluation to
            come) -> this seems plausible, if different sampled variables counteract each other.
        - sampling other isotopes (Pu) will improve situation for e.g. early variables like nat. U
        enrichment.
    - two jobs before this (#22442688 and #22443177) needed to be cancelled because a natural
    enrichment level of below 1% caused misoenrichment to crash
    - Enable candu model sampler to use isotopes from different sinks. `--only-isos` keeps working
    as before, but isotopes are now judged for every agent's inventory independently.
        - running jobs #22451316 (cyclerichment with U and Pu isotopes) and #22451459 (same but with
                three times more jobs, 15 instead of 5)
            - 22451316: ![](img/plot_merge_trace_22451316.png)
        - and #22451359 (cycle time and enrichment of natural and spent uranium), each with 1% sigma
    - for natural U enrichment + spent fuel enrichment + cycle time, running jobs
        - the theory is that with three dimensions it is possible that there is more than one
        combination of parameters leading to high likelihood ("plausibility" from the perspective of
        the model)
        - #22443251 (0.5% rel. sigma)
            - weird errors
```
2021-07-26 19:16:24.932402 :: Mutating: using parameters {'cycle_time': 66.63588018564553, 'high_enrichment_grade': 45.30418510219335, 'slight_enrichment_grade': 1.2527317347840419}
2021-07-26 19:16:24.933728 :: Running cyclus: ['cyclus', '-i', '/w0/tmp/slurm_vf962887.22443256/tmppls73095.json', '-o', '/w0/tmp/slurm_vf962887.22443256/tmpuzbh229h.sqlite']
2021-07-26 19:16:26.457697 :: Error calling cyclus. stdout/stderr: b'Experimental Warning: MatlBuyPolicy is experimental and its API may be subject to change\nExperimental Warning: MatlSellPolicy is experimental and its API may be subject to change\nExperimental Warning: The Storage Facility is experimental.\n' b'              :                                                               \n          .CL:CC CC             _Q     _Q  _Q_Q    _Q    _Q              _Q   \n        CC;CCCCCCCC:C;         /_\\)   /_\\)/_/\\\\)  /_\\)  /_\\)            /_\\)  \n        CCCCCCCCCCCCCl       __O|/O___O|/O_OO|/O__O|/O__O|/O____________O|/O__\n     CCCCCCf     iCCCLCC     /////////////////////////////////////////////////\n     iCCCt  ;;;;;.  CCCC                                                      \n    CCCC  ;;;;;;;;;. CClL.                          c                         \n   CCCC ,;;       ;;: CCCC  ;                   : CCCCi                       \n    CCC ;;         ;;  CC   ;;:                CCC`   `C;                     \n  lCCC ;;              CCCC  ;;;:             :CC .;;. C;   ;    :   ;  :;;   \n  CCCC ;.              CCCC    ;;;,           CC ;    ; Ci  ;    :   ;  :  ;  \n   iCC :;               CC       ;;;,        ;C ;       CC  ;    :   ; .      \n  CCCi ;;               CCC        ;;;.      .C ;       tf  ;    :   ;  ;.    \n  CCC  ;;               CCC          ;;;;;;; fC :       lC  ;    :   ;    ;:  \n   iCf ;;               CC         :;;:      tC ;       CC  ;    :   ;     ;  \n  fCCC :;              LCCf      ;;;:         LC :.  ,: C   ;    ;   ; ;   ;  \n  CCCC  ;;             CCCC    ;;;:           CCi `;;` CC.  ;;;; :;.;.  ; ,;  \n    CCl ;;             CC    ;;;;              CCC    CCL                     \n   tCCC  ;;        ;; CCCL  ;;;                  tCCCCC.                      \n    CCCC  ;;     :;; CCCCf  ;                     ,L                          \n     lCCC   ;;;;;;  CCCL                                                      \n     CCCCCC  :;;  fCCCCC                                                      \n      . CCCC     CCCC .                                                       \n       .CCCCCCCCCCCCCi                                                        \n          iCCCCCLCf                                                           \n           .  C. ,                                                            \n              :                                                               \n'
2021-07-26 19:16:26.532175 :: Measured concentration for 92234: 3.009475137117886e-09+/-1.504737568558943e-11, true concentration: 2.7359155787672705e-09, x = 19.99766078117616, likelihood: -200.87215689270027
2021-07-26 19:16:26.562286 :: Measured concentration for 92235: 0.0016759091785100645+/-8.379545892550322e-06, true concentration: 0.0017206755230965488, x = -5.203345312418019, likelihood: -14.456339753335959
2021-07-26 19:16:26.606118 :: Measured concentration for 92236: 0.0001286017680658031+/-6.430088403290155e-07, true concentration: 0.00011803315614052129, x = 17.907869739075046, likelihood: -161.26483782904455
2021-07-26 19:16:26.653474 :: Measured concentration for 92238: 0.9981949715237567+/-0.004990974857618784, true concentration: 0.9981607459001454, x = 0.006857737844712232, likelihood: -0.918962047488846
2021-07-26 19:16:26.653540 :: Likelihood for isotopic concentration {92234: 3.009475137117886e-09, 92235: 0.0016759091785100645, 92236: 0.0001286017680658031, 92238: 0.9981949715237567} is -377.5122965225696
```
-
    -
        -   
            - unknown reason, no details are printed
        - #22443831 (1% rel. sigma)
    - Gradients
        - Looking at implementing gradients based on [the well-known
        article](https://docs.pymc.io/notebooks/blackbox_external_likelihood.html)
        - implemented [basic algorithm](https://gist.github.com/dermesser/ce70f177ff5e17fa06d548d1325f6b5d) with not-too-many steps
        - implemented in my framework in 3874150, 94956b7.
        - works in principle
            - ![](img/nuts.png)
        - but: likely doesn't make sense to apply on integer variables such as cycle time (in days).
        NUTS (gradient) should thus only be used when only continuous variables are being samples.
        - started job #22470974 sampling natural U and spent fuel enrichment as well as moderator
        temperature to test NUTS behavior with these gradients.
- **2021-07-25**
    - Sampling
        - Evaluated job #22424773
            - temperature doesn't look promising; other two parameters still correct (which is good:
                    temperature didn't interfere when started with the "correct" start parameters)
            - ![](img/plot_merge_density_22424773.png)
        - Evaluated job #22424600
            - all parameters can be reasonably inferred (and could probably be better inferred with
                    a lower sigma0
            - ![](img/plot_merge_density_22424600.png)
        - What do we learn? -> This approach may be useful for hypothesis testing.
            - What happens when the hypothesis (represented by start values) is wrong? Do we get a
            nonsensical distribution? Will there still be some result?
            - This should be goal of the next sampling runs: runs with different start values and
            especially different chains with different start values.
- **2021-07-24**
    - Sampling
        - after disappointing results yesterday, attempt setting the start point at the target
        value. This is unrealistic of course, but will test if the likelihood around the target
        value is indeed high, meaning that the sampled parameters (cycle time and spent fuel/natU
                enrichment) are inferrable (in principle at least)
        - job #22424600 samples cycle time, sspent fuel enrichment, and natural
        enrichment, with target values as start values and 1% sigma.
        - job #22424773 samples cycle time, spent fuel enrichment, temperature;
            - temperature still resists
        with target values as start values and 1% sigma. Also using again the
        structured log mechanism.
    - `miso_enrichment`
        - the attempt at adding isotopes for Cs and Sr failed, I probably added them using a wrong
        format: `terminate called after throwing an instance of
        'pyne::nucname::IndeterminateNuclideForm'`
- **2021-07-23**
    - Finished reading article *Applications and Limitations of Nuclear Archaeology in Uranium
    Enrichment Plants*
    - Organizing job runs in spreadsheet: [experiments](experiments.gnumeric)
    - Results from yesterday's jobs about moderator temperature are not promising
        - job #22382486 (temperature in addition to cycle time and enrichment)
            - ![](img/plot_merge_trace_22382486.png)
        - job #22393386 sampling enrichment of natural uranium and spent fuel. Target values: 60
    days, 90% high enrichment, 1.1% low enrichment. --> off target!!
            - ![](img/plot_merge_trace_22393386.png)
    - next steps:
        - provide start values? there must be some likelihood bump near the target values.
        - look at Pu isotopes?
    - added Cs and Sr isotopes to miso_enrichment
- **2021-07-22**
    - Model
        - What can be inferred from Pu concentrations?
            - cyclerichment (cycle time + enrichment), even sampling across parameter space
            - obviously, enrichment doesn't have influence here because plutonium is extracted
            before the enrichment stage
            - a candidate would possibly be NatU enrichment (Pu comes out of the reactor without
                    further steps other than separation) or temperature
        - Which influence does temperature have?
            - ![](img/exploration/uranium_concentration_by_temp_ct63.png)
            - these are respective fractions for U isotopes for different temperatures (given by
                    what the reactor models allow) at 63 days cycle time, and for different
            enrichment levels.
            - Temperature also has influence on Pu isotope concentrations:
                - ![](img/exploration/pu_temp_ct_concentration.png)
    - sampling
        - Running job #22382486 to sample cycletime, enrichment, temperature (5 x 2000 iterations)
        - Running job #22393386 to sample cycletime, enrichment of both natural uranium and spent fuel
    - Next steps: recapping yesterday's discussion
        - Explore inferring **other parameters**
            - [Natural Uranium enrichment
            (0.9%-2%)](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/issues/1)
            - Temperature of reactor ("moderator temperature"); ask Lukas about specifics.
            - Potentially: Power. But: this is not a free parameter; it relates via `BU = P * CT /
            M` with burnup/cycle time/core mass. The burnup needs to stay within the useful limits
            (0.5 - 2.5 MWd/t). If the limits are obeyed, power could be sampled at 80% - 105%.
            - Then: enrichment plant capacity (in SWU). This should be readable from mass of tails?
            Although mass is also dependent on simulation run time and cycle time.
        - Explore exploiting **other simulation outputs**
            - Plutonium isotope composition: this gives us access to the reactor history itself, as
            Pu is not undergoing enrichment.
                - Keep in mind that Pu is usually not easily accessible, as it is not waste but
                product.
                - In separation, parts of the Pu should go into tails (sink `SeparatedWaste`)
            - other isotopes; these can be added in miso_enrichment (`gpr_reactor.cc`). E.g., Cs,
            Sr.
            - Tails mass (this is already implemented and just needs to be enabled)
            - Explore quality differences when using 1/2/3/4 different Uranium isotopes for
            inference.
                - crucial question: can two parameters be inferred using just one output variable?
        - Separation factor may just be not feasible to infer, although it is a very interesting
        aspect.
        - Scenario
            - Create somewhat realistic scenario where our techniques can be used to either infer
            unknown parameters, or check a report's consistency.
            - For this, more fuel cycle knowledge is required (although this shouldn't take too
                    long)
        - Gradients: attempt differentiating Cyclus numerically, if it is not taking too much of a
        performance impact.
    - There is a relation between jobs and power output for MAGNOX power plants
        - ![](img/magnox_jobs.png)
        - intercept = 59 jobs (required to run an offline plant?), slope = .3155 jobs/MW, p = 8.8e-4
    - Models & sampling
- **2021-07-21**
    - Models & sampling
        - examined samples of test job along three dimensions. axes: enrichment (%) 5-95 / cycle
        time (d) 10-120 / separation factor 1.1-1.9. Color is likelihood, with yellow = high
        likelihood.
            - ![](img/exploration/samplingspace_8k_metro.png)
            - ![](img/exploration/samplingspace_64k_metro.png)
        - also analyzed other samples in my Jupyter notebook
            - running cycletime + enrichment (the old well-known one) yields a new, weird/wrong
            sample distribution (slice): ![](img/exploration/cyclerichment_all_isos_samples.png)
                - the peak here should be at 90% enrichment. The difference may be due to the
                 likelihood calculation involving other isotopes.
                - calculating using only 234+236 with relative sigma 1% yields a different result;
    desired area is cycle time = 60d, enrichment = 90%.
                                             ![](img/exploration/cyclerichment_234_236_samples.png)
        - in order to examine this peculiar structure, I'm running three jobs, all with Slice
        algorithm:
            - #22320813 runs just cycle time and separation factor, with 0.1% rel. sigma and all
            isotopes (except 239, 240). This is to check if separation factor can, in principle, be
            inferred.
                - ![](img/exploration/plot_merge_trace_22320813.png)
                - **WRONG** because I used the wrong sampling parameter name, i.e. separation factor
                was not mutated!
            - #22320838 runs cyclerichment (cycle time, enrichment level) with 0.1% rel. sigma and
            all isotopes (except 239, 240) -> this is for the purpose of checking if the different
            likelihood calculation (which now involves U235 and U238 as opposed to before) is
            causing problems.
                - ![](img/exploration/plot_merge_trace_22320838.png)
            - #22321353 runs again cycletime+enrichment+separation factor, but only with U234 and
            U236
                - ![](img/exploration/plot_merge_trace_22321353.png)
                - **WRONG** because I used the wrong sampling parameter name, i.e. separation factor
                was not mutated!
            - #22331743 relative sigma 1% and only U234, U236 with cycle time + enrichment +
            separation factor. The idea is that a less "strict" likelihood calculation allows the
            sampling to "move around" more freely.
                - the principle works, although 1% may be a bit too high.
                - **Cancelled** wrong sampling parameters, of course nothing happens!
            - #22331758 like 22331743 but with all U235 and U238 in addition to U234 and U236
                - **Cancelled** wrong sampling parameters!
            - #22339009 cycle time + separation factor + enrichment, now with correct parameter name
                - ![](img/plot_merge_trace_22339009.png)
                - the separation factor inference still does not properly work. What is striking is
                that apparently certain values come with very high density and the chains don't move
                away from such values; these may correspond to sawtooth intersections?
            - #22339272 cycle time and separation factor, now with correct parameter names
                - ![](img/plot_merge_trace_22339272.png)
                - even this simple 2D problem cannot be solved properly (although there is a tiny
                        hump at separation factor = 1.6).
                - see sawtooth pattern below - the sudden changes in isotopic concentration even
                disturb the other measurements heavily.
                - also well-observable here: ![](img/plot_merge_histogram_22339272.png)
        - now using rwth0774 resources, because I ran out of personal resources
        - Idea: try again calculating impact of separation factor directly and compare with
        expectation
    - Discussions etc.
        - with Max
            - separation factor maybe has or maybe not influence on Uranium composition in depleted
            sink?
                - ![](img/exploration/max_separationfactor_influence.png)
                - Shows sawtooth pattern, indicating that it is difficult to extract useful
                information from these data. ![](img/exploration/separation_factor_isofrac.png)
                - the pattern is due to dynamic cascade resizing depending on separation factor. A
                single isotope concentration can thus appear for many different separation factors.
    - Moved notebooks to main repository (`notebooks/`)
- **2021-07-20**
    - Models & sampling
        - the new method using `pm.Potential()` seems to work well, too
            - compare with results from 2021-07-16, note that this result has fewer samples
                - ![](img/plot_merge_trace_22275244.png)
            - I am not satisfied and would like to know what changed in the last three days for the
            code to not work anymore
        - cyclerichementseparation: results from #22275230 are a bit indeterminate from the first few samples (sampling is very slow now)
            - true values: cycle time 60d, enrichment 90%, separation factor 1.6
            - ![](img/plot_merge_trace_22275230.png)
            - Sampling is very slow; about 90 seconds per sample, more than 90 cyclus runs per
            sample. Metropolis may be faster (job #22277919 uses metropolis)
        - Running again with 0.1% relative sigma for likelihood calculation: #22277468 (slice
                algorithm)
            - not very good at all: ![](img/plot_merge_trace_22277468.png)
        - Running again with 0.1% rel. sig. and metropolis algorithm: #22277919
            - Metropolis advantage: only ~6 cyclus runs per sample
            - Metropolis disadvantage: probable very choppy posterior. Like, really bad. Possible
            not useful at all.
            - yes... ![](img/plot_merge_trace_22277919.png)
        - I extended `visualize/merge.py` to directly run over Cyclus runs (instead of processed
                PyMC samples), yielding an interesting distribution from job #22293991 using
                Metropolis:
            - ![](img/plot_merge_trace_22293991.png)
            - compare to the normal trace: ![](img/plot_merge_trace_22293991_normal.png)
            - this was achieved by directly writing out Cyclus simulation outputs to a JSON log, in
            parallel to PyMC sampling, and converting the JSON logs into arviz traces.
    - Discussion
        - with Max
            - revise dependence of isotope concentration
            - choose which isotopes to look at for different variables
            - check out nvdenrichment for enrichment behavior
            - fissile vs fissionable -> Benjamin's master thesis
        - Scenario
            - 1 month of writing
            - stay with SRS GPR kernel
            - come up with scenario ("Country A vs Country B, report integrity, enrichment
                    declaration" etc)
            - discuss with Malte (and Max) what belongs in the thesis: text about cyclus, PyMC,
            reactors(?), underlying models/GPR...
- **2021-07-19**
    - Models & sampling
        - job #22214079 crashed due to sqlite errors (no explicit inventory in output). Added code
        to catch exceptions and return log likelihood of `-inf`.
        - ran job #22264065 (and cancelled soon: used uranium mine and enrichment, which is not
                necessary)
            - like before: sample cycle time, enrichment, separation factor
            - 15k samples
            - with improved error handling
            - only isotopes U233-238 in `DepletedUraniumSink` which contains all uranium that is
            thrown away
        - instead, started job #22265459 which uses explicit opaque Uranium source of correct
        enrichment
        - **cancelled both these jobs (22265459, 22264065) because I was sampling natural uranium
        separation factor...** trying again now with less stupidity
        - Problem: jobs are not saving traces correctly:
        */home/vf962887/.local/lib/python3.8/site-packages/arviz/data/base.py:169: UserWarning: More chains (500) than draws (3). Passed array should have shape (chains, draws, \*shape)*
        this last happened when using `chains=1` argument to `pm.sample()`, but I didn't do that now.
        Not good, but merging works around it successfully (for some reason, not sure why exactly)
            - using metropolis with multi-core sampling (and multiple chains) would probably get
            around this
        - Another problem: theano error (that I used to get when convergence checks were computed)
        occurs. Not sure where these errors come from, they are supposed to be fixed by now.
            - at least the theano error occurs around the separation factor statistics, which we
            didn't have before. But what is different?
                - bug in command line parsing: `--algorithm`'s default was `None`, which chooses
                `metropolis`. Which has the [known problem
                pymc3#4839](https://github.com/pymc-devs/pymc3/issues/4839)
            - Possibly write better logs (or structured logs). Because that makes it easier to infer
            even partial traces without needing PyMC to write them as CDF files, which is sometimes
            unreliable (see above)
            - Possible reason: `--iter-sample` was less than 10% of `--samples`
            meaning that first attempt to save has only tuning samples which
            are to be discarded.
                - ...no? The theano error now even appears with the conventional two variables, I am confused
                - Potentially [pymc3#4057](https://github.com/pymc-devs/pymc3/issues/4057), but this is the
                same issue I already knew about
                    - and [pymc3#4002](https://github.com/pymc-devs/pymc3/issues/4002), first seen
                    on Jun 24th (see below).
                    - tl;dr: the blackbox likelihood example slightly abuses the API and arviz
                    stopped taking the abuse
                - Will try solving using `pm.Potential()` which just brutally adds a likelihood value to the model.
        - PyMC seems to sample a lot slower with three instead of two variables; several dozen
        Cyclus runs per sample, sometimes.
    - **Final jobs (for the night)**
        - #22275230 as above: cycle time + enrichment + separation factor. iteratively (`job_array_candu_cyclerichmentseparation_iterative.sh`)
            - true values: cycle time 60d, enrichment 90%, separation factor 1.6
            - results are a bit diffuse, trying again with smaller sigma tomorrow
        - #22275244 classic cyclerichment, in order to validate the `pm.Potential()` approach
            - this still works (see tomorrow)
    - Discussions
        - Max
            - separation factor refers to separation factor of fissile material
            - sampled in `[1.1, 1.9]`
            - only look at U isotopes 233-238, not 239 and 240
        - other ideas
            - separate tails streams (only once explicit enrichment is enabled (again))
            - sample natural Uranium enrichment grade (currently fixed at 1.1%)
- **2021-07-18**
    - Models & Sampling
        - the following plots are based on data sitting in `sampling/models/candu/data/`
        - is it useful to look at Plutonium?? Ask Max -> because differing cycle time has constant
        Pu fractions, and I'd expect that irradiation will convert Pu-239 into Pu-240
            - ![](img/exploration/pu_239_separation_cycletime.png)
            - it's just an illusion, see diagrams below, the fraction doesn't very as much with
            cycle time as it does with enrichment
        - straight-sampled cycle time vs. separation factor. Results for U not very conclusive; Pu
        is not affected because it is separated upstream of the enrichment plant:
            - U234 concentration (relative): ![](img/exploration/cycle_time_separation_factor_U234.png)
            - U235 concentration (relative): ![](img/exploration/cycle_time_separation_factor_U235.png)
            - U236 concentration (relative): ![](img/exploration/cycle_time_separation_factor_U236.png)
            - U238 concentration (relative): ![](img/exploration/cycle_time_separation_factor_U238.png)
            - U239 concentration (relative): ![](img/exploration/cycle_time_separation_factor_U239.png)
            - U240 concentration (relative): ![](img/exploration/cycle_time_separation_factor_U240.png)
        - Ran job #22214079
            - sampling three parameters for the first time: cycle time, enrichment, separation
            factor (of natural U enrichment)
            - Likelihood has 1% rel. sigma and uses U and Pu isotopes in
            DepletedUranium/PlutoniumSink
            - Not very carefully prepared; 5x3000 = 15k samples à la `stuff in, results out`
    - GPR
        - [GPR from Scratch](https://peterroelants.github.io/posts/gaussian-process-tutorial/) -
        read and followed for a bit. Want to know more about it!
- **2021-07-17**
    - Condensed Matter Physics: 2 out of 3 tests gave me enough points, now I can focus everything on the thesis work!
    - Models & sampling
        - investigating effect of cycle time (and others) on Pu concentrations in `PlutoniumSink`,
        using `test_cyclus_outputs.py` script.
            - here: ![](img/pu_concentrations_cycle_time.png)
            - this definitely should be enpugh to infer at least cycle time; given that they are 
            monotonously-increasing (and injective), we could get good results (theoretically)
            - however, e.g. U *enrichment* is not changing Pu composition at all.
                - why? -> look into this
                - what does influence Pu composition? Is it useful to have this independent output?
                Do the reactor models deal with this?
            - take a look at sampling burnup instead of cycle time; we know that Pu-239 -> Pu-240
            over longer time frames.
        - what about "age of sample", factoring in decay? -> increase simulation time, radioactive
        decay will occur, leading to exponential decreases in some (decaying) elements. Interesting
        for nuclear forensics applications.
        - **Likelihood calculation** and its sigma
            - 1% rel. sigma for isotopic fractions has acceptable results in "simulation" stage (see
                    yesterday's entry). We should check if we can find real-world isotopic
            compositions and infer fuel cycles. In the worst, case our bayesian process would
            produce noise or extremely wide distributions.
            - E.g. Fedchenko's "Nuclear Forensics", p.27 - IAEA databases (also p.29)
    - Read "Nuclear Forensics"
- **2021-07-16**
    - Models & sampling
        - Running cyclerichment (cycle time + enrichment grade) with 1%
        relative sigma to check how 2D distribution looks in that case (job #22165212)
    - Visualization
        - Walk
            - job 22165212 (full range, cyclerichment, 1% rel. sigma) ![](img/walk_22165212.png)
        - Seaborn marginal distributions
            - job 22154009 (full range, cyclerichment, 0.1% rel. sigma) ![](img/seaborn_marginal_hist_22154009.png)
            - job 22165212 (full range, cyclerichment, 1% rel. sigma) ![](img/plot_merge_histogram_22165212.png)
            - job 22165212 (same as previous), KDE plot: ![](img/plot_merge_histogram_22165212_kde.png)
        - some visualizations don't work too well when done automatically in a script. However, by
        using the merged CDF trace files, it's easy to run visualizations in a Jupyter notebook etc.
- **2021-07-15**
    - Models & sampling
        - Running another enrichment sampling job #22148760 with 0.1% rel. sigma on isotopic
        fractions (both U234, U236), 2 nodes x 4 chains x 5 iterations x 200 samples = 8000 samples
        (multicore sampling)
            - run as job #22151611
            - results better than job #22099390 below (which used 1e-6 absolute sigma):
                - ![](img/trace_plot_22151611_prelim.png)
                - now also as density plots: ![](img/density_plot_22151611_prelim.png)
        - Working on sampling both enrichment and cycle time at once.
            - all jobs have the following "true" parameters that are to be inferred:
                - enrichment 90%
                - cycle time 60 days
                - these are the numbers to look out for in the plots below.
            - Calculating likelihood from mutliple isotopes has pitfalls. First observation: the
            sampler "corners" itself, and becomes stuck at the upper enrichment limit, not mutating
            the cycle time. Unclear why
            - by adjusting parameters (here: cycle time [20, 90] days,, enrichment [20, 94.5]
                    percent), this could be avoided (commit 5a33c61)
            - also (not fixing but probably helping): normalized likelihoods in commit c481d07
                - i.e., use a standard normal distribution. Otherwise the different sigmas will lead
                                                      to not being able to compare likelihoods of
                                                      different isotopes.
            - job with cycle time and enrichment (*cyclerichment*) being run as 22150679
                - 1% relative sigma, 20-94.5% enrichment, 20-90 days, conventional sampling with 1
                                                      chain
            - job with cycle time and enrichment (cyclerichment) running as #22153902
                - 0.1% relative sigma, 20-94.5% enrichment, 20-90 days, iterative sampling
                - acceptable results (same info, but different views):
                    - hist2d (note: long tails) ![](img/histogram_plot_22153902.png)
                    - refined hist2d: ![](img/plot_merge_histogram_22153902.png)
                    - density: ![](img/density_plot_22153902_prelim.png)
                    - trace: ![](trace_plot_22153902_prelim.png)
            - job cyclerichment running as #22154009
                - like #22153902, but with full range (5-95% enrichment, 20-120 days cycle time),
                iterative sampling
                - these worked: ![](img/plot_merge_histogram_22154009.png)
                - initial "stuckness" can be seen in the trace: ![](img/plot_merge_trace_22154009.png)
        - Next steps with Max
            - apply to more complicated (higher dimensional) models
            - infer separation factor (gamma235), personal interest of Max =), use U234 and U236
                                                      concentrations here
            - implement gradients
                - probably good for large sigmas
            - sample natural uranium enrichment
                - does the model still work then?
            - which output parameters to use for more sampling dimensions?
                - e.g. Pu concentrations
                - compare sampling with different sets of output parameters
                - consider product and tails
    - Improved merge script for handling traces.
        - To merge all most-recent traces for a certain job run, as much as
        `python traces/merge.py --jobid 22153902` can be sufficient now.
- **2021-07-14**
    - Models & Sampling
        - Starting work on reconstructing cycle time from isotopic enrichments.
        - Calculated isotopic concentrations in depleted Uranium (after reactor + separations) as function of cycle time:
        ![](img/concentration_by_cycle_time_1.png)
        - However, literature says that U234 concentration decreases with longer irradiation times:
        ![](img/iaea_concentration_by_cycle_time.png)
        - Max has similar results to mine, though
        - Ran model (#22118209) with 0.1% relative sigma (for likelihood); very narrow likelihood means that we
        can pin down cycle time to 1 day (because it is discrete), but not find a distribution.
            - ![](img/trace_plot_cycletime_narrow_22118209.png)
            - on second run, this worked better. With a relative concentration sigma of 1% (i.e.
            `sigma_concentration = 1e-2 * concentration`), we have a very clear distribution;
            ![](img/trace_plot_cycletime_local0.png)
    - Thesis
        - Looking at the plots above: why are we using Bayesian Inference? Could we achieve the same
        in a straight-forward way? Are the benefits only arising once we have multiple dimensions?
- **2021-07-13**
    - Cluster
        - new strategy for running jobs in an organized manner:
            - before each job run, the code including SLURM configuration is committed and tagged as
            `run.601959909` (with the number being the job ID). The job run integer is also given as `--run`
            argument to the script and used as SLURM job ID.
                - note: the first couple jobs used different job IDs, that didn't come from slurm
            - this links job run and code state tightly, making it trivial to identify parameters
            and code at the time of running.
    - Sampling
        - Questioning my sanity over likelihood behavior of model. Sampled overnight
        (enrichment{85,95} -> U236 concentration). ![](img/traceplot_8595_236.png)
        - behavior manifests as such:
```
2021-07-12 23:04:08.587515 :: Mutating: using parameters {'high_enrichment_grade': 90.07299624340567}
2021-07-12 23:04:09.843094 :: Measured concentration for 92236: 9.261522888181698e-05+/-0.0005 True concentration: 9.261522888181698e-05
2021-07-12 23:04:09.843146 :: Likelihood for isotopic concentration {92236: 9.261522888181698e-05} is 7.238003360397916

2021-07-12 23:04:09.857814 :: Mutating: using parameters {'high_enrichment_grade': 87.62104381464947}
2021-07-12 23:04:10.907980 :: Measured concentration for 92236: 9.243492608335802e-05+/-0.0005 True concentration: 9.261522888181698e-05
2021-07-12 23:04:10.908023 :: Likelihood for isotopic concentration {92236: 9.243492608335802e-05} is 7.238249924895927
```
-
    -
        - the first snippet is supposed to have the highest likelihood, because `measured == true`
        concentration (almost, 90.07 vs 90.0)
        - however, the second snippet has a concentration differing from the first one by ~2e-7, yet
        receives a HIGHER likelihood! This is not supposed to happen with a Gauß peak.
        - I wrote a log analysis script in `sampling/util/extract_u236_scatter.jl` to analyze this.
            - Enrichment curve (U236 concentration of enrichment factor):
                ![](img/enrichment_u236.png)
            - Likelihoods by U235 enrichment - this is supposed to have a peak at 90%! (disregard
                    wrong Y axis label) ![](img/likelihood_by_enrichment.png)
            - Likelihoods by concentration - this is supposed to have a peak at 9.2615e-5! (this
                    plot is from a run sampling 5-95% enrichment, but the point stands) ![](img/likelihoods.png)
    - I found an issue that looks like it's causing the problems. Compare the following plots modelling the
        likelihood distribution in my sampler script (mu/sigma like for U236 isotopes):
        - This is the distribution I'm currently using: ![truncated](img/truncatednormal_likelihoods.png)
        - This is a different distribution... ![non-truncated](img/nontruncatednormal_likelihoods.png)
        - The effective bug was that I expected `mu` and the argument to `logp()` to be interchangeable
              (a normal distribution is symmetric). PyMC doesn't agree:
              - ![](img/likelihoods_bug.png)
              - and I even know why: scaling! The TruncatedNormal distribution is obviously scaled s.t.
              $\int P(x) dx = 1$. Thus it is nonsensical to take absolute values!
        - Now the likelihood is correct; this is likelihood (based on U236 concentration) for
        enrichment, with true value at `9.2615e-5`: ![](img/likelihoods_correct.png)
        - The likelihood for different enrichments also looks alright (true enrichment is at 90%):
        ![](img/enrichment_likelihood_u236_correct.png)
        - when sampled straight, the Cyclus model also produces these steps (x - Enrichment, y - U236
                concentration); Max thinks this comes from the fact that enrichment stages are discrete
        and thus the tails enrichment will also show discrete behavior: ![](img/concentration_steps.png)
        - but... while the likelihoods point to the right enrichment and allow us to make predictions,
        PyMC doesn't deliver yet. The `sigma` for calculating likelihoods was at 0.05% (in terms of
                concentration)
            - first job, sampling enrichments between 85-95 percent: ![](img/trace_plot_601959909.png)
            - second job, sampling enrichments between 5 and 95 percent:
            ![](img/trace_plot_789925284.png)
            - it is not clear yet why. A new job is running with a sigma of `1e-5`, i.e. a very clear
            peak.
        - I described this problem in [a Jupyter notebook](../../docs/pathologic_distributions.pdf)
        - now it works -- this is (after the toy example) the **first end-to-end working model as
        described by my thesis' title**, generated by job #22099390:
            - ![](img/trace_plot_22099390.png)
    - Discussion with Max
        - sigma for likelihood of concentrations should be relative 0.1%
- **2021-07-12**
    - Max explained background about archeology/tails compositions. Several ideas:
        - use abundance instead of straight isotope concentration (implemented for one isotopic
                abundance)
        - consider only U234 (possibly U236) instead of all isotopes at once.
        - use more acccurate `sigma`s for calculating the likelihood: <1% for straight mass
        concentrations
        - raise true product enrichment value to 90% (from 85%)
    - sampling
        - implemented a different way of iteratively sampling, enabled by `--iter-sample N`. This
        uses the `pymc3.iter_sample()` API to save snapshots instead of resuming sampling.
        - ran jobs with different configurations
            - 2000 samples, only considering U-236, Slice/Metropolis (one each), 5 nodes
            - 2000 samples, U-234, failed due to occasional lack of U-234.
            - 2 nodes x 4 chains x 5 iterations x 300 samples = 12000 samples via conventional
            sampling (`sample()` instead of `iter_sample()`, for comparison)
        - some weird problems
            - likelihoods *decrease* as we near the "true" value of enrichment (9.2e-5):
                ![](img/likelihoods.png)
            - but! sampling happens mainly around the peak, still. However, only on the "left" side.
            I expect sampling to happen in areas of *high* likelihood.
            - but enrichment function seems correct ![](img/enrichment_u236.png)
            - I am not yet clear about what this means. Possibly a bug in my code, but not sure what
            would cause this
- **2021-07-11**
    - Sampling
        - found glaring issue (it came to me at night...): random seeds for sampling need to be
                                                           updated for each iteration.
        - otherwise, repeated chains (each with a few hundred samples) will be sampled with the very
                                                           same random data
        - see here: ![bad sampling seed](img/pymc_sampling_seed_effect.png)
            - change has effect, even for very few samples: ![good sampling
                                                           seed](img/trace_plot_better_random.png)
        - implemented new random seed selection based on a random array generated from the seed
        - weird error now also showing up on nodes. Whyyy? `2021-07-11 09:16:24.963062 :: Cyclus run finished!
/home/vf962887/.local/lib/python3.8/site-packages/theano/tensor/elemwise.py:826: RuntimeWarning:
divide by zero encountered in log1p`
        - exploiting the "shelling-out" to `cyclus` binary and run chains in parallel!
            - ![parallel sampling](img/parallel_sampling.png)
            - this very likely works fine with one random seed per `sample()` call.
- **2021-07-10**
    - Sampling
        - Sampling isotope composition with sampled product enrichment (5-95%, true value 85%),
        where product enrichment is the enrichment grade of product leaving the spent fuel
        enrichment facility (after reactor and separations).
        - first results: inconclusive after 12h x 5 nodes
            - ![samples](img/trace_plot_enrichment_85.png)
        - this was done on 5 nodes with each 700-800 samples
        - ways to improve?
            - talk to Max and ensure I'm sampling the correct parameter.
            - ensure that I am calculating the likelihood from isotopic concentration correctly!
            currently using `DepletedUraniumSink`
            - do concentrations/depleted uranium need to be weighted somehow?
        - looking forward to physics instruction about fuelcycle on Monday
- **2021-07-09**
    - PyMC3
        - there is a better, more formal method to calculating likelihoods. However, it is slightly
        more complex to implement. After I've implemented isotope extraction, I may get around to
        look more into this; but essentially like the following snippet. However, due to Theano
        maybe I'm totally wrong in understanding this.
```python
import pymc3 as pm
import arviz as az
import numpy as np
import theano.tensor as tt
import numpy as np
def my_own_density_dist(param=None, **kwargs):
    model = pm.model.modelcontext(None)
    print(param)
    return pm.Normal.dist(mu=model.a, sigma=1).logp(kwargs['x'])

with pm.Model() as mod:
    a = pm.Normal('a', mu=0, sigma=1)
    b = pm.DensityDist('b', my_own_density_dist, observed={'x': 3})
    
    infd = pm.sample(8000)
```
- 
    - Cyclus/Sampling
        - Wrote code for extracting isotopes concentrations from simulation output (explicit
                inventory), in `sampling/cyclus_db/extract.py`
        - fixed issue with model: separations emits uranium and plutonium into separated fissile
        stream
        - talked to Max and Malte and came to some conclusions
            - at first, only vary enrichment grade of product (5-95%); set everything else to fixed
            values.
            - reduce simulation time to the necessary minimum
            - skip enrichment step for natural uranium, and use a virtual 1.1% enriched U source
            instead.
            - Work on SRS (Savannah River Site) reactor model for now.
            - burn-up should probably be less than 2 MWd/kg (due to increasing Pu-240
                    concentrations)
            - first goal e.g. deduce product enrichment grade from U-234 concentration in tails.
        - fuelcycle sampling now works on cluster. But: not with multiple jobs on one machine. There
        seems to be interference between individual tasks: `Error deleting file
        'gpr_reactor_input_params.json'`
            - this looks like it is fixed by Max' latest commits, specificall `3f9d7`
        - error after long runtime: `ValueError: unable to infer dtype on variable 'observed';
        xarray cannot serialize arbitrary Python objects`
            - using `observed={'v': [...whatever...]}` as PyMC's likelihood distribution
- **2021-07-08**
    - Got key to the office!
    - Cyclus
        - original goal for today: extract isotope concentrations from simulation data.
        - first problem: problem which ran fine on `sb30` caused a double-free inside the RWTH
        singularity image.
        - Max' patch fixed this:
```diff
diff --git a/src/comp_math.cc b/src/comp_math.cc
index acf26391..3514140a 100644
--- a/src/comp_math.cc
+++ b/src/comp_math.cc
@@ -42,6 +42,32 @@ double Sum(const CompMap& v) {
   return CycArithmetic::KahanSum(vec);
 }
 
+CompMap ApplyThreshold(const CompMap& v, double threshold) {
+  if (threshold < 0) {
+    std::stringstream ss;
+    ss << "The threshold cannot be negative. The value provided was '"
+       << threshold << "'.";
+    throw ValueError(ss.str());
+  }
+  CompMap cm;
+  CompMap::const_iterator it;
+  for (it = v.begin(); it != v.end(); ++it) {
+    if (std::abs(it->second) > threshold) {
+      cm[it->first] = it->second;
+    }
+  }
+  return cm;
+  /*
+  while (it != v->end()) {
+    if (std::abs(it->second) <= threshold) {
+      v->erase(it++);
+    } else {
+      it++;
+    }
+  }
+  */
+}
+
 void ApplyThreshold(CompMap* v, double threshold) {
   if (threshold < 0) {
     std::stringstream ss;
diff --git a/src/comp_math.h b/src/comp_math.h
index 873d319a..be7bd943 100644
--- a/src/comp_math.h
+++ b/src/comp_math.h
@@ -34,6 +34,7 @@ double Sum(const CompMap& v1);
 /// All nuclides with quantities below threshold will have their quantity set to
 /// zero.
 void ApplyThreshold(CompMap* v, double threshold);
+CompMap ApplyThreshold(const CompMap& v, double threshold);
 
 /// The sum of quantities of all nuclides of v is normalized to val.
 void Normalize(CompMap* v, double val = 1.0);
diff --git a/src/material.cc b/src/material.cc
index 6f3d8c80..28aceb6f 100644
--- a/src/material.cc
+++ b/src/material.cc
@@ -82,7 +82,8 @@ Material::Ptr Material::ExtractComp(double qty, Composition::Ptr c,
     CompMap otherv(c->mass());
     compmath::Normalize(&otherv, qty);
     CompMap newv = compmath::Sub(v, otherv);
-    compmath::ApplyThreshold(&newv, threshold);
+    newv = compmath::ApplyThreshold(newv, threshold);
+    //compmath::ApplyThreshold(&newv, threshold);
     comp_ = Composition::CreateFromMass(newv);
   }
```
-
    -
        - next problem: the model runs fine as `cyclus candu.json` (where `candu.json` is the input
            file generated from `cyclus/natu-candu-heu/generate.py`, but it doesn't when attempted
            to run via the `cyclus` python module (`SimState`). Upon `sim.load()`:
```
Traceback (most recent call last):
  File "test_extract.py", line 19, in <module>
    main()
  File "test_extract.py", line 16, in main
    simst.run()
  File "/home/vf962887/.local/lib/python3.8/site-packages/cyclus-1.5.5-py3.8.egg/cyclus/simstate.py", line 195, in run
    self.si.timer.run_sim()
  File "lib.pyx", line 1197, in cyclus.lib._Timer.run_sim
ImportError: '_cyclus_eventhooks' is not a built-in module
Segmentation fault (core dumped)
```
-
    -
        - this looks again like maybe a cython problem?
        - next attempt: wrap `cyclus` binary, as suggested by Max earlier.
- **2021-07-07**
    - Progress presentation at group meeting. Very interesting discussion
        - Malte: prepare *useful* scenario to demonstrate benefits of method
        - Antonio: attempt using gradients
            - I will investigate this
        - Christopher: visualize 2D random walk
            - this is now implemented as `sampling/visualize/walk2d.py` as walk and histogram
            - ![img/walk2d.png](img/walk2d.png)
            - ![img/walkhist.png](img/walkhist.png)
    - marked [cyclus#1563](https://github.com/cyclus/cyclus/pull/1563) as ready
    - debugging CANDU (fake so far) fuel cycle for simulation. So far, nothing is produced!
        - some facilities were missing etc. Now the natural uranium enrichment facility doesn't
        enrich anything yet, doesn't use feed (but succeeds in buying it)
        - reactor isn't properly run? only two `LOAD` events, nothing else. cycle time should be
        properly set?
        - talked to Max, and with some additional adjustments it works
            - consider simulation time: needs to be long enough for enrichment from NatU to be
            sufficient
            - reactor core must be of appropriate size
            - separations facility has useful output stream!
            - sinks specify recipes in order for enrichment to know how much to enrich
            - some other general mishaps (missing commodities, typos, etc.)
    - installed `misoenrichment` on `sb30` (private server) in anaconda environment, to keep skills
    fresh
        - on error `/home/lbo/.anaconda3/envs/fuelcyclesrc/lib/libpython3.6m.so.1.0: undefined
        reference to `clock_getres@GLIBC_2.17'`, install from conda-forge `sysroot_linux-64=2.17`
- **2021-07-06**
    - Prepared first progress presentation (see `docs/presentation_20210707.sozi.pdf`)
    - doctor's appointment :(
- **2021-07-05**
    - Talked with Max about the actual fuelcycle to simulate
        - see reading list
        - sampling on cluster works, with reasonable failure tolerance (snapshotting), Slice and
        Metropolis both give more or less satisfying results. (Slice seems better though)
    - Implemented first model for that purpose
        - Cyclus runs, but nothing works yet: no resources are produced, no inventory exists
        - will need to debug this later
- **2021-07-04**
    - Make Metropolis/SMC choice switchable (`--algorithm={default,metropolis}`)
        - Run comparison. Some minor fixes to logging, etc.
        - Consider putting log output to WORK
        - at first glance, Metropolis seems to be a lot faster. (see below for weird dimension
                problem, looks like a bug)
    - Enhance output path configuration for samples: all paths explicit (except temp paths)
    - Sampling rate: `Sampling 1 chain for 10 tune and 100 draw iterations (10 + 100 draws total)
    took 225 seconds.`
    - weird error: `/home/vf962887/.local/lib/python3.8/site-packages/arviz/data/base.py:169:
    UserWarning: More chains (100) than draws (2). Passed array should have shape (chains, draws,
            *shape)`
        - only appears when using `Metropolis` algorithm
        - I filed [#4839](https://github.com/pymc-devs/pymc3/issues/4839) in PyMC
    - comparison
        - Metropolis at 500 samples per node... not that good ![metropolis
        trace](img/trace_plot_metropolis.png)
            - 33m, but not very good results
        - Slice has quite a better result: ![slice trace](img/trace_plot_slice_15x5x200.png)
            - 3h20m for 5x100 samples. But: useful result
            - Note that... Uranium enrichment level (second plot) probably really doesn't have much
            to do with produced energy (what we're measuring). Given that we sample on the reactor
            power output, it isn't surprising that this is the *main* factor in produced energy, and
            thus the only relevant factor. Even with more highly enriched Uranium, the reactor power
            output won't change.
    - MIso
        - How to run GPR part to generate JSON inputs to cyclus simulation?
- **2021-07-03**
    - Merged sample files from 15-node overnight run. tl;dr it mostly works!
        - Scenario: "sampling0.json" fuelcycle, one reactor, mine; 1178 MW power, 4% enrichment
        (unknown parameters); generated power is observed.
        - Power detection worked well, enrichment detection not so well (probably nonsensical
                parameter?): ![traceplot](img/attempt_sampling_trace.png)
    - Cycled again, finally; and studied condensed matter physics. No time for cyclus!
- **2021-07-02**
    - Cyclus
        - Make cyclus write `cyclus.h5` to a defined file. Is this the log?
        - Crash after long runtime - this is still what I've seen earlier:
```
Sampling 1 chain for 40 tune and 250 draw iterations (40 + 250 draws total) took 35044 seconds.
Traceback (most recent call last):
  File "/home/vf962887/cyclus/src/bayesian-cycle/sampling/attempt_sampling/run.py", line 81, in <module>
    
  File "/home/vf962887/cyclus/src/bayesian-cycle/sampling/attempt_sampling/run.py", line 75, in main
    ssc = SimpleSampleCyclus("sampling0.json", csk={'output_path': cyclus_output_path()})
  File "/home/vf962887/.local/lib/python3.8/site-packages/pymc3/sampling.py", line 639, in sample
    idata = arviz.from_pymc3(trace, **ikwargs)
  File "/home/vf962887/.local/lib/python3.8/site-packages/arviz/data/io_pymc3.py", line 563, in from_pymc3
    return PyMC3Converter(
  File "/home/vf962887/.local/lib/python3.8/site-packages/arviz/data/io_pymc3.py", line 171, in __init__
    self.observations, self.multi_observations = self.find_observations()
  File "/home/vf962887/.local/lib/python3.8/site-packages/arviz/data/io_pymc3.py", line 184, in find_observations
    multi_observations[key] = val.eval() if hasattr(val, "eval") else val
  File "/home/vf962887/.local/lib/python3.8/site-packages/theano/graph/basic.py", line 554, in eval
    self._fn_cache[inputs] = theano.function(inputs, self)
  File "/home/vf962887/.local/lib/python3.8/site-packages/theano/compile/function/__init__.py", line 337, in function
    fn = pfunc(
  File "/home/vf962887/.local/lib/python3.8/site-packages/theano/compile/function/pfunc.py", line 524, in pfunc
    return orig_function(
  File "/home/vf962887/.local/lib/python3.8/site-packages/theano/compile/function/types.py", line 1970, in orig_function
    m = Maker(
  File "/home/vf962887/.local/lib/python3.8/site-packages/theano/compile/function/types.py", line 1584, in __init__
    fgraph, additional_outputs = std_fgraph(inputs, outputs, accept_inplace)
  File "/home/vf962887/.local/lib/python3.8/site-packages/theano/compile/function/types.py", line 188, in std_fgraph
    fgraph = FunctionGraph(orig_inputs, orig_outputs, update_mapping=update_mapping)
  File "/home/vf962887/.local/lib/python3.8/site-packages/theano/graph/fg.py", line 162, in __init__
    self.import_var(output, reason="init")
  File "/home/vf962887/.local/lib/python3.8/site-packages/theano/graph/fg.py", line 330, in import_var
    self.import_node(var.owner, reason=reason)
  File "/home/vf962887/.local/lib/python3.8/site-packages/theano/graph/fg.py", line 383, in import_node
    raise MissingInputError(error_msg, variable=var)
theano.graph.fg.MissingInputError: Input 0 of the graph (indices start from 0), used to compute sigmoid(powercap_interval__), was not provided and not given a value. Use the Theano flag exception_verbosity='high', for more information on this error.
```
- 
    - 
        - We also see: 120s per sample! D:
            - will improve with shorter simulations.
        - this error is similar to [this issue](https://discourse.pymc.io/t/blackbox-likelihood-example-doesnt-work/5378)
            - I've applied two changes (no convergence check, and start point) to see if it works better now.
            - only convergence check makes a difference, but it was also [issue 48](https://github.com/pymc-devs/pymc-examples/issues/48#issue-839287296) in pymc-examples.
                - both convergence check and density dist obs seem to cause the same error
        - new error, after saving first chain, before resuming on first chain:
```
Traceback (most recent call last):
  File "/home/vf962887/cyclus/src/bayesian-cycle/sampling/attempt_sampling/run.py", line 102, in <module>
    main()
  File "/home/vf962887/cyclus/src/bayesian-cycle/sampling/attempt_sampling/run.py", line 93, in main
    trace = pm.sample(100, tune=10, chains=1, return_inferencedata=True, 
  File "/home/vf962887/.local/lib/python3.8/site-packages/pymc3/sampling.py", line 597, in sample
    trace = _sample_many(**sample_args)
  File "/home/vf962887/.local/lib/python3.8/site-packages/pymc3/sampling.py", line 713, in _sample_many
    trace = _sample(
  File "/home/vf962887/.local/lib/python3.8/site-packages/pymc3/sampling.py", line 855, in _sample
    for it, (strace, diverging) in enumerate(sampling):
  File "/home/vf962887/.local/lib/python3.8/site-packages/fastprogress/fastprogress.py", line 47, in __iter__
    raise e
  File "/home/vf962887/.local/lib/python3.8/site-packages/fastprogress/fastprogress.py", line 41, in __iter__
    for i,o in enumerate(self.gen):
  File "/home/vf962887/.local/lib/python3.8/site-packages/pymc3/sampling.py", line 978, in _iter_sample
    strace = _choose_backend(trace, chain, model=model)
  File "/home/vf962887/.local/lib/python3.8/site-packages/pymc3/sampling.py", line 1381, in _choose_backend
    return NDArray(vars=trace, **kwds)
  File "/home/vf962887/.local/lib/python3.8/site-packages/pymc3/backends/ndarray.py", line 219, in __init__
    super().__init__(name, model, vars, test_point)
  File "/home/vf962887/.local/lib/python3.8/site-packages/pymc3/backends/base.py", line 66, in __init__
    self.varnames = [var.name for var in vars]
  File "/home/vf962887/.local/lib/python3.8/site-packages/pymc3/backends/base.py", line 66, in <listcomp>
    self.varnames = [var.name for var in vars]
AttributeError: 'str' object has no attribute 'name'
```
- 
    - not sure what this is yet.
        - this is a consequence of using `return_inferencedata=True`, but `sample()` expecting a `MultiTrace`. By changing types, this now works.
    - ...but failed due to OOM on the next attempt!
        - memory and runtime optimizations needed!
            - use H5; this doesn't leak file descriptors; delete files after simulation run
            - use `$TMP` as working directory
            - close `MemBack` instances
            - still, growing memory use is a concern.
    - running a 15-task job overnight at 5x400 samples per task -> 30k samples
    - **Talk with Max**
        - Ideas
            - Simulation: We will use miso_enrichment scenarios
                - need to get miso_enrichment running for our use
                - installed & ready!
            - Simulation time probably << 100 steps, more likely 10-20. Runtime then hopefully only a few seconds
            - Likely have to use explicit inventory
            - Attempt to save to SQLite instead of HDF5; write to disk and analyze from there, it is more convenient than Pandas dataframes.
            - Use project ID for accounting CPU time. Which project ID?
            - Bug-resistant programming
                - use `iter_sample()`, which generates a trace on each iteration?
                - continue on saved traces.
            - Attempt using Metropolis algorithm, alternatively SMC
        - Talk to Max and Malte about specific simulation scenario by Thu Jul 8th at the latest

- **2021-07-01**
    - Singularity
        - Reading documentation about [bind mounts](https://sylabs.io/guides/3.7/user-guide/bind_paths_and_mounts.html)
    - Cluster
        - Familiarizing with [compute quota](https://help.itc.rwth-aachen.de/en/service/rhr4fjjutttf/article/fed436424d464e01ac7add680162a852/) etc.
        - As student, I have 6k core-hours (per year) available. This is e.g. sampling on 60 instances for 100 hours -> not *that* much! At 15s per sample, ~1e6 samples.
        - I qualify for RWTH-Thesis quota; Malte would need to apply. Then I can use up to 48k core-hours, which should be enough.
    - Cyclus
        - Note that once installed, the SOs are linked assuming the build system's
    paths. I.e., for me, SOs in ~/.local are linked to libraries in
    /home/vf962887 (because that's my uid): `
  libcyclus.so => /home/vf962887/.local/lib/libcyclus.so (0x00002af1c1879000)`
        - this can be changed using patchelf or similar.
        - Next step is finally finding out *what* to simulate. Otherwise I'm now at
      the point where cluster-level Cyclus sampling is ready, and we just need to
      decide on what to sample. Then we can start the test runs and gradually
      improve code. The `distributed_demo` code can serve as template for the
      cyclus-sampling scripts.
            - first entry may be attempting something close to Antonio's/Malte's paper.
            - also want to find out more about gaussian process regression
        - by the way - using Python input files now works in the singularity container, as opposed to the anaconda environment.
        - Got `sampling/attempt_sampling` to run: this is the first more-or-less useful blackbox sampling script able to produce real-world output.
            - Let it run with a few hundred samples overnight.
            - like this: `singularity exec /rwthfs/rz/SW/UTIL.common/singularity/cyclus /bin/bash -c 'pushd /home/vf962887/cyclus/src/bayesian-cycle/sampling && PYTHONPATH=. python3 ~/cyclus/src/bayesian-cycle/sampling/attempt_sampling/run.py'`
            - SLURM script committed; running. Let's hope and see!
    - PyMC3
        - I wasted time and wrote an article about Bayesian inference: [lewinb.net](https://lewinb.net/posts/05_bayesian-inference/)
        - I read a recommendation about using [SMC](https://docs.pymc.io/notebooks/SMC2_gaussians.html) for non-differentiable models
        - Reading about [Gibbs Sampling](https://www.siam.org/Portals/0/Publications/SIURO/Volume%2011/S016609.pdf?ver=2018-06-13-135921-713); not extremely relevant, but good to know probably. Somewhat unclear how this is applied to typical PyMC3 models?
- **2021-06-30**
    - PyMC3
        - How to sample chains individually and store/merge samples?
        - Test run parallel sampling on cluster? Using a different model (without blackbox)
          - merge\_traces?
            - [https://discourse.pymc.io/t/how-is-merge-traces-to-be-used/1991](https://discourse.pymc.io/t/how-is-merge-traces-to-be-used/1991)
            - [https://github.com/pymc-devs/pymc3/blob/main/pymc3/backends/base.py#L562](https://github.com/pymc-devs/pymc3/blob/main/pymc3/backends/base.py#L562) (take care to get unique chain numbers)
          - [https://github.com/pymc-devs/pymc3/pull/4495](https://github.com/pymc-devs/pymc3/pull/4495) → pymc3 &gt;= 3.11.2 is required, although that also didn&#39;t fix it? → because not actually included in 3.11.2!
            - Attempting hotfix: _git cherry-pick cf662_ on top of release 3.11.2 applies cleanly
            - ...this doesn&#39;t work properly, because arviz is also involved ([https://github.com/arviz-devs/arviz/pull/1590](https://github.com/arviz-devs/arviz/pull/1590)).
          - Give up on **merge\_traces** because apparently _nobody_ else uses it. Instead,use [https://arviz-devs.github.io/arviz/api/generated/ **arviz.concat**.html](https://arviz-devs.github.io/arviz/api/generated/arviz.concat.html) instead; but still needs patches from above!
          - Added cluster test program in `sampling/distributed_demo/` for testing
          how running a PyMC3 sampler in parallel on many nodes will work.
            - Seems to run fine. Use two scripts: One running on each node (with
                `--index` argument for distinguishing and random seeding), and one
            to merge results.
          - Use job arrays to run samplig in parallel:   
            - [-> Slurm](https://slurm.schedmd.com/job_array.html)
      - Singularity
          - need to install updated PyMC3 and arviz versions, if we are to use non-0
          chain indices.
          - model can be run like `singularity exec /rwthfs/rz/SW/UTIL.common/singularity/cyclus ~/bin/python ~/cyclus/src/bayesian-cycle/sampling/distributed_demo/model.py <args>`
- **2021-06-29**
    - Cyclus
        - PR for improving website/install instructions: [https://github.com/cyclus/cyclus/pull/1563](https://github.com/cyclus/cyclus/pull/1563)
    - Singularity
        - Attempting to run my unit tests on the RWTH cluster
        - _while extracting cyclus0.sif: root filesystem extraction failed: extract command failed: FATAL: singularity image is not in an allowed configured path_
        - solution: use &quot;official&quot; image /rwthfs/rz/SW/UTIL.common/singularity/cyclus which has its own problems however
    - Build on cluster(singularity)
        - cmake -DCMAKE\_C\_COMPILER=gcc -DCMAKE\_CXX\_COMPILER=g++ ..
        - and/or install.py
        - it runs!
        - Cycamore too? ...installing… and it runs too!
        - Simple simulation works
        - but: sanity test (python3 -m unittest) of blackbox sampling code crashes with a segmentation fault. GDB not available, unfortunately
        - Next step: attempt reproduction from scratch, which part of cyclus will crash?
          - This is [https://github.com/cyclus/cyclus/issues/1543](https://github.com/cyclus/cyclus/issues/1543) – tl;dr: cython 0.28.5 works, cython 0.29 doesn&#39;t. This is a problem because RWTH&#39;s cyclus container provides Cython 0.29 by default. Installing Cython 0.28 in ~/.local and recompiling cyclus solves this.
        - See cyclus\_singularity\_rwth\_working\_deps.txt and cyclus\_local\_1.tbz2 in sciebo: _ba\_lewin/cyclus/rwth\_cluster_ directory_
    - Formalities
        - Sent registration sheet for Bsc thesis to ZPA
- **2021-06-28**
    - Singularity
        - Getting to run the cyclus docker image in singularity
        - [https://hpc.fau.de/files/2020/05/2020-05-12-singularity-containers.pdf](https://hpc.fau.de/files/2020/05/2020-05-12-singularity-containers.pdf)
        - How to run singularity with cyclus inside: [https://git.rwth-aachen.de/-/snippets/998](https://git.rwth-aachen.de/-/snippets/998)
        - How to convert the existing docker container: _singularity build --sandbox cyclus0 docker-archive://cyclus.tar_
        - [https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/blob/master/singularity/NOTES.md](https://git.rwth-aachen.de/nvd/fuel-cycle/bayesian-cycle/-/blob/master/singularity/NOTES.md)
    - Cyclus
        - Dev meeting (8pm): Talked about problems with building cyclus. No conclusion, except that it&#39;s a topic for a hackathon and that cyclus is unable to keep up with Anaconda&#39;s dependency upgrades
    - PyMC
        - Update sampling method to use Metropolis. For some reason, NUTS appeared to be working?
        - Finished a small run with fake model in docker container; received exception :( [https://git.rwth-aachen.de/-/snippets/997](https://git.rwth-aachen.de/-/snippets/997)
    - Meeting with Max to explain initial code.
- **2021-06-24**
    - Cyclus
        - running in its docker container for now
    - PyMC
    - Simulation
        - [https://git.rwth-aachen.de/lewin/simple-fuel-cycles](https://git.rwth-aachen.de/lewin/simple-fuel-cycles)
        - Thinking about the best way to calculate the log-likelihood flexibly and efficiently
        - implemented barebones working implementation; see [https://git.rwth-aachen.de/lewin/simple-fuel-cycles/-/blob/master/simple0/blackbox/test\_blackbox.py#L72](https://git.rwth-aachen.de/lewin/simple-fuel-cycles/-/blob/master/simple0/blackbox/test_blackbox.py#L72)
        - Still one or another error during sampling, especially from Theano.
          - [https://stackoverflow.com/questions/62800160/theano-error-when-using-pymc3-theano-gof-fg-missinginputerror](https://stackoverflow.com/questions/62800160/theano-error-when-using-pymc3-theano-gof-fg-missinginputerror)
          - [https://github.com/pymc-devs/pymc3/issues/4002](https://github.com/pymc-devs/pymc3/issues/4002)
- **2021-06-23**
    - Cyclus
        - Work on PR [https://github.com/cyclus/cyclus/pull/1560](https://github.com/cyclus/cyclus/pull/1560)
        - Created PR [https://github.com/cyclus/cyclus/pull/1561](https://github.com/cyclus/cyclus/pull/1561)
        - MORE shenanigans, build stopped working after investigating why cyclus (binary) doesn&#39;t accept python simulation inputs... time wasting stuff
        - complaints [https://github.com/cyclus/cyclus/issues/1562](https://github.com/cyclus/cyclus/issues/1562)
    - PyMC
        - Created sketch of how I imagine the (black-box) inference process: ![](img/cyclus_sampling.png) 
    - Simulation
        - Start integration of cyclus and python: [https://git.rwth-aachen.de/-/snippets/993](https://git.rwth-aachen.de/-/snippets/993)
        - segfault in python library:
        - _CYCLUS\_RNG\_SCHEMA=&#39;/home/lbo/.local/share/cyclus/cyclus-flat.rng.in&#39;
 python \&lt;\&lt;\&lt; &#39;import cyclus; import blackbox.blackbox as bb; m = bb.CyclusModel(&quot;simple.json&quot;); m.simulate()&#39;_
        - [https://git.rwth-aachen.de/-/snippets/995](https://git.rwth-aachen.de/-/snippets/995)
        - This segfault doesn&#39;t happen in the cyclus docker container. I&#39;m afraid we will need to use that one. What a shame... but whatever
    - **The plan is: wrap cyclus in a small-ish library that allows loading a base model and &quot;mutating&quot; individual parameters, and then running an in-process simulation on the fly. From that, desired result parameters should be extracted from the resulting sqlite/** [**memback**](https://fuelcycle.org/python/memback.html?highlight=memback#module-cyclus.memback) **.**
- **2021-06-22**
    - PyMC black box inference
        - [https://docs.pymc.io/notebooks/blackbox\_external\_likelihood.html](https://docs.pymc.io/notebooks/blackbox_external_likelihood.html)
        - [https://docs.pymc.io/Probability\_Distributions.html](https://docs.pymc.io/Probability_Distributions.html)
    - Goal: With a simple fuel cycle model,
        - what are the variables (and their priors)
        - what are the outcomes (and their likelihoods)
        - how will the PyMC model look like, roughly
    - More PyMC/MC playing:
        - &quot;card problem&quot;, what is the true distribution of football player cards based on a sample. (see pymc/discrete\_problems.pdf)
        - and some other stuff
    - Cyclus
        - Built cyclus+cycamore in its docker container. This works more or less (need to /opt/conda/bin/conda install libsigcpp=2)
        - Cyclus python library: [https://fuelcycle.org/python/index.html](https://fuelcycle.org/python/index.html)
    - Discussion with Max
        - installing cyclus
        - which fuelcycle to use?
        - which variables will be inferred etc.

- **2021-06-21**
      - first cyclus pull request [https://github.com/cyclus/cyclus/pull/1560](https://github.com/cyclus/cyclus/pull/1560)
      - with new learnings, misoenrichment also builds now
      - studious\_potato
        - model is generated now (via JSON is easy), but (both anaconda and source-compiled) cyclus crash with core dump...
        - pointing to issue in cbc, but CbcMain1 is FULL of abort() calls, cf. [https://github.com/coin-or/Cbc/blob/master/src/CbcSolver.cpp](https://github.com/coin-or/Cbc/blob/master/src/CbcSolver.cpp)
```
#0 0x00007ffff3fd42a2 in raise () from /lib64/libc.so.6
#1 0x00007ffff3fbd8a4 in abort () from /lib64/libc.so.6
#2 0x00007fffef493b59 in **CbcMain1** (int, char const\*\*, CbcModel&amp;, int (\*)(CbcModel\*, int), CbcSolverUsefulData&amp;) () from /home/lbo/.anaconda3/envs/fuelcycle155/lib/./libCbcSolver.so.3
#3 0x00007ffff74d5965 in cyclus::SolveProg(OsiSolverInterface\*, double, bool) () from /home/lbo/.anaconda3/envs/fuelcycle155/lib/libcyclus.so
#4 0x00007ffff74cff72 in cyclus::ProgSolver::SolveGraph() () from /home/lbo/.anaconda3/envs/fuelcycle155/lib/libcyclus.so
#5 0x00007ffff74b4c4a in cyclus::ExchangeManager\&lt;cyclus::Material\&gt;::Execute() () from /home/lbo/.anaconda3/envs/fuelcycle155/lib/libcyclus.so
#6 0x00007ffff74ba57c in cyclus::Timer::DoResEx(cyclus::ExchangeManager\&lt;cyclus::Material\&gt;\*, cyclus::ExchangeManager\&lt;cyclus::Product\&gt;\*) () from /home/lbo/.anaconda3/envs/fuelcycle155/lib/libcyclus.so
#7 0x00007ffff74ba7f8 in cyclus::Timer::RunSim() () from /home/lbo/.anaconda3/envs/fuelcycle155/lib/libcyclus.so
 #8 0x000055555556e0a5 in main ()
```
-   
      - Cyclus unit tests crash in boost!? Maybe wrong version.
      - [https://www.world-nuclear.org/information-library/nuclear-fuel-cycle/conversion-enrichment-and-fabrication/uranium-enrichment.aspx](https://www.world-nuclear.org/information-library/nuclear-fuel-cycle/conversion-enrichment-and-fabrication/uranium-enrichment.aspx)
        - 50 kWh/SWU for modern centrifuge plants, electric energy responsible for 50% of fuel cost, 5% nuclear energy cost. 60-160 USD/SWU, 8-9 SWU/ton for LEU
      - [https://www.world-nuclear.org/information-library/nuclear-fuel-cycle/conversion-enrichment-and-fabrication/fuel-fabrication.aspx](https://www.world-nuclear.org/information-library/nuclear-fuel-cycle/conversion-enrichment-and-fabrication/fuel-fabrication.aspx)
        - conversion of UF6 into UO2 powder, pelleting, drying (firing), assembly into Zr tubes (fuel assemblies)
        - How are assemblies transported to the reactor without overheating? Inserted control rods?
      - [https://www.world-nuclear.org/information-library/nuclear-fuel-cycle/fuel-recycling/processing-of-used-nuclear-fuel.aspx](https://www.world-nuclear.org/information-library/nuclear-fuel-cycle/fuel-recycling/processing-of-used-nuclear-fuel.aspx)
      - PyMC3 exercise in Jupyter

![pymc_exercise](img/pymc_exercise.png)

- **2021-06-20**
    - Still trying to build cyclus...but giving up, it doesn&#39;t seem to work. Too many dependencies, cmake configuration not robust enough
        - _conda install pkgconfig_ helps with finding local libraries..
        - on fedora, _sqlite-devel_ helps with the mysterious sqlite3d library (cmake/FindSqlite.cmake)
        - ...it finalllllyyyy worked (I think it was the pkg-config, anaconda gcc, and sqlite-devel combination...)
        - Note: sometimes _cyclus-build-deps_ from conda-forge is evil (relates to cyclus-1.5.3!)
    - Reading background material about nuclear fuel cycle (_Nuclear Fuel Cycle Overview)_ from reading list
        - Preparation of natural Uranium: mining/milling/conversion/enrichment/fuel fabrication (&quot;front end&quot;)
        - Enriched Uranium spends about three years in a reactor, then temporarily stored/reprocessed/recycled, waste product is disposed of
        - Uranium 500x more abundant than Gold
        - Mining: method depends on nature of orebody, safety, economics
        - open pit mining when depth less than 120m
        - open pit mining holes much larger than deposit
        - in-situ leaching removes Uranium by dissolving it in oxygenated water (weak acid/alkaline solution)
        - Milling separates Uranium from ore or leachate; ore is crushed to a slurry, dissolved in H2SO4, precipitated as U3O8 concentrate (yellowcake)
        - 200t of U3O8 required to run a reactor for one year (1GWe)
        - Remainder of ore -\&gt; tailings, stored e.g. in old mine. Isolation from environment required (radioacctive)
        - Natural Uranium has 7/1000 fissile U-235 (remains 993/1000 U-238). Reactors need 35-50/1000 U-235
        - Enrichment happens in gaseous phase; Uranium oxide -\&gt; uranium dioxide UO2 (usable for non-enriched-capable reactors) -\&gt; UF6 (hexafluoride).
        - Enrichment plant separates UF6 into enriched stream and tails stream (depleted U)
        - Commercial enrichment uses centrifuges
        - Reactor fuel in form of pellets made of UO2, encased in Zirconium rods, arranged into fuel assembly
        - 27t of fresh fuel to run a 1GWe reactor for a year
        - 1GWe reactor contains about 75t of low-enriched U, generating heat by means of a chain reaction
        - U-238 partially converted into Pu, partially fissioned (1/2), providing 1/3 of energy output
        - 1/3 of spent fuel removed every 18 mo. Correlated with use of burnable absorbers
        - 1t of natural uranium -\&gt; 44e6 kwh electrical energy, equivalent to 20e3 t of coal
        - fuel burn-up: gigawatt-days per tonne (proportional to enrichment level)
        - burn-up limited to ~40GWd/t (4% enrichment). New equipment: 55 GWd/t (5% enrichment), potentially 70 GWd/t (6%). Higher burn-up: longer fuel cycles (24mo)
        - CANDU reactors (natural uranium), low burnup, 7.5GWd/t (equiv. 50GWd/t for enriched fuel)
        - 60GWd/t equivalent to 6.5 atomic percent burnup. Used for oxide fuels; metal fuels use atomic percent metric.
        - 2/3 of generated heat dissipates into cooling medium
        - Concentration of fission products/heavy elements increases until use no longer efficient. Spent fuel removed every 18-36 mos
        - Spent fuel has 1% U-235, 0.6% fissile Pu, 95% U-238. Remainder: fission products, minor actinides
        - Spent fuel emits radiation and heat. Storage pool allows cooling down, takes months-years
        - Spent fuel may be transferred to central facilities
        - Two options for spent fuel:
            - Reprocessing for recovering/recycling the usable portion
            - Long-term storage and final disposal without reprocessing
        - **Reprocessing**
            - Used fuel: 96% of original U, but \&lt;1% U-235, 3% waste products, 1% Pu
            - Separate U, Pu from waste, by dissolving in acid. Recycling of U and Pu into new fuel. Waste amount is reduced
            - 3% waste is high-level radioactive (750 kg / year from 1GWe reactor)
        - **Recycling**
            - spent fuel still has higher concentration of U-235; can be reused as fuel after conversion+enrichment
            - Pu can be made into Mixed Oxide (MOX) fuel - PuO + UO. Pu substitutes for U-235
            - Areva: eight fuel assemblies -\&gt; one MOX fuel assembly, 2/3 U assembly, 3t depleted U, 150kg waste. Reduces natural Uranium demand by 12t
            - REMIX (not commercial yet): non-separated mix of Pu+U combined with low-enriched (17% U-235) U. -\&gt; Fuel with 1% Pu-239, 4% U-235
            - U-238 not usable in fuel cycle (apart from incidental transformation). Fast neutron reactors fission U-238 however, and produce Pu.
        - Waste levels: low/intermediate/high.
            - high-level waste is dried and melted into glass bodies poured inside steel canisters. 1 yr of waste of 1 GWe reactor fills 5t of glass (12 canisters 1.3x0.4 m)
            - Depleted uranium used for high-density (high-mass) applications, like shielding, ammunition
        - Final disposal
            - No good places for final disposal known yet
            - spent fuel can be a good energy source in the future, once recycling methods have advanced
        - Alternatiive sources
            - military uranium (50% of civil uranium in US came from Russia, in 2013)
      - [https://www.world-nuclear.org/](https://www.world-nuclear.org/) — careful: Nuclear lobbying group
      - [https://www.world-nuclear.org/information-library/nuclear-fuel-cycle/conversion-enrichment-and-fabrication/conversion-and-deconversion.aspx](https://www.world-nuclear.org/information-library/nuclear-fuel-cycle/conversion-enrichment-and-fabrication/conversion-and-deconversion.aspx)
      - [https://www.world-nuclear.org/information-library/nuclear-fuel-cycle/conversion-enrichment-and-fabrication/uranium-enrichment.aspx](https://www.world-nuclear.org/information-library/nuclear-fuel-cycle/conversion-enrichment-and-fabrication/uranium-enrichment.aspx)
- **2021-06-19**
    - Expanding on Cyclus simulation scenario [https://fuelcycle.org/user/tutorial/add\_second\_reactor.html](https://fuelcycle.org/user/tutorial/add_second_reactor.html)
    - attempting to build cyclus from source, in case of modifications or optimizations
        - some small source level fixes (coincbc version determination doesn&#39;t work, ...); install conda gcc/gxx as advised in README ( **gcc\_linux-64, gxx\_linux-64** )
        - proposed [https://github.com/cyclus/cyclus/issues/1312#issuecomment-864413668](https://github.com/cyclus/cyclus/issues/1312#issuecomment-864413668)
        - fixable with brute force: [https://github.com/cyclus/cyclus/issues/1548](https://github.com/cyclus/cyclus/issues/1548)
- **2021-06-18**
    - Cyclus now installed, even misoenrichment works (except for some bugs preventing it from completing)
    - studious\_potato and misoenrichment don&#39;t work well with cyclus yet, due to missing auxiliary files etc.
    - Simpler input files probably good for starting out
        - [https://git.rwth-aachen.de/lewin/simple-fuel-cycles/](https://git.rwth-aachen.de/lewin/simple-fuel-cycles/)
    - First successful &amp; productive cyclus run!
        - _cyclus simple.xml -o result.sqlite_
        - good for becoming accustomed to the database schema of results
        - will try to play a bit and map changes in configuration to changes in output
        - how will this play with the blackbox PyMC calculation, etc.
- **2021-06-17**
    - Cyclus
        - want to run basic simulations
        - Explicit inventory: saves all buffers as snapshots; causes slowness in combination with decay option
    - Other software; building miso\_enrichment to get a feeling for plugins
        - **miso-enrichment** : needs all(?) coin-or packages **,** and _nlohmann\_json_ from anaconda
        - libstdc++ linking issue (g++ links against system libstdc++ which is newer than anaconda&#39;s, but ld.so uses anaconda one -\&gt; version mismatch): solve by installing gcc\_linux-64, gxx\_linux-64 ([https://github.com/conda/conda-build/issues/3080](https://github.com/conda/conda-build/issues/3080) )
        - libsigc not found: _conda install -c conda-forge libsigcpp=2.10.0_
        - ...unit tests run now
    - Installing Cyclus
        - Use python=3.6 environment
        - use _conda install -c conda-forge cycamore_
    - **Installing Cyclus in 2021** (_works on my machine_)
        - use anaconda, _conda create -n fuelcycle python=3.6_
        - Problem: &#39;_conda install -c conda-forge cycamore_&#39; ends up requiring S.O. libhdf5\_cpp.so.101, which is... arcane and may have existed in Ubuntu 16.04 (!?)
        - Solution: install _that one specific build of a couple years ago_ of version 1.5.3: **conda install cyclus=1.5.3=py36\_blas\_openblas\_2 -c conda-forge**
        - then install _cycamore_, which will get a version that depends on _cyclus_ without version requirement. We end up with 1.5.3 of both cyclus and cycamore
        - _cyclus\_unit\_tests_ and _cycamore\_unit\_tests_ then both work, even without a segfault. (but non-deterministically - second install segfaults again)
    - Cyclus Files: [https://github.com/maxschalz/studious\_potato/tree/main/data/run\_two\_repositories\_2MWd\_0/input\_files](https://github.com/maxschalz/studious_potato/tree/main/data/run_two_repositories_2MWd_0/input_files)
    - for running: [https://github.com/maxschalz/miso\_enrichment](https://github.com/maxschalz/miso_enrichment)
        - for building: possibly set BOOST\_ROOT
    - &quot;cyclus live demo&quot; [https://github.com/nuclearkatie/cyclus-live-demo/tree/main/separate-ore](https://github.com/nuclearkatie/cyclus-live-demo/tree/main/separate-ore)
    - Understood PyMC enough to reimplement simple example without framework: [https://gist.github.com/dermesser/54e1389880fb7b6b661df898aeca90af](https://gist.github.com/dermesser/54e1389880fb7b6b661df898aeca90af)
        - key &quot;click&quot; was: _P(theta | x ) = P(x | theta) \* P(theta)_
        - because: left side is what we want to know
        - first factor is likelihood function: easily calculated using _observed_ data
        - second factor is total probability of parameters, given our prior parameter distributions.
- **2021-06-16**
    - Playing around quite a bit with PyMC3
        - [https://docs.pymc.io/pymc-examples/examples/getting\_started.html](https://docs.pymc.io/pymc-examples/examples/getting_started.html)
    
        - [https://docs.pymc.io/pymc-examples/examples/generalized\_linear\_models/GLM-linear.html](https://docs.pymc.io/pymc-examples/examples/generalized_linear_models/GLM-linear.html)
    - Re-read Antonio+Malte&#39;s paper, it makes more sense now (with some knowledge about the fundamental principles)
    - Getting familiar with [PyMC3](https://docs.pymc.io/about.html)
        - [https://docs.pymc.io/Probability\_Distributions.html](https://docs.pymc.io/Probability_Distributions.html)
        - Gaussian processes? (appeared in Antonio+Malte&#39;s paper), [http://www.gaussianprocess.org/gpml/chapters/RW.pdf](http://www.gaussianprocess.org/gpml/chapters/RW.pdf)
        - [https://docs.pymc.io/PyMC3\_and\_Theano.html](https://docs.pymc.io/PyMC3_and_Theano.html)
        - [https://docs.pymc.io/pymc-examples/examples/pymc3\_howto/api\_quickstart.html](https://docs.pymc.io/pymc-examples/examples/pymc3_howto/api_quickstart.html)
    - Lukas&#39; thesis: thanks for the annotations!

- **2021-06-15**
    - [https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings\_algorithm](https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm)
        - core of MCMC methods? (mentioned in first meeting)
        - created [short demo implementation](https://gist.github.com/dermesser/c7bb020179c305a4633e1ec1a7a4bdea) (n-dim) in Julia
        - ![](RackMultipart20210630-4-gmvlvg_html_d4757ef3730c34cc.png)
    - [&quot;Writing a Cyclus Input File&quot;](https://fuelcycle.org/user/writing_input.html)
        - and its section types
        - need deeper examples
    - [&quot;A Zero-Math Introduction to Markov Chain Monte Carlo Methods&quot;](https://towardsdatascience.com/a-zero-math-introduction-to-markov-chain-monte-carlo-methods-dcba889e0c50)
    - [&quot;Probability concepts explained: Bayesian inference for parameter estimation](https://towardsdatascience.com/probability-concepts-explained-bayesian-inference-for-parameter-estimation-90e8930e5348).&quot;
    - &quot;Nuclear archaeology: reconstructing reactor histories from reprocessing waste&quot;
        - use Bayes&#39; theorem ![](RackMultipart20210630-4-gmvlvg_html_121db20fba883abf.png) to estimate probability of a parameter set x given knowledge about waste composition y (and &quot;out of band&quot; knowledge about possible parameters): simulate output for a large set of parameters, obtain probability distribution for parameters
        - Numerical evaluation using probabilistic Markov chains
        - simulation of output is expensive; a &quot;surrogate model&quot; was used for accelerating the simulation, using Gaussian Process Regression/GPR
        - Proof of concept: simulation of Savannah River Site K&#39;s inner core
        - Test surrogate model vs. actual simulation; good results achieved
        - Investigation of three simple scenarios, each time calculating the prior x from the posterior y, comparing inferred results to simulation
        - Inference works reasonably well, although scenario is simplistic
- **2021-06-14**
    - Cyclus - Fundamental Concepts in Cyclus
    - Agent-based modelling system: facilities etc. are agents, processing discrete, identifiable batches of nuclear material
      - &quot;Nuclear Fuel Cycle&quot; (NFC) (Max&#39; thesis)
        - Fuel Cycle Overview
          - Uranium mining: open-put/underground/leaching
          - Conversion into yellowcake (UOC)
          - Purification of UOC and production of fuel _or_ conversion into UF6 (UF6 most common). UF6 is volatile, allows for enrichment in gas phase
          - Enrichment: reactors require low-enriched uranium (LEU), in which U-235 appears with higher concentration. Can use gas diffusion or gas centrifuges; today, centrifuges are the way to go
          - LEU is enriched to 3-5% from 0.72% in natural U. Research reactors and other  
            applications need 20-90% enriched U.
          - Fuel fabrication by reconversion into UO or metallic U, shaping into fuel rods/
            pellets (depending on reactor type).
          - Nuclear reactors maintain nuclear fission reactions, generating energy. U-235
             concentration decreases, Pu etc. are enriched. Reaction ceases when U-235
             concentration too low.
          - Waste material is either stored or reprocessed (once-through vs. closed fuel cycle)
          - Recycling splits waste into U-235/Pu-239/decay products
        - Military Fuel Cycle
          - Focus doesn&#39;t lie on energy production, but Pu/U production for weapon building purposes
          - Two main aspects: enrichment plant (U-235 enrichment above 90%); reprocessing plant (recovery of Pu from waste material)
          - Production of fissile material is main difficulty for producing nuclear weapons
          - NPT disallows NNWS from acquiring such material, while peaceful (energy) use is allowed, and so is constructing civil-use NFCs
          - IAEA inspects NNWS&#39; inventories
        - Simulation of Nuclear Fuel Cycles
          - Foundation: flows of material between facilities.
          - Consider operation of facilities, derive material composition
          - Metrics and cycle characteristics can then be determined. Such as:
            - Natural U consumption
            - Energy production
            - Amount of generated waste material
          - Challenge arises if number of fuel cycle actors increases
          - Good fuel cycle simulators track single batches, decay, costs, and flexible facility setups etc.
      - &quot;**Kick-off&quot;**
        - Arbeit anmelden (Torsten.Loevenich@zhv.rwth-aachen.de)
        - &quot;Reconstructing Nuclear Fuel Cycles Using Bayesian Inference: a first implementation&quot;
        - Literatur lesen &amp; verstehen
        - Gruppen-Meeting mittwochs
    
