# Singularity

I've converted the `cyclus/cyclus` docker container into a singularity sandbox
image, which I store in sciebo (`cyclus0_sandbox.tar.bz2`). It contains in its
`/root` directory a working from-source compiled version of cyclus, including
python library etc.

The conversion from a docker image on my machine is easy:

```bash
docker save <container> -o cyclus.tar
singularity build --sandbox docker-archive://cyclus.tar
```

From experimentation, I found that the following *incantation* makes python work
well enough: `--writable` is required by Theano wanting to store/update compiled
kernels. Setting `HOME` and sourcing the `.bashrc` is needed for python
respectively Anaconda (`conda`) working properly.

```bash
# Alternatively, this works:

singularity exec --writable cyclus0/ /bin/bash -c 'export HOME=/root  && cd /root && source .bashrc && cd app/simple0 && python -m unittest blackbox.test_blackbox.LogLikelihoodTest'
```

# RWTH cluster

There is a pre-built Cyclus image which can be used like this:

```bash
singularity shell /rwthfs/rz/SW/UTIL.common/singularity/cyclus
```

This will drop you into a shell within the container. From there, everything using Cyclus should
work once you have installed it, e.g. by compiling from source. That is, this image has the right
environment for Cyclus to compile (Ubuntu 20.04), but not Cyclus itself. Typically, cyclus will be
installed into `~/.local`. 

The contents of my `.local` directory can be [downloaded](https://my.hidrive.com/lnk/BbsAtgvP).
There may be slightly more content than necessary, but not by a lot. It should definitely be enough
to run the code in this repository from within a singularity container.
