# `singularity`

Running cyclus is difficult; in order to do it on the RWTH cluster, we need to
run it inside `singularity` (https://sylabs.io/guides/3.8/user-guide/). Here I'm
storing assets and notes related to this.
