# `bayesian-fuelcycle`

This repository contains code for using the Cyclus simulation software (https://fuelcycle.org)
inside a likelihood function used by a Markov Chain Monte Carlo (MCMC) inference process driven by
PyMC3.

This sounds complicated.

However, it is somewhat easy. For technical details, please refer to my bachelor's thesis at
`thesis/thesis.pdf`, especially to its third and fourth sections.

The directories here are organized as follows:

* `cluster/`: scripts for running different inference scenarios on the RWTH SLURM cluster.
* `cyclus/`: Disregard; only used for testing the Cyclus integration.
* `docs/`: Presentations I've held about this topic.
* `log/`: A log of all inference runs as well as my work log.
* `notebooks/`: Supporting code in Jupyter notebooks.
* `result_data/`: CDF files containing PyMC3 samples.
* `sampling/`: The core code for running inferences using Cyclus.
 * `blackbox/`: Core code for blackbox likelihood function integration with PyMC3.
 * `cyclus_db/`: Extraction of results from Cyclus' SQLite simulation databases.
 * `models/`: Specific Cyclus models
 * `visualize/`: Evaluation of collected samples.
* `singularity/`: Notes on running Cyclus inside a Singularity container.
* `thesis/`: My thesis.
