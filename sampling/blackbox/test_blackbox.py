import copy
import unittest
import numpy as np
import pymc3 as pm
import arviz as az
import theano.tensor as tt

import blackbox.blackbox as bb
import blackbox.likelihood as bl


class LogLikelihoodTest(unittest.TestCase):
    def test_normal_likelihood(self):
        mus = [0, 1, 0]
        sigmas = [1, 2, 1 / 2]
        nl = bl.NormalLikelihood(mus, sigmas)

        samples1 = mus
        samples2 = [1 / 2, 1 / 3, -1 / 3]

        # Possibly brittle
        self.assertEqual(nl.log_likelihood(samples1), -2.756815599614018,
                         'incorrect likelihood')
        self.assertEqual(nl(samples2), -3.1595933773917957,
                         'incorrect likelihood')

    def test_uniform_likelihood(self):
        intervals = [(0, 1), (0, 10), (5, 10)]
        nl = bl.UniformLikelihood(intervals)

        samples1 = [.4, 1, 6]
        self.assertEqual(nl(samples1), -3.9120230054281464,
                         "incorrect likelihood")


class TestCyclusModel(bb.CyclusModel):
    def __init__(self, *args, **kwargs):
        super(TestCyclusModel, self).__init__(*args, **kwargs)

    def result(self):
        return np.array([42, 1337])

    def mutate(self, params):
        """params is [power_cap_lightwater, separations_throughput]."""
        print("Mutate called with parameters:", params)
        self.mut_model = copy.deepcopy(self.model)
        # TODO: check if key exists; verify model identity
        self.mut_model['simulation']['facility'][3]['config']['Reactor'][
            'power_cap'] = params[0]
        self.mut_model['simulation']['facility'][4]['config']['Separations'][
            'throughput'] = params[1]


def example_cll_creation():
    cycmod = TestCyclusModel("simple.json")
    cycmod.run_groundtruth()
    result0 = cycmod.result()

    sigmas = np.ones_like(result0)
    likelihood = bl.NormalLikelihood(result0, sigmas)

    return bl.CyclusLogLikelihood(likelihood, cycmod)


class BlackBoxSimulateTest(unittest.TestCase):
    def test_hello(self):
        print("Hello World")

    def test_simulate_simple_json(self):
        sim = bb.CyclusModel("simple.json")
        self.assertTrue(sim.simulate() is not None, "simulation returned None")

    def test_theano_op(self):
        loglik = example_cll_creation()

        with pm.Model() as pmod:
            powercap = pm.Uniform("powercap", 900, 1100)
            throughput = pm.Uniform("throughput", 79e3, 81e3)

            pm.DensityDist(
                "likelihood",
                lambda v: loglik(v),
                observed={'v': tt.as_tensor_variable([powercap, throughput])})

            step = pm.Metropolis()

            return  # takes too long
            trace = None
            for i in range(0, 4):
                print("Trace attempt", i)
                trace = pm.sample(100,
                                  tune=10,
                                  chains=1,
                                  trace=trace,
                                  step=step)
                pm.backends.ndarray.save_trace(trace, ".")
            print(az.summary(trace))


class TestGradient(unittest.TestCase):
    def test_grad_simple(self):
        f = lambda x: x[0]**2

        grd = bl.simplegrad(f, [1.])
        self.assertEqual(np.round(grd, decimals=5), np.array([2.0]))

    def test_grad_multi(self):
        f = lambda x: 3 * x[0]**3 + 2 * x[0] * x[1] + 4 * x[1] * x[2] + 2 * x[
            2]**2
        grd = bl.simplegrad(f, [1., 2., 3.])
        self.assertTrue(
            np.equal(np.round(grd, decimals=3), np.array([13., 14.,
                                                          20.])).all())
