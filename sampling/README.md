# `sampling`

This tree contains the Python code linking PyMC with Cyclus, enabling the
sampling that is core to the thesis.

* `blackbox` contains code for the blackbox bayesian inference using cyclus.
* `distributed_demo` contains a simple PyMC3 model to be sampled in parallel
from many nodes, with the goal of merging resulting chains for joint results.
Another goal is to ensure reproducibility across multiple runs.
    * It also contains `merge.py`, which is the current way of merging samples from different machines.
* `attempt_sampling` contains a proof-of-concept script that can run on the cluster. It samples a
toy model in parallel and produces chains ready for merging.
* `util` contains e.g. logging code.
* `cyclus_db` is code to work with Cyclus' simulation outputs.
* `models` has actual models.
    * each model folder has a `run.py` script which samples the model. It is supposed to be
    configured in a way that job output (logs) and data end up in $WORK automatically, in
    appropriately named files.
    * it is accompanied by the Cyclus model as a python file, which contains a function
    `simulation()` returning a JSON dictionary (the simulation object).
    * the model is supposed to be run from the `sampling` subdirectory: `PYTHONPATH=. python3
    models/xyz/run.py`.

## Pitfalls etc.

### Models

* `miso_enrichment` appears to calculate negative compositions if the cycle time of a reactor is not
quite long enough. If it is a problem, attempt increasing cycle time.

### Regarding merging samples.

* `merge_samples` is arcane/unmaintained/mysterious and doesn't work. 
  * `arviz.concat` is the desired function. It allows concatenation along draws
  and chains.
  * But, it only works with some fixes. Namely, PRs 1590 (from arviz) and 4495
  (from pymc3) need to be applied. They are not contained in the stable releases
  0.11 (arviz)/3.11 (pymc3)
  * The cherry-picked working versions are available in branch `lewin` at
    * https://git.rwth-aachen.de/lewin/arviz
    * https://git.rwth-aachen.de/lewin/pymc3
  * This may have changed by now.
* TODO: Figure out (de)serialization of traces. Arviz should be able to do that
easily, using
[to\_netcdf](https://arviz-devs.github.io/arviz/api/generated/arviz.InferenceData.to_netcdf.html) and friends.
Using these, it should be easy to save `InferenceData` objects from different
nodes to the filesystem, and stitch them together at the end. The orchestration
of this is obviouslt a different matter.
