"""Extract various simulation results from a Cyclus SQLite simulation output
database."""

import sqlite3


def run_with_conn(filename, extract, params={}):
    with sqlite3.connect(filename) as sql:
        return extract(sql, **params)


def get_all_agents(sqlite):
    query = """
SELECT AgentId, Kind, Spec, Prototype
FROM AgentEntry
ORDER BY AgentId ASC"""
    cursor = sqlite.execute(query)

    out = []
    for row in cursor:
        out.append('{} {} spec {} proto {}'.format(*row))
    return '\n'.join(out)


def multi_agent_concs(last_sqlite_file, sinks):
    filename = last_sqlite_file
    all_concentrations = {}
    masses = {}
    for sink in sinks:
        concentrations, mass = run_with_conn(filename,
                                             extract_isotope_concentrations,
                                             {'agent_name': sink})
        all_concentrations[sink] = concentrations
        masses[sink] = mass
    return all_concentrations, masses


def extract_isotope_concentrations(sqlite,
                                   agent_name='DepletedUraniumSink',
                                   inventory_name='inventory'):
    """Select the most recent isotope distribution (normed to 1) for a given
    agent and inventory.

    Returns (isotopes, total_mass) tuple, where isotopes is dict of int
    (isotope id) to fraction, and fractions sum up to 1.
    """

    agentidquery = """
SELECT AgentId
FROM AgentEntry
WHERE Prototype = :agentname"""
    cursor = sqlite.execute(agentidquery, {'agentname': agent_name})
    agentid = -1
    for row in cursor:
        agentid = int(row[0])

    isoquery = """
SELECT NucId, Quantity
FROM ExplicitInventory
WHERE AgentId = :agentid AND InventoryName = :invname
AND Time = (SELECT MAX(Time)
            FROM ExplicitInventory
            WHERE AgentId = :agentid);
"""
    cursor = sqlite.execute(isoquery, {
        'agentid': agentid,
        'invname': inventory_name
    })

    isotopes = {}
    for row in cursor:
        isotopes[row[0]] = row[1]

    total_mass = sum(v for (k, v) in isotopes.items())
    for (k, v) in isotopes.items():
        isotopes[k] = v / total_mass
    return isotopes, total_mass
