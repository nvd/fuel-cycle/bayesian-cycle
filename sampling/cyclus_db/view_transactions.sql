select T.Time, S.Prototype Sender, R.Prototype Receiver, T.Commodity from Transactions T
join AgentEntry S on (S.AgentId = T.SenderId)
join AgentEntry R on (R.AgentId = T.ReceiverId);
