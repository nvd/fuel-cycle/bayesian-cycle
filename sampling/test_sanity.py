import json
from cyclus import simstate


def main():
    filename = "simple.json"
    model = json.load(open(filename, 'r'))

    js = json.dumps(model)
    simst = simstate.SimState(debug=True, input_file=js, input_format="json")
    print("before load")
    simst.load()
    print("before run")
    simst.run()


if __name__ == "__main__":
    main()
