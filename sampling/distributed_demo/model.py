import argparse
from os import path

import numpy as np
import pymc3 as pm

RANDOM_SEED = 12345


def model():
    '''This is the 'card model', binary distribution checking frequencies of
    football player cards.'''

    fair_probs = [1 / 25, 1 / 25, 1 / 25, 1 / 25, 21 / 25]
    rigged_probs = [.3 / 25, 1.7 / 25, .4 / 25, 1.6 / 25, 21 / 25]

    fair_observation = pm.Categorical.dist(p=fair_probs).random(size=100)
    rigged_observation = pm.Categorical.dist(p=rigged_probs).random(size=100)

    with pm.Model() as mod:
        probs_sigma = np.ones(len(fair_probs)) * .1

        # Sample 5 card probabilities
        sampled_probs = pm.TruncatedNormal('sampled_probs',
                                           mu=fair_probs,
                                           sigma=probs_sigma,
                                           lower=0,
                                           upper=1,
                                           shape=(5, ))

        pm.Categorical('observation',
                       p=sampled_probs,
                       observed=rigged_observation)

    return mod


def sample(mod, seed_off=0, n=10000, chains=1):
    with mod:
        sample_result = pm.sample(n,
                                  chains=chains,
                                  return_inferencedata=True,
                                  random_seed=RANDOM_SEED + seed_off)
        return sample_result


def save_samples(filename, samples):
    samples.to_netcdf(filename)


def run(args):
    mod = model()
    samples = sample(mod,
                     seed_off=args.index,
                     n=args.samples,
                     chains=args.chains)
    save_samples(
        path.join(args.output_path,
                  'samples_{}_{}.cdf'.format(args.run, args.index)), samples)


def main():
    p = argparse.ArgumentParser(description='Cluster sampling test')
    p.add_argument('--index', type=int, default=0, help='Instance index')
    p.add_argument('--run', type=str, help='Name for this run.')
    p.add_argument('--output-path',
                   type=str,
                   default='/home/vf962887/data/',
                   help='Output path for sample CDF files.')
    p.add_argument('--samples',
                   type=int,
                   default=10000,
                   help='Number of samples per chain')
    p.add_argument('--chains', type=int, default=1, help='Number of chains.')
    args = p.parse_args()

    run(args)


if __name__ == '__main__':
    main()
