using Plots
using Distributions

function read_enrichment_u236_data(files=ARGS)
    re_enrich = r"Mutating: using parameters {'high_enrichment_grade': ([0-9\.]+)}"
    re236 = r"Likelihood for isotopic concentration \{92236: ([0-9\.e-]+)\} is ([0-9\.e-]+)"

    current_enrichment = 0
    current_236_conc = 0

    u236_of_enrichment = Dict()

    @time for f in files
        for line in eachline(f)
            m = match(re_enrich, line)
            if m !== nothing
                current_enrichment = parse(Float64, m[1])
                continue
            end
            m = match(re236, line)
            if m !== nothing
                current_236_conc = parse(Float64, m[1])
                current_lik = parse(Float64, m[2])
                u236_of_enrichment[current_enrichment] = (current_236_conc, current_lik)
            end
        end
    end

    xs, ys, ls = zeros(length(u236_of_enrichment)), zeros(length(u236_of_enrichment)), zeros(length(u236_of_enrichment))
    for (i, k) = enumerate(keys(u236_of_enrichment))
        xs[i] = k
        ys[i] = u236_of_enrichment[k][1]
        ls[i] = u236_of_enrichment[k][2]
    end
    (xs, ys, ls)
end

function read_likelihood_data(files=ARGS)
    # Looking for lines like
    # 2021-07-12 15:29:07.834316 :: Likelihood for isotopic concentration {92236: 8.766286323817704e-05} is 8.501321586268258
    re = r"Likelihood for isotopic concentration \{92236: ([0-9\.e-]+)\} is ([0-9\.e-]+)"
    values = Dict()
    @time for f in files
        for line in eachline(f)
            m = match(re, line)
            if m !== nothing
                values[parse(Float64, m[1])] = parse(Float64, m[2])
            end
        end
    end

    xs, ys = zeros(length(values)), zeros(length(values))
    for (i, k) = enumerate(keys(values))
        xs[i] = k
        ys[i] = values[k]
    end

    println("extracted $(length(xs)) values")
    (xs, exp.(ys))
end

function plot_likelihood(f=ARGS)
    (xs, ys) = read_likelihood_data(f)
    likplot = scatter(xs, log.(ys), ylabel="Likelihood", xlabel="U236 concentration", xlims=(9e-5, 9.5e-5), ylims=(8.2913, 8.2915))
    savefig(likplot, "likelihoods.png")
    likplot
end

function plot_ideal_distribution(mu=9.261522888181698e-05, sigma=0.05/100)
    xs = LinRange(9e-5, 9.5e-5, 100)
    f = TruncatedNormal(mu, sigma, 0, 1)
    plot(xs, x -> (pdf(f, x)))
end

function plot_enrichment(f=ARGS)
    (xs, ys, ls) = read_enrichment_u236_data(f)
    enrichplot = scatter(xs, ys, xlabel="Enrichment U235 / %", ylabel="U236 conc")
    savefig(enrichplot, "enrichment.png")
    enrichlikeli = scatter(xs, ls, xlabel="Enrichment U235 / %", ylabel="Likelihood", ylims=(8.29135, 8.29142))
    savefig(enrichlikeli, "enrichment_likelihood.png")
    enrichplot
end

plot_likelihood()
plot_enrichment()
