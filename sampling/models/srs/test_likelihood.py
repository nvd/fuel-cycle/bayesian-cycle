from models.srs import run

import numpy as np

truth = run.SimulationOutput(depleted_u_composition={922360000: 9.261522e-5},
                             depleted_u_amount=1,
                             abundance=0)

isolike = run.IsotopeLikelihood(truth, only_isos=[92236])

concentrations = np.linspace(1e-5, 10e-5, 100)
likelihoods = np.zeros_like(concentrations)

for (i, conc) in enumerate(concentrations):
    likelihoods[i] = isolike(
        run.SimulationOutput(depleted_u_composition={922360000: conc},
                             depleted_u_amount=1,
                             abundance=0))

data = np.array([concentrations, likelihoods]).T
np.savetxt("likelihoods.csv", data, delimiter=",")
