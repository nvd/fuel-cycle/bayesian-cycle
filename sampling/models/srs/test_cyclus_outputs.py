from models.srs.run import SRSCyclusModel
from models.srs import run
from cyclus_db import extract

from collections import namedtuple
import csv
import numpy as np


def cartesian_product(dimensions, acc=[]):
    if len(dimensions) == 0:
        yield acc
    else:
        dim = dimensions[0]
        for e in dim:
            yield from cartesian_product(dimensions[1:], acc=acc + [e])


def run_simulation(ccm, parameter_dimensions, conc_func=None):
    parameter_combinations = cartesian_product(parameter_dimensions)

    for (i, c) in enumerate(parameter_combinations):
        print('Running simulation with parameters', c)
        ccm.mutate(c)
        ccm.simulate()
        conc = conc_func(ccm.last_sqlite_file)
        yield c, conc


def pu_concs(last_sqlite_file):
    return sink_concs(last_sqlite_file, sinks=['PlutoniumSink'])


def u_concs(last_sqlite_file):
    return sink_concs(last_sqlite_file, sinks=['DepletedUraniumSink'])


def all_concs(last_sqlite_file):
    return sink_concs(
        last_sqlite_file,
        sinks=['DepletedUraniumSink', 'PlutoniumSink', 'SeparatedWasteSink', 'SR_Reactor'])


def sink_concs(last_sqlite_file, sinks):
    filename = last_sqlite_file
    all_concentrations = {}
    for sink in sinks:
        concentrations, total_mass = extract.run_with_conn(
            filename, extract.extract_isotope_concentrations,
            {'agent_name': sink})
        all_concentrations.update(
            {f'{sink}_{k}': v
             for (k, v) in concentrations.items()})
        all_concentrations.update(
                {f'total_mass_{sink}': total_mass})
    return all_concentrations


def run_generic_simulations(parameters,
                            samples_per_dim=20,
                            outfile='concentrations.csv'):
    assert all(len(p) == 3 for p in parameters)
    params = [np.linspace(p[1], p[2], samples_per_dim) for p in parameters]
    ccm = SRSCyclusModel(
        true_parameters={p[0]: (p[1] + p[2]) / 2
                         for p in parameters},
        sampled_parameters={p[0]: list(p[1:2])
                            for p in parameters})
    truth = ccm.run_groundtruth()
    print('Ground truth:', truth, 'for parameters', parameters)

    with open(outfile, 'w') as f:
        cw = csv.writer(f)
        param_heads = [p[0] for p in parameters]
        baserow = None

        for (ps, row) in run_simulation(ccm, params, conc_func=all_concs):
            if baserow is None:
                baserow = row
                colheads = list(sorted(baserow.keys()))
                cw.writerow(param_heads + colheads)
            cw.writerow(ps + [row.get(p, '') for p in sorted(baserow.keys())])
            f.flush()
    return


def cycletime_simulation():
    run_generic_simulations([('cycle_time', 20, 120),
                             ('high_enrichment_grade', 5, 94.5),
                             ('slight_enrichment_grade', 1.0, 2.0)],
                            samples_per_dim=25,
                            outfile='concentrations_cycletime_heg_seg.csv')

if __name__ == '__main__':
    cycletime_simulation()
