import util.log as log

import argparse
import json
import math
import os

from collections import namedtuple

import arviz as az
import numpy as np
import pymc3 as pm
import theano.tensor as tt

import blackbox.blackbox as bb
import blackbox.likelihood as lik

from models.srs import cyclus_input
from cyclus_db import extract

RANDOM_SEED = 12345

SimulationOutput = namedtuple('SimulationOutput',
                              ('composition', 'mass', 'parameters'))
Abundance = namedtuple('Abundance', ('iso1', 'iso2', 'factor'))


def samples_output_path(i, name='', dir='data', default=None):
    task_id = log.task_identifier()
    if default is None or default == '':
        basepath = os.environ.get('HOME', None)
        default = os.path.join(basepath, dir)
    os.makedirs(default, exist_ok=True)
    return os.path.join(
        default, 'cyclus_trace_{}_{}_{:03d}.cdf'.format(name, task_id, i))


class ElboniaCyclusModel(bb.CyclusCliModel):
    def __init__(self,
                 true_parameters,
                 sampled_parameters,
                 sinks=['DepletedUraniumSink']):
        self.true_parameters = true_parameters
        self.sampled_parameters = sampled_parameters
        self.current_parameters = None
        self.sinks = sinks
        super(ElboniaCyclusModel, self).__init__()

    def mutate(self, sample=None):
        """Mutate the model.

        `sample` is a list or vector of parameters, corresponding to the
        parameters in `sampled_parameters`. The order must be identical to the
        alphabetically sorted order of keys in `sampled_parameters`.
        """
        if sample is None:
            self.mut_model = cyclus_input.simulation(
                parameters=self.true_parameters)
            return
        # Alphabetical order, see model. Only copy keys, as sampled_parameters
        # contain the sampling range
        parameters = {k: None for k in self.sampled_parameters}
        # Unpack alphabetically sorted model parameters.
        for i, k in enumerate(sorted(self.sampled_parameters.keys())):
            parameters[k] = sample[i]
        log.log_print('Mutating: using parameters', parameters)
        self.mut_model = cyclus_input.simulation(parameters=parameters)
        self.current_parameters = parameters

    def result(self):
        filename = self.last_sqlite_file
        concentrations, total_mass = extract.multi_agent_concs(
            filename, self.sinks)
        return SimulationOutput(composition=concentrations,
                                mass=total_mass,
                                parameters=self.current_parameters)


class IsotopeLikelihood(lik.LikelihoodFunction):
    def __init__(self,
                 truth: SimulationOutput,
                 only_isos=[],
                 use_mass=True,
                 abundances=None,
                 rel_sigma=0.5,
                 logdest=None):
        self.truth = truth
        self.only_isos = only_isos
        self.use_mass = use_mass
        self.abundances = abundances
        self.rel_sigma = rel_sigma
        if logdest is not None:
            log.log_print('Logging samples to', logdest)
            self.structured_log = open(logdest, 'w')
        else:
            self.structured_log = None

    def log_structured_sample(self, simout: SimulationOutput,
                              likelihood: float):
        if self.structured_log is not None:
            json.dump(
                {
                    'likelihood': likelihood,
                    'concentrations': simout.composition,
                    'parameters': simout.parameters
                }, self.structured_log)
            print('', file=self.structured_log)
            self.structured_log.flush()

    def log_likelihood(self, output: SimulationOutput):
        compo_lik = 0
        abundance_lik = 0
        # Relative sigmas
        compo_sigma = lambda true_frac: true_frac * self.rel_sigma / 100

        # Account for mass.
        mass_lik = 0
        if self.use_mass:
            for (sink, mass) in output.mass.items():
                if sink not in self.truth.mass:
                    continue
                mass_normalized = (mass - self.truth.mass[sink]) / compo_sigma(
                    self.truth.mass[sink])
                mass_lik += pm.Normal.dist(mu=mass_normalized,
                                           sigma=1).logp(0).eval()

        if self.abundances:
            # Rough estimation
            abundance_sigma = self.rel_sigma / 100
            for sink, truesinkcomp in self.truth.composition.items():
                for absp in self.abundances:
                    iso1, iso2 = int(absp.iso1 * 1e4), int(absp.iso2 * 1e4)
                    if not (iso1 in truesinkcomp and iso2 in truesinkcomp):
                        # One of the two isotopes is not in the sink's true composition. Try next.
                        continue
                    c1, c2 = output.composition[sink].get(
                        iso1, 0), output.composition[sink].get(iso2, None)
                    if c1 == 0:
                        log.log_print(
                            f'Zero concentration found for {absp.iso1} in agent {sink} of simulation output! This may be fine. Check for typos'
                        )
                        abundance_lik -= np.inf
                        break
                    if c2 is None:
                        log.log_print(
                            f'No concentration found for {absp.iso2} in agent {sink} of simulation output!'
                        )
                        abundance_lik -= np.inf
                        break
                    t1, t2 = truesinkcomp.get(iso1,
                                              0), truesinkcomp.get(iso2, None)
                    if t1 == 0:
                        log.log_print(
                            f'Zero concentration found for {absp.iso1} in agent {sink} of ground truth! This may be fine. Check for typos'
                        )
                        abundance_lik -= np.inf
                        break
                    assert t2 is not None

                    observed_abundance = c1 / c2
                    true_abundance = t1 / t2
                    normalized = (observed_abundance - true_abundance) / (
                        abundance_sigma * true_abundance)
                    lik = pm.Normal.dist(mu=normalized, sigma=1).logp(0).eval()
                    abundance_lik += absp.factor * lik

                    log.log_print(
                            f'{sink}: Likelihood for abundance {absp} = {observed_abundance} (target: {true_abundance}) is {lik:.4e}'
                    )
            log.log_print(f'Assigning likelihood {abundance_lik}')
        else:
            used_concs = {}
            for (sink, composition) in self.truth.composition.items():
                used_concs[sink] = {}
                sink_lik = 0
                for (iso, conc) in composition.items():
                    iso = int(iso * 1e-4)  # Scale down to our notation
                    if (not self.only_isos) or (iso in self.only_isos):
                        measured_conc = output.composition[sink].get(
                            int(iso * 1e4), None)
                        if measured_conc is None:
                            compo_lik -= np.inf
                            # Skip this sink
                            break
                        # Use standard normal distribution to allow for addition of likelihoods for multiple isotopes
                        x = (measured_conc - conc) / compo_sigma(conc)
                        iso_lik = pm.Normal.dist(mu=x, sigma=1).logp(0).eval()
                        compo_lik += iso_lik
                        sink_lik += iso_lik
                        used_concs[sink][iso] = output.composition[sink].get(
                            int(iso * 1e4), 0)
                log.log_print(f'Likelihood for sink {sink}: {sink_lik}')
            # Sometimes, no isotope composition is returned (if the simulation
            # failed); count this as "may never happen", i.e. probability == 0
            if len(output.composition) == 0:
                compo_lik = -math.inf
            self.log_structured_sample(output, compo_lik)
            log.log_print(
                f'Likelihood for isotopic concentration {used_concs} is {compo_lik}'
            )
        return mass_lik + ((abundance_lik + compo_lik) if
                           (compo_lik + abundance_lik) != 0 else -math.inf)


def generate_start_value(sample_parameters, n=1):
    """Generate random start values. Ensure that the random generator has been deterministically seeded!"""
    def genstart():
        d = {}
        for (k, v) in sample_parameters.items():
            if type(v) is list:
                d[k] = np.random.random() * (v[1] - v[0]) + v[0]
            elif type(v) is dict:
                if v['type'] == 'Normal':
                    d[k] = (np.random.randn()*v['sigma']) + v['mu']
                elif v['type'] == 'TruncatedNormal':
                    d[k] = pm.TruncatedNormal.dist(**{p: pv for (p, pv) in v.items() if p != 'type'}).random()
                else:
                    assert False, f'Unknown parameter type {k} => {v}'
            else:
                assert False, f'Unknown parameter type {k} => {v}'
        return d

    if n == 1:
        return genstart()
    return [genstart() for i in range(0, n)]

def jitter_groundtruth(gt, jittersigma, seed):
    # e.g.:
    # SimulationOutput(composition={'DepletedUraniumSink': {922340000: 2.735915578767279e-09, 922350000: 0.00172067552309655, 922360000: 0.00011803315614052132, 922380000: 0.9981607459001454, 922390000: 5.42519298226051e-07, 922400000: 1.6540382580414916e-10}}, mass={'DepletedUraniumSink': 106434.34946185134}, parameters=None)
    newcompo = {}
    newmass = {}
    np.random.seed(seed)
    # Ensure determinism by sorting dict entries
    for (sink, content) in sorted(gt.composition.items(), key=lambda x: x[0]):
        newcompo[sink] = {}
        for (iso, conc) in sorted(content.items(), key=lambda x: x[0]):
            newcompo[sink][iso] = (np.random.randn() * jittersigma * conc) + conc
    for (sink, mass) in sorted(gt.mass.items(), key=lambda x: x[0]):
        newmass[sink] = (np.random.randn() * jittersigma * mass) + mass
    return SimulationOutput(composition=newcompo, mass=newmass, parameters=gt.parameters)


def sampling_parameter_to_rv(name, value):
    if type(value) is list:
        return pm.Uniform(name, lower=value[0], upper=value[1])
    if type(value) is dict:
        if value['type'] == 'Normal':
            return pm.Normal(name, mu=value['mu'], sigma=value['sigma'])
        elif value['type'] == 'TruncatedNormal':
            return pm.TruncatedNormal(name, mu=value['mu'], sigma=value['sigma'], upper=value['upper'], lower=value['lower'])
    assert False, f'invalid RV in parameters: {name} => {value}'

def model(args, jitterseed=1):
    # Sample parameters are read from file and determine which variables will be sampled.
    with open(args.sample_parameters_file, 'r') as spf:
        sample_parameters = json.load(spf)
    with open(args.true_parameters_file, 'r') as spf:
        true_parameters = json.load(spf)

    cyclus_model = ElboniaCyclusModel(true_parameters,
                                    sample_parameters,
                                    sinks=args.sinks)
    groundtruth = cyclus_model.run_groundtruth()

    log.log_print(f'Ground truth parameters are: {groundtruth}')

    if args.add_groundtruth_jitter:
        groundtruth = jitter_groundtruth(groundtruth, args.jitter_sigma, jitterseed)
        log.log_print(f'JITTERED ground truth parameters are: {groundtruth}')

    if args.structured_log:
        structured_log_out = log.log_file_path(run=args.run,
                                               outpath=args.log_path,
                                               typ='samples',
                                               ending='json')
    else:
        structured_log_out = None
    loglikelihood_op = lik.CyclusLogLikelihood(IsotopeLikelihood(
        groundtruth,
        only_isos=args.only_isos,
        use_mass=args.use_mass,
        rel_sigma=args.rel_sigma,
        logdest=structured_log_out,
        abundances=args.abundances),
                                               cyclus_model,
                                               memoize=True,
                                               maxiter=args.grad_max_iter)

    log.log_print('Building PyMC3 model.')
    log.log_print('Sampling variables as follows:',
                  [f'{k} => {v}' for (k, v) in sample_parameters.items()])
    log.log_print('The true parameters are:',
                  [f'{k} => {v}' for (k, v) in true_parameters.items()])

    with pm.Model() as experiment_model:
        model_vars = {
            k: sampling_parameter_to_rv(k, v) 
            for (k, v) in sample_parameters.items()
        }
        log.log_print('Model variables:', model_vars)

        if False:
            # This has started causing problems, although it is the original solution.

            # Give sampled variables in alphabetical order to the likelihood operator.
            pm.DensityDist('observed',
                           lambda v: loglikelihood_op(v),
                           observed={
                               'v':
                               tt.as_tensor_variable([
                                   model_vars[k]
                                   for k in sorted(model_vars.keys())
                               ])
                           })
        else:
            pm.Potential(
                'observed',
                loglikelihood_op(
                    tt.as_tensor_variable(
                        [model_vars[k] for k in sorted(model_vars.keys())])))

    start = generate_start_value(sample_parameters, n=args.chains)
    return experiment_model, start


def save_trace(args, trace, i=0):
    output_path = samples_output_path(i, args.run, default=args.output_path)
    log.log_print(f'Saving trace #{i} ({trace}) to file {output_path}')
    log.log_print(trace)
    log.log_print(az.summary(az.from_pymc3(trace)))
    location = az.from_pymc3(trace,
                             density_dist_obs=False).to_netcdf(output_path)
    log.log_print(f'Successfully saved trace #{i} to {location}')


def sample(args, pm_model, start={}, seeds=[]):
    algo = None
    with pm_model:
        algo = pm.Slice()
        if args.algorithm == 'metropolis':
            algo = pm.Metropolis()
        elif args.algorithm == 'nuts':
            # TODO: tuning, like target_accept, step_scale, ...
            algo = pm.NUTS()

    # If chunk sampling is desired:
    if args.iter_sample <= 0:
        log.log_print('Start of sampling')

        if args.algorithm in ('default', 'metropolis', 'nuts'):
            with pm_model:
                trace = None
                for i in range(0, args.iterations):
                    log.log_print(
                        f'sampling iteration {i} at {args.samples} samples per iteration, initial parameters {start}'
                    )
                    # Refer to https://discourse.pymc.io/t/blackbox-likelihood-example-doesnt-work/5378
                    trace = pm.sample(args.samples,
                                      tune=args.samples // 10,
                                      step=algo,
                                      chains=args.chains,
                                      cores=args.chains,
                                      start=start,
                                      return_inferencedata=False,
                                      compute_convergence_checks=False,
                                      random_seed=list(seeds[i, :]),
                                      trace=trace)
                    # this is https://github.com/pymc-devs/pymc-examples/issues/48#issue-839287296
                    # see https://arviz-devs.github.io/arviz/api/generated/arviz.from_pymc3.html#arviz-from-pymc3
                    save_trace(args, trace, i=i)
        elif args.algorithm == 'smc':
            with pm_model:
                log.log_print(f'variables: {pm_model.vars}')
                traces = []
                for i in range(0, args.iterations):
                    log.log_print(f'sampling: iteration {i} at {args.samples} per iteration (SMC), initial parameters {start}')

                    traces.append(pm.sample_smc(
                            draws=args.samples, n_steps=args.samples//10,
                            random_seed=list(seeds[i,:]),
                            chains=args.chains,
                            cores=args.chains,
                            # sample_smc expects different variable names. Let's see if it works without start values for now.
                            #start=start,
                            parallel=True
                            ))

                    current_all = az.concat([az.from_pymc3(t) for t in traces], dim='chain')
                    save_trace(args, current_all, i=i)
    else:
        log.log_print(
            f'Starting to sample iteratively (iter_sample), initial parameters {start}'
        )
        if args.chains > 1:
            log.log_print(
                'WARN: --chains > 1, but sampling iteratively. This won\'t work -- sampling one chain only.'
            )
        with pm_model:
            sampler = pm.iter_sample(args.samples,
                                     algo,
                                     start=start[0] if type(start) is list else start,
                                     tune=int(args.samples / 10),
                                     random_seed=int(seeds[0, 0]))
            sample_ix = 0
            saved_traces = 0
            for trace in sampler:
                sample_ix += 1
                log.log_print(
                    f'sampling: {saved_traces} {sample_ix}/{args.iter_sample}')
                if sample_ix >= args.iter_sample:
                    save_trace(args, trace, i=saved_traces)
                    sample_ix = 0
                    saved_traces += 1
            save_trace(args, trace, i=saved_traces)

    log.log_print('Sampling finished!')


def main():
    user = os.environ.get('USER')

    p = argparse.ArgumentParser(
        description='Cluster sampling test',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    p.add_argument('--index', type=int, default=0, help='Instance index')
    p.add_argument('--run', type=str, help='Name for this run.')
    p.add_argument('--true-parameters-file',
                   type=str,
                   default=os.path.join(os.path.dirname(__file__),
                                        'test_parameters.json'),
                   help='JSON file containing the "true" model parameters.')
    p.add_argument(
        '--sample-parameters-file',
        type=str,
        default=os.path.join(os.path.dirname(__file__),
                             'sample_parameters.json'),
        help='JSON file containing the sample ranges for model parameters.')
    p.add_argument('--output-path',
                   type=str,
                   default=f'/work/{user}/data/',
                   help='Output path for sample CDF files.')
    p.add_argument('--log-path',
                   type=str,
                   default=f'/work/{user}/job_output/',
                   help='log path.')
    p.add_argument('--samples',
                   type=int,
                   default=400,
                   help='Number of samples per chain per iteration')
    p.add_argument(
        '--iterations',
        type=int,
        default=5,
        help=
        'How many successive iterations with each --samples should be run. Total number of samples is `--samples * --iterations`'
    )
    p.add_argument('--algorithm',
                   type=str,
                   choices=('default', 'metropolis', 'nuts', 'smc'),
                   default='default',
                   help='Which sampling algorithm we should use')
    p.add_argument('--chains', type=int, default=1, help='Number of chains.')
    p.add_argument(
        '--only-isos',
        type=str,
        default='',
        help=
        'Only process these isotopes (e.g. 92235,92238). Overridden by --abundances'
    )
    p.add_argument(
        '--iter-sample',
        type=int,
        default=-1,
        help=
        'Experimental: Use pm.iter_sample() and save a trace every n iterations. Disabled if < 0'
    )
    p.add_argument(
        '--structured-log',
        action='store_true',
        default=False,
        help='Write JSON log (next to application log) for analysis')
    p.add_argument(
        '--grad-max-iter',
        type=int,
        default=5,
        help=
        'Max. iterations for calculating gradients when using --algorithm=nuts.'
    )
    p.add_argument(
        '--sinks',
        type=str,
        default='DepletedUraniumSink,SeparatedWasteSink',
        help=
        'The list of sinks to calculate likelihoods for. --only-isos also determines which isotopes will be looked at.'
    )
    p.add_argument(
        '--abundances',
        type=str,
        default=None,
        help=
        'Use abundances to calculate likelihoods (if not empty). Specify like this: iso1:iso2,iso3:iso1. E.g., 92234:92235. Overrides --only-isos'
    )
    p.add_argument(
        '--use-mass',
        action='store_true', dest='use_mass',
        default=True,
        help=
        'Whether to take mass into account for calculating likelihood.'
    )
    p.add_argument(
        '--no-use-mass',
        action='store_false', dest='use_mass',
        help=
        'Whether to take mass into account for calculating likelihood.'
    )
    p.add_argument(
            '--add-groundtruth-jitter',
            action='store_true',
            default=False,
            help='Add jitter to ground truth measurements to simulate real measurements.'
    )
    p.add_argument('--jitter-sigma',
            type=float,
            default=0.01,
            help='Jitter standard deviation relative to value for isotopic compositions/masses')
    p.add_argument(
        '--rel-sigma',
        type=float,
        default=0.5,
        help='Relative sigma for calculation of likelihoods, in percent.'
    )


    args = p.parse_args()

    if args.only_isos:
        args.only_isos = [int(s) for s in args.only_isos.split(',')]

    if args.sinks:
        args.sinks = args.sinks.split(',')
    if args.abundances:
        parts = args.abundances.split(',')
        abundances = []
        for p in parts:
            i1, i2 = p.split(':')
            factor = 1
            if 'x' in i1:
                parts = i1.split('x')
                factor = float(parts[0])
                i1 = parts[1]
            abundances.append(Abundance(int(i1), int(i2), factor))
        args.abundances = abundances

    log.write_to_log_file(run=args.run, outpath=args.log_path)

    log.log_print(f'Numpy random seed: {RANDOM_SEED+args.index}')
    np.random.seed(RANDOM_SEED + args.index)
    seeds = np.random.randint(1e9, size=(args.iterations, args.chains))
    np.random.seed(RANDOM_SEED + 421337)
    jitterseed = np.random.randint(1e9)

    log.log_print(f'PyMC3 seeds: {seeds}')
    log.log_print(f'Jitter seed: {jitterseed}')

    log.log_print(f'Sampling run {args.run}, task {args.index}')
    log.log_print(f'Running with arguments: {args}')
    pm_model, start = model(args, jitterseed=jitterseed)
    sample(args, pm_model, start, seeds=seeds)


if __name__ == '__main__':
    main()
