#!/usr/bin/env python3

# This script generates a JSON Cyclus input file (which is more robust than
# directly using a Python input file)

import argparse
import json
import sys

GLOBAL_PARAMETERS = {
    'simulation_duration': None,  # Calculate from irradiation time
    'simulation_dt': 86400,
    'explicit_inventory': True,
    'decay': 'lazy',
    'reactor_type': 'savannahriver',  # ('savannahriver', 'candu')

    # Main model parameters!
    'slight_enrichment_grade': 1.1,  # Reactor input enrichment
    'tails_assay': 2e-3,  # U235 in tails
    'high_enrichment_grade': 90,  # %
    'separated_high_uranium_grade': 30,
    'separation_factor_nat': 1.35,
    'separation_factor_high_uranium': 1.4,
    'separation_efficiency_u': 0.97,
    'separation_efficiency_pu': 0.99,
    'depleted_uranium_grade': 0.1,
    'spent_fuel_grade': 0.2,
    'max_enrich_high_uranium': 0.95,
    'cycle_time': None,  #  this is being sampled, for now.
    'burnup':
    None,  # MWd, this is not used in the model itself, only for calculating the cycle time.
    'power': 2410,  # MWth
    'core_mass': 110996,  # kg
    'moderator_temperature': 350,
    'use_uranium_mine': False,
}


def cycle_time_bounds():
    burnup_min, burnup_max = (0.5, 2.5)  # MWd/t
    power = GLOBAL_PARAMETERS.get('power')
    core_mass = GLOBAL_PARAMETERS.get('core_mass')
    return calculate_cycle_time(burnup_min, power,
                                core_mass), calculate_cycle_time(
                                    burnup_max, power, core_mass)


def burnup():
    if all(
            GLOBAL_PARAMETERS.get(p)
            for p in ['cycle_time', 'power', 'core_mass']):
        return GLOBAL_PARAMETERS.get('cycle_time') * GLOBAL_PARAMETERS.get(
            'power') / GLOBAL_PARAMETERS.get('core_mass')
    return None


def calculate_cycle_time(burnup, power, core_mass):
    return burnup * core_mass / power


def cycle_time():
    # This parameter is being sampled at the moment.
    if GLOBAL_PARAMETERS.get('cycle_time', False):
        return int(GLOBAL_PARAMETERS.get('cycle_time'))

    # cycle time is not present, but we can calculate it
    if all(GLOBAL_PARAMETERS.get(p) for p in ['burnup', 'power', 'core_mass']):
        return int(
            calculate_cycle_time(GLOBAL_PARAMETERS.get('burnup'),
                                 GLOBAL_PARAMETERS.get('power'),
                                 GLOBAL_PARAMETERS.get('core_mass')))
    return None


def control_section():
    return {
        'control': {
            'startyear':
            2020,
            'startmonth':
            1,
            'duration':
            GLOBAL_PARAMETERS.get('simulation_duration')
            or (cycle_time() + 10),
            'dt':
            GLOBAL_PARAMETERS.get('simulation_dt'),
            'simhandle':
            GLOBAL_PARAMETERS.get('name'),
            'explicit_inventory':
            GLOBAL_PARAMETERS.get('explicit_inventory'),
            'decay': GLOBAL_PARAMETERS.get('decay'),
        }
    }


def archetypes_section():
    by_agent = {
        'agents': ['NullInst', 'NullRegion'],
        'cycamore': ['Sink', 'Source', 'Storage', 'Separations', 'Reactor'],
        'misoenrichment': [
            'GprReactor',
            {
                'name': 'MIsoEnrich'
                # just an example: extra attributes can be set in a dict
            }
        ],
    }

    return {
        'archetypes': {
            'spec': [{
                'lib': lib,
                'name': n if type(n) is str else '',
                **(n if type(n) is dict else {})
            } for lib in by_agent.keys() for n in by_agent[lib]],
        }
    }


def commodity_section():
    names = [
        'SlightlyEnrichedUranium',
        'DepletedUranium',
        'NaturalUranium',
        'SpentFuel',
        'HighlyEnrichedUranium',
        'SeparatedPlutonium',
        'SeparatedWaste',
        'SeparatedUranium',
    ]
    return {'commodity': [{'name': n, 'solution_priority': 1} for n in names]}


def facility_section():
    facilities = []
    if GLOBAL_PARAMETERS.get('use_uranium_mine'):
        uranium_mine = {
            'name': 'UMine',
            'config': {
                'Source': {
                    'outcommod': 'NaturalUranium',
                    'outrecipe': 'NaturalUraniumRecipe',
                    'throughput': 1e299,
                }
            }
        }
        facilities.append(uranium_mine)

        # Enriches for CANDU reactor up to 1.1%
        natural_enrichment_facility = {
            'name': 'NaturalUraniumEnrichmentFacility',
            'config': {
                'MIsoEnrich': {
                    'feed_commod': 'NaturalUranium',
                    'feed_recipe': 'NaturalUraniumRecipe',
                    'product_commod': 'SlightlyEnrichedUranium',
                    'tails_commod': 'DepletedUranium',
                    'tails_assay': GLOBAL_PARAMETERS.get('tails_assay'),
                    'initial_feed': 0,
                    'max_feed_inventory': 1e299,
                    'gamma_235':
                    GLOBAL_PARAMETERS.get('separation_factor_nat'),
                    'swu_capacity': 1e299,
                    'swu_capacity_vals': {
                        'val': [1e299]
                    },
                    'swu_capacity_times': {
                        'val': [0]
                    },
                    'use_downblending': True,
                },
            },
        }
        facilities.append(natural_enrichment_facility)
    else:  # not use_uranium_mine
        # this is used as long as we are not interested in the details of enrichment.
        enriched_uranium_source = {
            'name': 'UMine',
            'config': {
                'Source': {
                    'outcommod': 'SlightlyEnrichedUranium',
                    'outrecipe': 'SlightlyEnrichedUraniumRecipe',
                    'throughput': 1e299,
                }
            }
        }
        facilities.append(enriched_uranium_source)

    def sink(**kwargs):
        return {
            'name': kwargs.pop('name'),
            'config': {
                'Sink': {
                    'in_commods': {
                        'val': [kwargs.pop('in_commods')]
                    },
                    **kwargs
                },
            }
        }

    sep_waste_sink = sink(name='SeparatedWasteSink',
                          in_commods='SeparatedWaste')
    pu_sink = sink(name='PlutoniumSink', in_commods='SeparatedPlutonium')
    depleted_uranium_sink = sink(name='DepletedUraniumSink',
                                 in_commods='DepletedUranium',
                                 recipe_name='DepletedUraniumRecipe')
    highly_enriched_sink = sink(name='HighlyEnrichedUraniumSink',
                                in_commods='HighlyEnrichedUranium',
                                recipe_name='HighlyEnrichedUraniumRecipe')

    # We are still waiting for candu reactor data. It will be simulated using the GprReactor agent type.
    candu_reactor = {
        'name': 'CANDU_Reactor',
        'config': {
            'GprReactor': {
                'in_commods': {
                    'val': ['SlightlyEnrichedUranium']
                },
                'in_recipes': {
                    'val': ['SlightlyEnrichedUraniumRecipe']
                },
                'out_commods': {
                    'val': ['SpentFuel']
                },
                'n_assem_core': 1,
                'n_assem_batch': 1,
                'assem_size': GLOBAL_PARAMETERS.get('core_mass'),
                'cycle_time': cycle_time(),
                'refuel_time': 7,
                'power_output': GLOBAL_PARAMETERS.get('power'),
                'temperature': GLOBAL_PARAMETERS.get('moderator_temperature'),
            },
        },
    }
    cycamore_savannah_river_reactor = {
        'name': 'SR_Reactor',
        'config': {
            'Reactor': {
                'fuel_incommods': {
                    'val': ['SlightlyEnrichedUranium']
                },
                'fuel_inrecipes': {
                    'val': ['SlightlyEnrichedUraniumRecipe']
                },
                'fuel_outcommods': {
                    'val': ['SpentFuel']
                },
                'fuel_outrecipes': {
                    'val': ['SpentFuelRecipe']
                },
                'n_assem_core': 10,
                'n_assem_batch': 1,
                'assem_size': GLOBAL_PARAMETERS.get('core_mass'),
                'cycle_time': cycle_time(),
                'power_cap': GLOBAL_PARAMETERS.get('power'),
                'refuel_time': 6,
            },
        },
    }
    # We use this in the meantime.
    savannah_river_reactor = {
        'name': 'SR_Reactor',
        'config': {
            'GprReactor': {
                'in_commods': {
                    'val': ['SlightlyEnrichedUranium']
                },
                'in_recipes': {
                    'val': ['SlightlyEnrichedUraniumRecipe']
                },
                'out_commods': {
                    'val': ['SpentFuel']
                },
                'n_assem_core': 1,
                'n_assem_batch': 1,
                'assem_size': GLOBAL_PARAMETERS.get('core_mass'),
                'cycle_time': cycle_time(),
                'refuel_time': 6,
                'power_output': GLOBAL_PARAMETERS.get('power'),
                'temperature': GLOBAL_PARAMETERS.get('moderator_temperature'),
            },
        },
    }
    reactors = {
        'savannahriver': savannah_river_reactor,
        'candu': candu_reactor
    }

    # https://fuelcycle.org/user/tutorial/add_sep.html
    separations_facility = {
        'name': 'Separations',
        'config': {
            'Separations': {
                'feed_commods': {
                    'val': ['SpentFuel']
                },
                'feed_commod_prefs': {
                    'val': [1.]
                },
                'feedbuf_size':
                1e299,
                'throughput':
                1e299,
                'leftover_commod':
                'SeparatedWaste',
                'leftoverbuf_size':
                1e299,
                'streams': [
                    {
                        'item': [{
                            'commod': 'SeparatedUranium',
                            'info': {
                                'buf_size':
                                1e299,
                                'efficiencies': [{
                                    'item': [{
                                        'comp': 92000,
                                        'eff': GLOBAL_PARAMETERS.get('separation_efficiency_u'),
                                    }]
                                }]
                            }
                        }, {
                            'commod': 'SeparatedPlutonium',
                            'info': {
                                'buf_size':
                                1e299,
                                'efficiencies': [{
                                    'item': [{
                                        'comp': 94000,
                                        'eff': GLOBAL_PARAMETERS.get('separation_efficiency_pu'),
                                    }]
                                }]
                            }
                        }]
                    },
                ]
            },
        },
    }

    spent_fuel_enrichment = {
        'name': 'SpentFuelEnrichmentFacility',
        'config': {
            'MIsoEnrich': {
                'feed_commod':
                'SeparatedUranium',
                'feed_recipe':
                'SeparatedUraniumRecipe',
                'product_commod':
                'HighlyEnrichedUranium',
                'tails_commod':
                'DepletedUranium',
                # NOTE: is this the same tails assay as for natural uranium
                # enrichment?
                'tails_assay':
                GLOBAL_PARAMETERS.get('tails_assay'),
                'initial_feed':
                0,
                'max_feed_inventory':
                1e299,
                'gamma_235':
                GLOBAL_PARAMETERS.get('separation_factor_high_uranium'),
                'max_enrich':
                GLOBAL_PARAMETERS.get('max_enrich_high_uranium'),
                'swu_capacity':
                1e299,
                'swu_capacity_vals': {
                    'val': [1e299]
                },
                'swu_capacity_times': {
                    'val': [0]
                },
                'use_downblending':
                True,
            },
        },
    }

    facilities.extend([
        reactors.get(GLOBAL_PARAMETERS.get('reactor_type')),
        separations_facility,
        spent_fuel_enrichment,
        highly_enriched_sink,
        pu_sink,
        sep_waste_sink,
        depleted_uranium_sink,
    ])

    return {'facility': facilities}


def recipe_section():
    natural_uranium = {
        'name': 'NaturalUraniumRecipe',
        'basis': 'mass',
        'nuclide': [{
            'id': 'U235',
            'comp': 0.711
        }, {
            'id': 'U238',
            'comp': 99.289
        }],
    }
    slightly_enriched_uranium = {
        'name':
        'SlightlyEnrichedUraniumRecipe',
        'basis':
        'mass',
        'nuclide': [{
            'id': 'U235',
            'comp': GLOBAL_PARAMETERS.get('slight_enrichment_grade')
        }, {
            'id':
            'U238',
            'comp':
            100 - GLOBAL_PARAMETERS.get('slight_enrichment_grade')
        }],
    }
    highly_enriched_uranium = {
        'name':
        'HighlyEnrichedUraniumRecipe',
        'basis':
        'mass',
        'nuclide': [{
            'id': 'U235',
            'comp': GLOBAL_PARAMETERS.get('high_enrichment_grade')
        }, {
            'id':
            'U238',
            'comp':
            100 - GLOBAL_PARAMETERS.get('high_enrichment_grade')
        }],
    }
    spent_fuel = {
        'name':
        'SpentFuelRecipe',
        'basis':
        'mass',
        'nuclide': [{
            'id': 'U235',
            'comp': GLOBAL_PARAMETERS.get('spent_fuel_grade')
        }, {
            'id': 'U238',
            'comp': 100 - GLOBAL_PARAMETERS.get('spent_fuel_grade')
        }],
    }
    separated_high_uranium = {
        'name':
        'SeparatedUraniumRecipe',
        'basis':
        'mass',
        'nuclide': [{
            'id':
            'U235',
            'comp':
            GLOBAL_PARAMETERS.get('separated_high_uranium_grade')
        }, {
            'id':
            'U238',
            'comp':
            100 - GLOBAL_PARAMETERS.get('separated_high_uranium_grade')
        }],
    }
    depleted_uranium = {
        'name':
        'DepletedUraniumRecipe',
        'basis':
        'mass',
        'nuclide': [{
            'id': 'U235',
            'comp': GLOBAL_PARAMETERS.get('depleted_uranium_grade')
        }, {
            'id':
            'U238',
            'comp':
            100 - GLOBAL_PARAMETERS.get('depleted_uranium_grade')
        }],
    }

    return {
        'recipe': [
            natural_uranium,
            slightly_enriched_uranium,
            separated_high_uranium,
            depleted_uranium,
            highly_enriched_uranium,
            spent_fuel,
        ]
    }


def region_section():
    return {
        'region': [{
            'name':
            'region0',
            'config': {
                'NullRegion': None
            },
            'institution': [{
                'name': 'institution0',
                'config': {
                    'NullInst': None
                },
                'initialfacilitylist': {
                    'entry': [{
                        'number': 1,
                        'prototype': f.get('name')
                    } for f in facility_section().get('facility')],
                },
            }]
        }]
    }


def simulation(name='elbonia', parameters={}):
    GLOBAL_PARAMETERS.update(parameters)
    GLOBAL_PARAMETERS.update(name=name)

    simd = {}
    handlers = [
        control_section, archetypes_section, commodity_section,
        facility_section, recipe_section, region_section
    ]

    for h in handlers:
        simd.update(h())

    d = {"simulation": simd}
    return d


def main():
    p = argparse.ArgumentParser('cyclus_input')
    p.add_argument('--name',
                   default='natu-candu-heu',
                   help='A name for this run (not very important)')
    p.add_argument(
        '--parameters',
        default=None,
        type=str,
        help=
        'A JSON file containing additional parameters overriding GLOBAL_PARAMETERS'
    )
    args = p.parse_args()

    additional_params = {}
    if args.parameters is not None:
        with open(args.parameters, 'r') as ap:
            additional_params = json.load(ap)

    json.dump(simulation(name=args.name, parameters=additional_params),
              sys.stdout,
              indent=2)


if __name__ == "__main__":
    main()
