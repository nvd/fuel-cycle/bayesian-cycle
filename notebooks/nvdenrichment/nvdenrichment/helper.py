#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A collection of helper functions.
"""

# Molar masses in g/mol
MOLAR_F19 = 18.998
MOLAR_U235 = 235.04
MOLAR_U238 = 238.05


def hex_to_uranium_mass(hex_mass, depletion_factor=0.003):
    """Convert a UF6 mass to the corresponding U mass."""
    molar_uranium = (depletion_factor * MOLAR_U235 +
                     (1 - depletion_factor) * MOLAR_U238)
    uranium_mass = (hex_mass * molar_uranium / (molar_uranium + 6 * MOLAR_F19))
    return uranium_mass


def uranium_to_hex_mass(uranium_mass, depletion_factor=0.003):
    """Convert a uranium mass to the corresponding UF6 mass."""
    molar_uranium = (depletion_factor * MOLAR_U235 +
                     (1 - depletion_factor) * MOLAR_U238)
    hex_mass = uranium_mass * (1 + 6 * MOLAR_F19 / molar_uranium)
    return hex_mass
