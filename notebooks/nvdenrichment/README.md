# nvdenrichment

This repository contains programs related to uranium enrichment. It has 
been developed by the Nuclear Verification and Disarmament group from 
RWTH Aachen University, Germany [1].

## multi\_isotope\_calculator
`multi_isotope_calculator` is a module that calculates relevant enrichment 
parameters for uranium enrichment, i.e. the product and tails isotopic
composition, the number of enriching and the number of stripping stages.
It does so taking into account all of the uranium isotopes present in the 
feed, potentially making it suitable for nuclear archaeology as it allows
to trace minor isotopes.

### Later versions
Currently, only enrichment using gaseous diffusion is implemented. Later 
versions will include gas centrifugation, as well. Moreover, the feed and
product streams will be calculated.

### Known bugs
#### Problem in string comparison
The following error
```
Traceback (most recent call last):
 File "~/nvdenrichment/nvdenrichment/multi_isotope_calculator.py", line 560, in pprint
    self.calculate_staging()
  File "~/nvdenrichment/nvdenrichment/multi_isotope_calculator.py", line 522, in calculate_staging
    if ('NORM_OF_PROJECTED_GRADIENT' in result['message']
TypeError: a bytes-like object is required, not 'str'
```
can be fixed by replacing `if ('NORM_OF_PROJECTED_GRADIENT' in result['message']`
(currently line 522) in the `calculate_staging` function with
`if (b'NORM_OF_PROJECTED_GRADIENT' in result['message']` (conversion of string to bytes).
It is unclear why some users encounter this error while others do not.

## Getting started
The example below shows how the calculator can be used.

The feed concentrations are defined using a dictionary with the keys being 
the isotopes in question and the values being the concentrations in percent.
The U235 concentration is a particular case: it must be a tuple (or list)
containing three numbers, the first one being the U235 feed concentration,
the second one being the desired U235 product enrichment and the third one
being the desired U235 tails enrichment.
The U238 feed concentration does not need to be specified, it is calculated
upon instantiation.

The results are calculated and displayed using the `pprint` function. They
can be returned using `get_results`.

The `set_product` and `set_tails` functions allow to change the desired
enrichment level by passing it as an argument. In order to update the results,
either re-run the calculations using `pprint` or `calculate_staging`, which
does not pretty-print the results.

```python
from multi_isotope_calculator import Multi_isotope

concentrations = {'234': 5.5e-3, '235': (0.711, 95, 0.3)}
calculator = Multi_isotope(concentrations,
                           process='diffusion',
                           feed=1000)
calculator.pprint()
```

\---

[1] <https://www.nvd.rwth-aachen.de/>
