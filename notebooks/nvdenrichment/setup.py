#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup


def main():
    setup(name="nvdenrichment",
          version="2.0",
          description="Scripts related to uranium enrichment",
          author=
          "Nuclear Verification and Disarmament Group, RWTH Aachen University",
          url="https://git.rwth-aachen.de/nvd/fuel-cycle/nvdenrichment",
          license="BSD-3-Clause",
          packages=["nvdenrichment"],
          maintainer=["Max Schalz"],
          classifiers=[
              "License :: OSI Approved :: BSD-3-Clause License",
              "Programming Language :: Python :: 3"
          ],
          install_requires=["numpy", "scipy"])
    return


if __name__ == "__main__":
    main()
